package anime.comic.util

object Const {
    const val KEY_COUNT_AD = "keyCountAds"
    const val TOPIC_ALL = "all"
    const val FID = "firebaseId"
    const val TYPE_COMIC = "typeComic"
    const val TITLE_PAGING = "titlePaging"
    const val POLICY_URL = "policy"
    const val TOKEN_FILE = "userToken"
    const val FILE_NOT_FOUND = "fileNotFound"
    const val DATABASE_NAME = "AppDatabase"

    const val ID_GENRE = "idGenre"
    const val ID_COMIC = "idComic"
    const val COMIC_OBJECT = "comicObject"
    const val CHAPTER_OBJECT = "chapterObject"
    const val CHAPTER_ORDER = "order"
    const val CHANNEL_ID = "c178a95c-1ef1-4646-8c5f-806e773a6c31"
    private const val HTTPS = "https://"
    private val listText = listOf("a", "n", "g", "o", "m", "c", "e", "l")

    val REFERRER =
        "$HTTPS${listText[4]}${listText[0]}${listText[1]}${listText[2]}" +
                "${listText[0]}${listText[1]}${listText[6]}${listText[7]}" +
                "${listText[3]}.${listText[5]}${listText[3]}${listText[4]}"

    const val QUALITY_HD = 720
    const val QUALITY_SHD = 600
    const val QUALITY_SD = 480

    const val SPLASH_FREE = "https://api.animezizi.com/public/free/splash_screen/splash_screen.jpeg"
    const val SPLASH = "https://api.animezizi.com/public/splash_screen/splash_screen.jpg"
}