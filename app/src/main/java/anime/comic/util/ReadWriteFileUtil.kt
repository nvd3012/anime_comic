package anime.comic.util

import android.content.Context
import anime.comic.BuildConfig
import anime.comic.util.Const.TOKEN_FILE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.io.OutputStreamWriter


class ReadWriteFileUtil(private val context: Context) {

    fun saveToken(token: String) {
        try {
            OutputStreamWriter(context.openFileOutput(TOKEN_FILE, Context.MODE_PRIVATE)).use {
                it.write(token)
            }
        } catch (e: IOException) {
            Timber.e("File write failed: $e")
        }
    }

    suspend fun getToken() = withContext(Dispatchers.IO) {
        try {
            File(context.filesDir, TOKEN_FILE).let { file ->
                if (!file.exists()) return@withContext BuildConfig.TOKEN_APP
                file.useLines {
                    return@withContext "Bearer ${it.toList().joinToString()}"
                }
            }
        } catch (e: IOException) {
            Timber.e("File read failed: $e")
        }
        return@withContext BuildConfig.TOKEN_APP
    }

}
