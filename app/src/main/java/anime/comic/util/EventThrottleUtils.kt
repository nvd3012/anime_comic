package anime.comic.util

import android.os.Handler
import android.os.Looper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashSet

typealias EventThrottleTagString = String

object EventThrottleUtils {
    private val events = Collections.synchronizedSet(HashSet<EventThrottleTagString>())

    @Synchronized
    fun request(tag: String = GLOBAL, mills: Long = 300, coroutineScope: CoroutineScope, action: () -> Unit): Boolean {
        synchronized(events) {
            if (events.contains(tag)) {
                return false
            }
            events.add(tag)
            action()

            coroutineScope.launch {
                delay(mills)
                events.remove(tag)
            }
            return true
        }
    }

    const val GLOBAL = "GLOBAL"
    //TODO create something tag here

    private var doubleBackToExitPressedOnce = false
    fun doubleToAction(action: () -> Unit,showMsg: ()->Unit) {
        if (doubleBackToExitPressedOnce) {
            doubleBackToExitPressedOnce = false
            action()
            return
        }

        this.doubleBackToExitPressedOnce = true
        showMsg()
        Handler(Looper.getMainLooper()).postDelayed({
            doubleBackToExitPressedOnce = false
        }, 2000)
    }
}

