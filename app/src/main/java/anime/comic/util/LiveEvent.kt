package anime.comic.util

import androidx.annotation.MainThread
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean


/**
 * It is used when you want to display LiveData events only once,
 * such as snack bar and dialog display.
 *
 * In the case of normal LiveData, after displaying on the View side,
 * a display completion notification is sent to the ViewModel,
 * and unless the LiveData parameters are changed, the snack bar etc.
 * will be displayed again when the suspension etc. is performed.
 * If you simply set the transition event in LiveData,
 * the last value will be notified when you return from the transition destination screen,
 * so it will be moved again immediately.
 *
 * The operation around that is troublesome, so this code will solve it.
 *
 * To use it, just add `toSingleEvent ()` when creating LiveData.
 *
 * https://proandroiddev.com/livedata-with-single-events-2395dea972a8
 */
fun <T> LiveData<T>.toSingleEvent(): LiveData<T> {
    val result = LiveEvent<T>()
    result.addSource(this) {
        result.value = it
    }
    return result
}

class LiveEvent<T> : MediatorLiveData<T>() {

    private val observers = ConcurrentHashMap<LifecycleOwner, MutableSet<ObserverWrapper<T>>>()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        val wrapper = ObserverWrapper(observer)
        val set = observers[owner]
        set?.apply {
            add(wrapper)
        } ?: run {
            val newSet = Collections.newSetFromMap(ConcurrentHashMap<ObserverWrapper<T>, Boolean>())
            newSet.add(wrapper)
            observers[owner] = newSet
        }
        super.observe(owner, wrapper)
    }

    override fun removeObservers(owner: LifecycleOwner) {
        observers.remove(owner)
        super.removeObservers(owner)
    }

    @Suppress("UNCHECKED_CAST")
    @MainThread
    override fun removeObserver(observer: Observer<in T>) {
        observers.forEach {
            val wrapper = observer as ObserverWrapper<T>
            if (it.value.remove(wrapper)) {
                if (it.value.isEmpty()) {
                    observers.remove(it.key)
                }
                return@forEach
            }
        }
        super.removeObserver(observer)
    }

    @MainThread
    override fun setValue(t: T?) {
        observers.forEach { it.value.forEach { wrapper -> wrapper.newValue() } }
        super.setValue(t)
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    fun call() {
        value = null
    }

    private class ObserverWrapper<T>(private val observer: Observer<in T>) : Observer<T> {

        private val pending = AtomicBoolean(false)

        override fun onChanged(t: T?) {
            if (pending.compareAndSet(true, false)) {
                observer.onChanged(t)
            }
        }

        fun newValue() {
            pending.set(true)
        }
    }
}

/***
 * Change of any property in object and push a change notification
 * */
fun <T> MutableLiveData<T>.updateObjectProperties(actions: (T) -> Unit) {
    value?.let {
        actions(it)
        postValue(it)
    }
}

fun<T> MutableLiveData<T>.toLiveData() : LiveData<T> = this