package anime.comic.util.swiperefresh

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewConfiguration
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlin.math.abs

class VerticalSwipeRefresh(context: Context, attrs: AttributeSet?) :
    SwipeRefreshLayout(context, attrs) {
    private var mTouchSlop = 0
    private var mPrevX = 0f

    // Indicate if we've already declined the move event
    private var mDeclined = false

    init {
        mTouchSlop = ViewConfiguration.get(context).scaledTouchSlop
    }

    @SuppressLint("Recycle")
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                mPrevX = MotionEvent.obtain(ev).x
                mDeclined = false // New action
            }
            MotionEvent.ACTION_MOVE -> {
                val eventX: Float = ev.x
                val xDiff = abs(eventX - mPrevX)
                if (mDeclined || xDiff > mTouchSlop) {
                    mDeclined = true // Memorize
                    return false
                }
            }
        }
        return super.onInterceptTouchEvent(ev)
    }
}