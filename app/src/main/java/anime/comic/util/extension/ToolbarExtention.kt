package anime.comic.util.extension

import androidx.databinding.BindingAdapter
import com.google.android.material.appbar.MaterialToolbar

@BindingAdapter("android:onClickNavigationIcon")
fun MaterialToolbar.onClickNavigationIcon(onClick: () -> Unit) {
    setNavigationOnClickListener {
        onClick()
    }
}