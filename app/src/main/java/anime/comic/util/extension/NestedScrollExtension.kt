package anime.comic.util.extension

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.view.View
import android.widget.ScrollView
import androidx.core.widget.NestedScrollView
import androidx.databinding.BindingAdapter

@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("android:isDisableNestedScroll")
fun NestedScrollView.isDisable(state: Boolean){
    this.setOnTouchListener { _, _ -> state }
}