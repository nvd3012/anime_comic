package anime.comic.util.extension

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.updateLayoutParams
import anime.comic.R
import anime.comic.util.Const
import anime.comic.util.GlideApp
import anime.comic.util.helper.CommonHelper
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import timber.log.Timber

fun LinearLayout.loadImageReader(url: String?, isLoaded: () -> Unit) {
    this.post {
        val quality = if (width > Const.QUALITY_HD) Const.QUALITY_HD else Const.QUALITY_SD
        url?.let {
            val glideUrl = GlideUrl(url) { mapOf(Pair("referer", Const.REFERRER)) }
            GlideApp.with(context)
                .asDrawable()
                .load(glideUrl)
                .placeholder(CommonHelper.circularProgressDrawable(context))
                .error(
                    ContextCompat.getDrawable(
                        context, R.drawable.ic_not_found
                    )
                )
                .into(object : CustomTarget<Drawable>(quality, 1) {

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        Timber.e("failed")
//                        val ratio = width.toFloat() / (errorDrawable?.intrinsicWidth ?: 0)
//                        val imageView = ImageView(context).apply {
//                            layoutParams = ViewGroup.LayoutParams(
//                                MATCH_PARENT,
//                                (errorDrawable?.intrinsicHeight ?: 0 * ratio).toInt()
//                            )
//                            scaleType = ImageView.ScaleType.FIT_XY
//                            setImageDrawable(errorDrawable)
//                        }
//                        this@loadImageReader.addView(imageView)
                        isLoaded()
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // clear resources
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        val imageView = this@loadImageReader[0] as ImageView
                        if (width > quality) {
                            val ratio = width.toFloat() / resource.intrinsicWidth
                            this@loadImageReader.updateLayoutParams {
                                this.height = (resource.intrinsicHeight * ratio).toInt()
                            }
                        }
                        imageView.setImageDrawable(resource)
                        isLoaded()
                    }
                })
        }
    }
}

fun LinearLayout.loadImageReaderLargeSize(url: String?, isLoaded: () -> Unit) {
    this.post {
        url?.let {
            val glideUrl = GlideUrl(url) { mapOf(Pair("referer", Const.REFERRER)) }
            GlideApp.with(context)
                .asBitmap()
                .load(glideUrl)
                .placeholder(CommonHelper.circularProgressDrawable(context))
                .error(
                    ContextCompat.getDrawable(
                        context, R.drawable.ic_not_found
                    )
                )
                .into(object : CustomTarget<Bitmap>(Const.QUALITY_SHD, 1) {
                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        Timber.e("failed")
//                        val ratio = width.toFloat() / (errorDrawable?.intrinsicWidth ?: 0)
//                        val imageView = ImageView(context).apply {
//                            layoutParams = ViewGroup.LayoutParams(
//                                MATCH_PARENT,
//                                (errorDrawable?.intrinsicHeight ?: 0 * ratio).toInt()
//                            )
//                            scaleType = ImageView.ScaleType.FIT_XY
//                            setImageDrawable(errorDrawable)
//                        }
//                        this@loadImageReaderLargeSize.addView(imageView)
                        isLoaded()
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // clear resources
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        removeAllViews()
                        val ratio = width.toFloat() / resource.width
                        if (resource.height > height) {
                            val heightCrop = resource.height / 6
                            val newHeight = (heightCrop * ratio).toInt()
                            var yStartCrop = 0
                            for (i in 0..5) {
                                val newBitmap = Bitmap.createBitmap(
                                    resource,
                                    0,
                                    yStartCrop,
                                    resource.width,
                                    heightCrop
                                )

                                val imageView = ImageView(context).apply {
                                    layoutParams = ViewGroup.LayoutParams(
                                        this@loadImageReaderLargeSize.width,
                                        newHeight
                                    )
                                    scaleType = ImageView.ScaleType.FIT_XY
                                    setImageBitmap(newBitmap)
                                }
                                this@loadImageReaderLargeSize.addView(imageView)
                                yStartCrop += heightCrop
                            }
                        } else {
                            val imageView = ImageView(context).apply {
                                layoutParams = ViewGroup.LayoutParams(
                                    this@loadImageReaderLargeSize.width,
                                    (resource.height * ratio).toInt()
                                )
                                scaleType = ImageView.ScaleType.FIT_XY
                                setImageBitmap(resource)
                            }
                            this@loadImageReaderLargeSize.addView(imageView)
                        }
                        isLoaded()
                    }
                })
        }
    }
}