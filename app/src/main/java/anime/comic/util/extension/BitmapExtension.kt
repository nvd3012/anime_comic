package anime.comic.util.extension

import android.graphics.Bitmap
import timber.log.Timber
import kotlin.math.round

fun Bitmap.convertImageFitScreen(
    sizeScreen: Pair<Int, Int>
): List<Pair<Bitmap, Boolean>> {
    val listBitmap = mutableListOf<Pair<Bitmap, Boolean>>()
    val widthScreen = sizeScreen.first
    val heightScreen = sizeScreen.second
    Timber.e("screen size x = $widthScreen y= $heightScreen")
    val imageHeight = this.height
    val imageWidth = this.width
    Timber.e("image size x = $imageWidth y= $imageHeight")

    val ratioScreen = widthScreen.toFloat() / heightScreen
    val ratioImage = imageWidth.toFloat() / imageHeight

    Timber.e(
        "round number $ratioScreen to  ${round(ratioScreen * 10)} ratio image = $ratioImage to ${
            round(
                ratioImage * 10
            )
        }"
    )
    if (round(ratioScreen * 10) == round(ratioImage * 10)) {
        return listOf(
            Pair(
                this,
                false
            )
        )
    }

    if (imageHeight < heightScreen) {
        return listOf(Pair(this, true))
    } else {
        Timber.e("convert image")
        val stepHeight = imageWidth / ratioScreen
        var yStartCrop = 0
        var yEndCrop = stepHeight
        while (true) {
            val isFitHeight = yStartCrop < imageHeight && yEndCrop > imageHeight
            Timber.e("infrom yStartCrop= $yStartCrop yEndCrop= $yEndCrop imageHeight $imageHeight imageWidth $imageWidth")
            val newBitmap = Bitmap.createBitmap(
                this,
                0,
                yStartCrop,
                imageWidth,
                if (isFitHeight) imageHeight - yStartCrop else stepHeight.toInt()
            )
            yStartCrop += stepHeight.toInt()
            yEndCrop += stepHeight.toInt()
            listBitmap.add(
                Pair(newBitmap, isFitHeight)
            )
            Timber.e("newBitmap size ${newBitmap.width} and ${newBitmap.height}")
            if (yStartCrop > imageHeight) return listBitmap
        }
    }
}