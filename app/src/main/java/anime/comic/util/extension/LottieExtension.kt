package anime.comic.util.extension

import android.view.View
import androidx.databinding.BindingAdapter
import com.airbnb.lottie.LottieAnimationView

@BindingAdapter("android:startAnimation")
fun LottieAnimationView.startShowAnimation(state: Boolean) {
    if (state) {
        visibility = View.VISIBLE
        playAnimation()
    } else {
        visibility = View.INVISIBLE
        cancelAnimation()
    }
}