package anime.comic.util.extension

import android.widget.Button
import androidx.databinding.BindingAdapter

@BindingAdapter("android:isSelected")
fun Button.isSelectedState(state: Boolean) {
    isSelected = state
}