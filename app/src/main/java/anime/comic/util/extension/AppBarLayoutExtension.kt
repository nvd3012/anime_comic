package anime.comic.util.extension

import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

fun AppBarLayout.offsetChangedListener(offsetState: (OffsetChangedState) -> Unit) {
    addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
        when {
            abs(verticalOffset) == appBarLayout?.totalScrollRange -> {
                offsetState(OffsetChangedState.COLLAPSED)
            }
            verticalOffset == 0 -> {
                offsetState(OffsetChangedState.EXPANDED)
            }
            else -> {
                offsetState(OffsetChangedState.SCROLLING)
            }
        }
    })
}

sealed class OffsetChangedState {
    object COLLAPSED : OffsetChangedState()
    object EXPANDED : OffsetChangedState()
    object SCROLLING : OffsetChangedState()

    fun isCollapsed() = this is COLLAPSED
    fun isExpanded() = this is EXPANDED
    fun isScrolling() = this is SCROLLING

}