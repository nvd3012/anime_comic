package anime.comic.util.extension

import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import anime.comic.ui.base.BaseViewModel

@BindingAdapter("android:onSwipeRefresh")
fun SwipeRefreshLayout.onSwipeRefresh(onRefresh: () -> Unit) {
    setOnRefreshListener {
        onRefresh()
    }
}

@BindingAdapter("android:isRefreshing")
fun SwipeRefreshLayout.isRefreshing(pagingLoadState: BaseViewModel.PagingLoadState) {
    isRefreshing = when(pagingLoadState){
        BaseViewModel.PagingLoadState.Error -> false
        BaseViewModel.PagingLoadState.NotLoading -> false
        else -> true
    }
}

@BindingAdapter("android:isRefreshing")
fun SwipeRefreshLayout.isRefreshing(state: Boolean) {
    isRefreshing = state
}

@BindingAdapter("android:isEnableSwipeRefresh")
fun SwipeRefreshLayout.isEnable(state: Boolean) {
    isEnabled = state
}