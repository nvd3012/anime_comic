package anime.comic.util.extension

import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.interstitial.InterstitialAd

fun InterstitialAd?.handleFullContentCallBack(
    onAdFailed: (AdError?) -> Unit = {},
    onAdShowFull: () -> Unit = {},
    onAdImpress: () -> Unit = {},
    onAdDismiss: () -> Unit = {}
    ) {
    this?.fullScreenContentCallback = object : FullScreenContentCallback() {
        override fun onAdFailedToShowFullScreenContent(p0: AdError?) {
            onAdFailed(p0)
            onAdDismiss()
        }

        override fun onAdShowedFullScreenContent() {
            onAdShowFull()
        }

        override fun onAdDismissedFullScreenContent() {
            onAdDismiss()
        }

        override fun onAdImpression() {
            onAdImpress()
        }
    }
}