package anime.comic.util.extension

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import anime.comic.R

@BindingAdapter("android:setListData","android:onSelected")
fun Spinner.setListData(dataList: List<String>?, onSelected: (Int) -> Unit) {
    dataList?.let {
        adapter = ArrayAdapter(context, R.layout.dropdown_menu_item, dataList)
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                onSelected(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }
}

@BindingAdapter("android:setListData","android:onSelected")
fun Spinner.setListDataId(dataList: List<Int>?, onSelected: (Int) -> Unit) {
    dataList?.let {
        adapter = ArrayAdapter(context, R.layout.dropdown_menu_item, dataList)
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                onSelected(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }
}