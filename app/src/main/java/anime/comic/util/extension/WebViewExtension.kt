package anime.comic.util.extension

import android.webkit.WebView
import androidx.databinding.BindingAdapter


@BindingAdapter("android:loadComic")
fun WebView.loadComic(listData: List<String>?) {
    listData?.let {
        loadData(createHtmlComic(listData), "text/html", "UTF-8")
    }
}

private fun createHtmlComic(listData: List<String>): String {
    val builder = StringBuilder()
    builder.append("<body>")
    listData.forEach {
        builder.append("<img style='width:100%; height:auto' src='$it'/></br>")
    }
    builder.append("</body>")
    return builder.toString()
}