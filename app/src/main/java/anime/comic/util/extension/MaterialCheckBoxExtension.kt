package anime.comic.util.extension

import androidx.databinding.BindingAdapter
import com.google.android.material.checkbox.MaterialCheckBox

@BindingAdapter("android:isChecked")
fun MaterialCheckBox.setCheckedState(state: Boolean){
    isChecked = state
}