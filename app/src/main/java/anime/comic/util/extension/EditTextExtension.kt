package anime.comic.util.extension

import android.text.Editable
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout


@BindingAdapter("android:errorText")
fun TextInputLayout.setErrorMessage(errorMessage: String?) {
    error = errorMessage
}

@BindingAdapter("android:onClearText")
fun EditText.onClearText(state: Boolean) {
    if (state) {
        text.clear()
    }
}

@BindingAdapter("android:onSubmit")
fun EditText.onEnter(callBack: (String) -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId != EditorInfo.IME_ACTION_DONE)
            return@setOnEditorActionListener false
        callBack(text.toString())
       true
    }
}