package anime.comic.util.extension

import android.content.Context
import android.view.View
import anime.comic.R
import com.google.android.ads.nativetemplates.TemplateView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdOptions.ADCHOICES_TOP_RIGHT
import com.google.android.gms.ads.nativead.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_LANDSCAPE
import timber.log.Timber


fun TemplateView.loadAds(context: Context) {
    val adLoader = AdLoader.Builder(context, context.getString(R.string.ads_native))
        .withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(p0: LoadAdError?) {
                Timber.e("error $p0")
                visibility = View.GONE
            }
        })
        .forNativeAd {
            Timber.e("${it.body} ${it.icon} ${it.headline} ")
            visibility =
                if (it.body == null || it.body.isEmpty() || it.icon == null || it.headline == null || it.headline.isEmpty()) View.GONE else View.VISIBLE
            this.setNativeAd(it)
        }.withNativeAdOptions(
            NativeAdOptions.Builder()
                .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_LANDSCAPE)
                .setAdChoicesPlacement(ADCHOICES_TOP_RIGHT)
                .build()
        )
        .build()
    adLoader.loadAd(AdRequest.Builder().build())
}