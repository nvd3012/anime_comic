package anime.comic.util.extension

import android.content.Context
import android.util.TypedValue

fun Number.spToPx(context: Context) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    this.toFloat(),
    context.resources.displayMetrics
)

fun Number.pxToDp(context: Context) = this.toFloat() / context.resources.displayMetrics.density

fun Number.toSp(context: Context) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    this.toFloat(),
    context.resources.displayMetrics
)