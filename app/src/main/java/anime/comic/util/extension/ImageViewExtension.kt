package anime.comic.util.extension

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import anime.comic.R
import anime.comic.util.Const
import anime.comic.util.GlideApp
import anime.comic.util.helper.CommonHelper
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import jp.wasabeef.blurry.Blurry

@BindingAdapter("android:loadUrl")
fun ImageView.loadImage(url: String?) {
    loadUrl(url)
}

@BindingAdapter("android:loadUrl", "android:error")
fun ImageView.loadImage(
    url: String?,
    error: Drawable? = null
) {
    loadUrl(url, error)
}

fun ImageView.loadUrl(
    url: String?, error: Drawable? = ContextCompat.getDrawable(
        context, R.drawable.ic_not_found
    )
) {
    url?.let {
        GlideApp
            .with(context)
            .load(url)
            .placeholder(CommonHelper.circularProgressDrawable(context))
            .apply {
                error?.let {
                    error(it)
                    centerCrop()
                }
            }
            .into(this)
    }
}

@BindingAdapter("android:loadWithoutCache")
fun ImageView.loadWithoutCache(url: String?) {
    url?.let {
        GlideApp
            .with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(this)
    }
}

@BindingAdapter("android:blurBackground")
fun ImageView.blurBackground(url: String?) {
    GlideApp.with(context).asBitmap().load(url).into(object : CustomTarget<Bitmap>() {
        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
            Blurry.with(context).from(resource).into(this@blurBackground)
        }

        override fun onLoadCleared(placeholder: Drawable?) {
            // this is called when imageView is cleared on lifecycle call or for
            // some other reason.
            // if you are referencing the bitmap somewhere else too other than this imageView
            // clear it here as you can no longer have the bitmap
        }
    })
}

@BindingAdapter("android:loadImageBitmap")
fun ImageView.loadBitmap(bitmap: Bitmap) {
    GlideApp.with(context)
        .load(bitmap)
        .transition(DrawableTransitionOptions.withCrossFade(300))
        .placeholder(CommonHelper.circularProgressDrawable(context))
        .error(R.drawable.ic_not_found)
        .into(this)
}
