package anime.comic.util.extension

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import anime.comic.R
import java.time.format.DateTimeFormatter

fun Context.getWidthScreen() =
    Resources.getSystem().displayMetrics.widthPixels

fun Context.getHeightScreen() =
    Resources.getSystem().displayMetrics.heightPixels

fun Context.shareWith(content: String){
    val share = Intent.createChooser(Intent().apply {
        action = Intent.ACTION_SEND_MULTIPLE
        type = "text/plain"
        putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=tdc.animezizi.comic")

//        // (Optional) Here we're setting the title of the content
        putExtra(Intent.EXTRA_TITLE, content)
//
//        // (Optional) Here we're passing a content URI to an image to be displayed
//        data = contentUri
//        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    }, "Share with...")
    startActivity(share)
}
