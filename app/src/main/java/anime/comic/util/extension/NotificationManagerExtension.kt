package anime.comic.util.extension

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import anime.comic.R
import anime.comic.ui.home.activity.HomeActivity
import timber.log.Timber

fun NotificationManager.sendNotification(messageBody: String, applicationContext: Context) {
    Timber.e("Create notification")

    val contentIntent = Intent(applicationContext, HomeActivity::class.java)

    val contentPendingIntent = PendingIntent.getActivity(
        applicationContext,
        1,
        contentIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
//
//    val eggImage = BitmapFactory.decodeResource(
//        applicationContext.resources,
//        R.drawable.cooked_egg
//    )
//    val bigPicStyle = NotificationCompat.BigPictureStyle()
//        .bigPicture(eggImage)
//        .bigLargeIcon(null)
//
//
//    val snoozeIntent = Intent(applicationContext, SnoozeReceiver::class.java)
//    val snoozePendingIntent: PendingIntent =
//        PendingIntent.getBroadcast(applicationContext, 1, snoozeIntent, FLAGS)

    // Build the notification
    val builder = NotificationCompat.Builder(
        applicationContext,
        applicationContext.getString(R.string.app_name)
    )
        .setChannelId("CHANNEL_ID")
        .setSmallIcon(R.drawable.ic_arrow_back)
        .setContentTitle(applicationContext.getString(R.string.title_notification))
        .setContentText(messageBody)
        .setContentIntent(contentPendingIntent)
//        .setStyle(bigPicStyle)
//        .setLargeIcon(eggImage)
//        .addAction(
//            R.drawable.egg_icon,
//            applicationContext.getString(R.string.snooze),
//            snoozePendingIntent
//        )
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setAutoCancel(true)

    // Deliver the notification
    notify(231, builder.build())
}

/**
 * Cancels all notifications.
 *
 */
fun NotificationManager.cancelNotifications() {
    cancelAll()
}