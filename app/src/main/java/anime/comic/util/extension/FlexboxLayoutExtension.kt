package anime.comic.util.extension

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import anime.comic.R
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.button.MaterialButton

@BindingAdapter(
    value = [
        "android:addChips",
        "android:chipBackground",
        "android:chipCount",
        "android:onClickChipItem"
    ],
    requireAll = false
)
fun FlexboxLayout.addChips(
    chips: List<String>?,
    chipBackground: Int?,
    count: Int?,
    onClickChip: ((String) -> Unit)?
) {
    val chipCount = count ?: 2
    removeAllViews()
    chips?.let {
//        if (childCount > 1) return

        val subList = subListData(chips, chipCount)
        subList.forEach { data ->
            addView(createMaterialTextButton(this, data, chipBackground,onClickChip))
        }
        if (chips.size > subList.size) {
            addView(createMaterialTextButton(this, "...", chipBackground))
        }
    } ?: removeAllViews()
}

private fun createMaterialTextButton(
    viewGroup: ViewGroup,
    data: String,
    chipBackground: Int?,
    onClickChip: ((String) -> Unit)? = null
) = (LayoutInflater.from(viewGroup.context)
        .inflate(R.layout.item_chip, viewGroup, false) as MaterialButton).apply {
        text = data
        chipBackground?.let {
            setBackgroundColor(chipBackground)
        }
        setOnClickListener { onClickChip?.invoke(data) }
    }

private fun subListData(listData: List<String>, chipCount: Int) = if (listData.size > chipCount) {
    listData.subList(0, chipCount)
} else {
    listData
}