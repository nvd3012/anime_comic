package anime.comic.util.extension

import android.R
import android.view.View
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.transition.Fade
import androidx.transition.TransitionManager


/**
 * Get the lifecycle of the View
 *
 * @return The owner if the DataBinding is complete,
 *         otherwise it returns the lifecycle of the view itself.
 *         Null if not available
 */
@MainThread
fun View.lifecycleOwnerOrNull(): LifecycleOwner? {
    val binding = DataBindingUtil.findBinding<ViewDataBinding>(this)
    var lifecycleOwner: LifecycleOwner? = null
    if (binding != null) {
        lifecycleOwner = binding.lifecycleOwner
    }
    val ctx = this.context
    if (lifecycleOwner == null && ctx is LifecycleOwner) {
        lifecycleOwner = ctx
    }
    return lifecycleOwner
}

@BindingAdapter("android:animationAlpha")
fun View.animationAlpha(isHidden: Boolean) {
    if (isHidden) {
        this.animate().alpha(0f).duration = 0L
    } else {
        this.animate().alpha(1f).duration = 300L
    }
}

@BindingAdapter("android:isHidden", "android:isMoveUp")
fun View.animationMove(isHidden: Boolean, isMoveUp: Boolean){
    val moveY = if(isMoveUp){
        if(isHidden)-(2 * height) else 0
    }else{
        if(isHidden)(2 * height) else 0
    }

    animate().translationY(moveY.toFloat()).setDuration(200).start()
//    ObjectAnimator.ofFloat(this,"translationY",moveY.toFloat()).apply {
//        duration = 300
//        start()
//    }
}

@BindingAdapter("android:fadeTransaction")
fun View.fadeTransaction(isVisible: Boolean){
    val transition = Fade()
    transition.duration = 500
    transition.addTarget(this)
    val viewGroup = parent as ViewGroup
    TransitionManager.beginDelayedTransition(viewGroup, transition)
    visibility = if (isVisible){
        View.VISIBLE
    }else{
        View.INVISIBLE
    }
}

@BindingAdapter("android:isDisable")
fun View.isDisable(state: Boolean){
    isEnabled = !state
}

@BindingAdapter("android:isSelected")
fun View.isSelected(state: Boolean){
    isSelected = state
}