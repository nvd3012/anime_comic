package anime.comic.util.helper

import android.util.Patterns

private const val MIN_LENGTH_PASSWORD = 6

object ValidationHelper{

    fun isValidEmail(email: String) =
        email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun isPassword(password: String) =
        password.isNotEmpty() && password.trim().length > MIN_LENGTH_PASSWORD
}