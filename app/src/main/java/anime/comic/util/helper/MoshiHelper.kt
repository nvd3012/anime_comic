package anime.comic.util.helper

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

object MoshiHelper {
    fun <T : Any> toJson(format: Class<T>, data: T): String =
        Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(format).toJson(data)

    fun <T : Any> toObject(format: Class<T>, json: String) = if (json.isEmpty()) null else
        Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(format).fromJson(json)
}