package anime.comic.util.helper

import android.content.Context
import anime.comic.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback

object InterstitialAdHelper {
    fun loadInterstitialAd(context: Context, callback: (InterstitialAd?) -> Unit) {
        val adRequest = AdRequest.Builder().build()

        InterstitialAd.load(
            context,
            context.getString(R.string.ads_interstitial),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    callback(null)
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    callback(interstitialAd)
                }
            })
    }
}