package anime.comic.util.helper

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import okhttp3.internal.format
import kotlin.math.abs

object CommonHelper {
    fun formatNumberSocial(count: Long) =
        when {
            abs(count / 1000000f) >= 1 -> {
                "${format("%.1f",abs(count / 1000000f))}m"
            }
            abs(count / 1000f) >= 1 -> {
                "${format("%.1f",abs(count / 1000f))}k"
            }
            else -> {
                count.toString()
            }
        }

    fun circularProgressDrawable(context: Context) = CircularProgressDrawable(context).apply {
        strokeWidth = 5f
        centerRadius = 30f
        start()
    }
}