package anime.comic.util.helper

import android.content.Context
import anime.comic.BuildConfig
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

object CrashlyticsHelper {

    fun initialize(context: Context){
        //TODO make something
    }

    fun sendCrashlyticsMessage(msg: String){
        if(!BuildConfig.DEBUG){
            FirebaseCrashlytics.getInstance().log(msg)
        }else{
            Timber.e(msg)
        }
    }

    fun sendCrashlyticsExceptionLog(t: Throwable){
        if(!BuildConfig.DEBUG){
            FirebaseCrashlytics.getInstance().recordException(t)
        }else{
            Timber.e(t)
        }
    }
}