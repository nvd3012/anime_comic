package anime.comic.util.helper

import androidx.room.TypeConverter
import anime.comic.data.model.ComicDetailData
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class RoomConverter {
    private val listStringType = Types.newParameterizedType(List::class.java, String::class.java)
    private val listComicDetailDataType =
        Types.newParameterizedType(List::class.java, ComicDetailData::class.java)
    private val moshi =
        Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            .adapter<List<String>>(listStringType)

    private val moshiObject =
        Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            .adapter<List<ComicDetailData>>(listComicDetailDataType)

    @TypeConverter
    fun fromString(value: String): List<String> {
        return moshi.fromJson(value) ?: listOf()
    }

    @TypeConverter
    fun fromListString(list: List<String>): String {
        return moshi.toJson(list)
    }

    @TypeConverter
    fun fromStringToListComicDetail(value: String): List<ComicDetailData> {
        return moshiObject.fromJson(value) ?: listOf()
    }

    @TypeConverter
    fun fromListComicDetail(list: List<ComicDetailData>): String {
        return moshiObject.toJson(list)
    }
}