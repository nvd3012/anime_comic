package anime.comic.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import anime.comic.util.helper.CrashlyticsHelper

object FragmentUtils {
    fun addFragment(
        fragmentManager: FragmentManager,
        containerId: Int,
        fragment: Fragment
    ) {
        try {
            fragmentManager.apply {
                findFragmentByTag(fragment.tag)?.let { return }
                beginTransaction().add(containerId, fragment, fragment.tag).commit()
            }
        } catch (e: IllegalStateException) {
            CrashlyticsHelper.sendCrashlyticsExceptionLog(e)
        }
    }

    fun replaceFragment(
        fragmentManager: FragmentManager,
        containerId: Int,
        fragment: Fragment,
        tag: String = "",
        requireAddBackStack: Boolean = false
    ) {
        try {
            val transaction =
                fragmentManager.beginTransaction().replace(containerId, fragment, tag)
            if (requireAddBackStack) {
                transaction.addToBackStack(null)
            }
            transaction.commit()
        } catch (e: IllegalStateException) {
            CrashlyticsHelper.sendCrashlyticsExceptionLog(e)
        }
    }

    fun addOrShowFragment(
        fragmentManager: FragmentManager,
        containerId: Int,
        fragment: Fragment,
        tag: String
    ) {
        try {
            fragmentManager.apply {
                beginTransaction().apply {
                    fragments.forEach { fragment ->
                        hide(fragment)
                    }
                }.commit()
                findFragmentByTag(tag)?.let {
                    beginTransaction().show(it).commit()
                    return
                }
                beginTransaction().add(containerId, fragment, tag).commit()
            }
        } catch (e: IllegalStateException) {
            CrashlyticsHelper.sendCrashlyticsExceptionLog(e)
        }
    }
}