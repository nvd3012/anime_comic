package anime.comic.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.os.Build
import android.widget.Toast
import androidx.core.app.NotificationCompat
import anime.comic.R
import anime.comic.data.model.ComicDetailData
import anime.comic.data.model.DataNotificationModel
import anime.comic.domain.usecases.FcmUseCase
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.ui.home.activity.HomeActivity
import anime.comic.ui.notifications.NotificationsActivity
import anime.comic.util.helper.MoshiHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MyFirebaseMessagingService :
    FirebaseMessagingService() {
    @Inject
    lateinit var fcmUseCase: FcmUseCase

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        remoteMessage.data.let { data ->
            val json = JSONObject(data as Map<*, *>).toString()
            val dataNotification = MoshiHelper.toObject(DataNotificationModel::class.java, json)
            Timber.e("From server: $json and ${dataNotification?.comicId}")
            when (dataNotification?.topicType) {
                TYPE_ALL -> {
                    sendNotification(dataNotification, null)
                }
                TYPE_COMIC -> {
                    sendNotificationComic(dataNotification)
                }
            }

            Timber.e("Message data payload: ${remoteMessage.data}")
        }
    }

    private fun sendNotificationComic(dataNotification: DataNotificationModel) {
        MainScope().launch {
            dataNotification.comicId?.let {
                fcmUseCase.getComicDetail(it).onSuccess { data ->
                    sendNotification(dataNotification, data)
                }
            }
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Timber.e("Refreshed token: $token")
        MainScope().launch {
            fcmUseCase.upsertAppConfig {
                it.copy(tokenFcm = token)
            }
        }
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(dataNotification: DataNotificationModel, data: ComicDetailData?) {
        val resultPendingIntent =
            TaskStackBuilder.create(this).run {
                addNextIntentWithParentStack(
                    data?.let {
                        ComicDetailActivity.createIntent(
                            applicationContext,
                            it
                        )
                    } ?: HomeActivity.createIntent(applicationContext)
                )
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                Const.CHANNEL_ID,
                getString(R.string.app_name),
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                description = "descriptionText"
            }
            // Register the channel with the system
            notificationManager.createNotificationChannel(channel)
        }

        // Build the notification
        val builder = NotificationCompat.Builder(
            applicationContext,
            applicationContext.getString(R.string.app_name)
        )
            .setChannelId(Const.CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(dataNotification.title)
            .setContentText(dataNotification.content)
            .setContentIntent(resultPendingIntent)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)

        Timber.e("send notification")
        // Deliver the notification
        notificationManager.notify(dataNotification.fcmId, builder.build())
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
        const val KEY_TOKEN_FCM = "tokenFcm"
        const val TYPE_ALL = "all"
        const val TYPE_COMIC = "comic"
    }
}