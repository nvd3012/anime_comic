package anime.comic.util

import org.ocpsoft.prettytime.PrettyTime
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun formatDateSocial(timeString: String): String {
        var socialTime = ""
        try {
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
            val prettyTime = PrettyTime(Locale.US)
            socialTime = prettyTime.format(sdf.parse(timeString))
            if (socialTime.contains("from now")) {
                socialTime = socialTime.replace("from now", "ago")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return socialTime
    }

    fun formatStringToDate(timeString: String) =
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.ENGLISH)
            val date = formatter.parse(timeString)
            date?.toString() ?: timeString
        } catch (e: Exception) {
            timeString
        }
}