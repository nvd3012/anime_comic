package anime.comic.ui.comicreader.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import anime.comic.R
import anime.comic.ui.comicreader.viewmodel.ComicReaderViewModel
import anime.comic.util.extension.loadImageReader
import anime.comic.util.extension.loadImageReaderLargeSize
import kotlinx.android.synthetic.main.item_image_reader_url.view.*
import timber.log.Timber

class ImagesChapterUrlAdapter(private val viewModel: ComicReaderViewModel) :
    RecyclerView.Adapter<ImagesChapterUrlAdapter.ImageChapterItemViewHolder>() {
    private val listData = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ImageChapterItemViewHolder(parent)

    override fun onBindViewHolder(holder: ImageChapterItemViewHolder, position: Int) {
        Timber.e("size list bitmap ${listData.size}")
        holder.bind(listData[position])
    }

    override fun getItemCount() = listData.size

    override fun getItemId(position: Int) = position.toLong()

    override fun getItemViewType(position: Int) = position

    fun submitList(listUrl: List<String>) {
        Timber.e("data change ${listUrl.size}")
        listData.clear()
        listData.addAll(listUrl)
        notifyDataSetChanged()
    }

    inner class ImageChapterItemViewHolder(
        parent: ViewGroup,
        private val view: View =
            LayoutInflater.from(
                parent.context
            ).inflate(R.layout.item_image_reader_url, parent, false)

    ) : RecyclerView.ViewHolder(view) {
        fun bind(data: String) {
            if (listData.size < 3) {
                view.linearReader.loadImageReaderLargeSize(data) {
                    viewModel.setLoaded()
                }
            } else {
                view.linearReader.loadImageReader(data) {
                    viewModel.setLoaded()
                }
            }
        }
    }

}