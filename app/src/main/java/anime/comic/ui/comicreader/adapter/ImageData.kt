package anime.comic.ui.comicreader.adapter

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageData(
    val serverImage1: List<String> = listOf(),
    val serverImage2: List<String> = listOf()
) {
}