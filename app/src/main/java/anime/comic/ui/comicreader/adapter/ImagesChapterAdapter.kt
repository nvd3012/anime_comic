package anime.comic.ui.comicreader.adapter

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import anime.comic.databinding.ItemImageReaderBinding
import timber.log.Timber

class ImagesChapterAdapter(private val sizeScreen: Pair<Int, Int>) :
    RecyclerView.Adapter<ImagesChapterAdapter.ImageChapterItemViewHolder>() {
    private val differ = AsyncListDiffer(this, DIFF_UTIL)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ImageChapterItemViewHolder(parent)

    override fun onBindViewHolder(holder: ImageChapterItemViewHolder, position: Int) {
        Timber.e("size list bitmap ${differ.currentList.size}")
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount() = differ.currentList.size

    override fun getItemId(position: Int) = position.toLong()

    override fun getItemViewType(position: Int) = position

    fun submitList(listBitmap: List<Pair<Bitmap, Boolean>>) {
        Timber.e("data change ${listBitmap.size}")
        differ.submitList(listBitmap)
        notifyDataSetChanged()
    }

    fun clearData(){
        differ.submitList(null)
        notifyDataSetChanged()
    }

    companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<Pair<Bitmap, Boolean>>() {
            override fun areItemsTheSame(
                oldItem: Pair<Bitmap, Boolean>,
                newItem: Pair<Bitmap, Boolean>
            ) = false

            override fun areContentsTheSame(
                oldItem: Pair<Bitmap, Boolean>,
                newItem: Pair<Bitmap, Boolean>
            ) = false
        }
    }

    inner class ImageChapterItemViewHolder(
        private val parent: ViewGroup,
        private val binding: ItemImageReaderBinding = ItemImageReaderBinding.inflate(
            LayoutInflater.from(
                parent.context
            ), parent, false
        )
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Pair<Bitmap, Boolean>) {
            binding.apply {
                val bitmap = data.first
                val ratioBitmap = bitmap.width.toFloat() / bitmap.height
                if (data.second) {
                    imgPageComic.updateLayoutParams {
                        height = (sizeScreen.first / ratioBitmap).toInt()
                    }
                    linearLayout.updateLayoutParams {
                        height = (sizeScreen.first / ratioBitmap).toInt()
                    }
                }
                image = bitmap
                executePendingBindings()
            }
        }
    }

}