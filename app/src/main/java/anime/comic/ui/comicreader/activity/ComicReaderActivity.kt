package anime.comic.ui.comicreader.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import anime.comic.R
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ActivityComicReaderBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.ui.comicdetail.adapter.ChapterItemData
import anime.comic.ui.comicreader.adapter.ImagesChapterUrlAdapter
import anime.comic.ui.comicreader.viewmodel.ComicReaderViewModel
import anime.comic.util.Const
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ComicReaderActivity : BaseActivity<ActivityComicReaderBinding, ComicReaderViewModel>() {
    override val viewModel by viewModels<ComicReaderViewModel>()
    override val layoutId = R.layout.activity_comic_reader
    private val chapterItemData: ChapterItemData? by lazy {
        intent.getParcelableExtra(Const.CHAPTER_OBJECT)
    }
    private val comicDetailData: ComicDetailData? by lazy {
        intent.getParcelableExtra(Const.COMIC_OBJECT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val listAdapter = ImagesChapterUrlAdapter(viewModel)
        binding.apply {
            tvTitle.text = chapterItemData?.chapterName
            viewModel = this@ComicReaderActivity.viewModel

            recyclerView.apply {
                val linearLayoutManager = layoutManager as LinearLayoutManager
                adapter = listAdapter
                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        val pos = linearLayoutManager.findFirstVisibleItemPosition()

                        if (linearLayoutManager.findViewByPosition(pos)
                                ?.top == 0 && pos == 0
                        ) {
                            this@ComicReaderActivity.viewModel.setFullScreen(true)

                        } else {
                            this@ComicReaderActivity.viewModel.setFullScreen(false)
                        }
                        super.onScrollStateChanged(recyclerView, newState)
                    }
                })
            }
        }

        chapterItemData?.let {
            viewModel.fetchData(it)
        }

        viewModel.listImageUrl.observe(this) {
            listAdapter.submitList(it)
        }
    }

    override fun onBackPressed() {
        comicDetailData?.let {
            startActivity(ComicDetailActivity.createIntent(applicationContext, it))
            finish()
        } ?: finish()
    }

    companion object {
        fun createIntent(
            context: Context,
            comicDetailData: ComicDetailData? = null,
            chapterItemData: ChapterItemData
        ) = Intent(context, ComicReaderActivity::class.java).apply {
            comicDetailData?.let {
                putExtra(
                    Const.COMIC_OBJECT,
                    comicDetailData
                )
            }
            putExtra(
                Const.CHAPTER_OBJECT,
                chapterItemData
            )
        }
    }


}