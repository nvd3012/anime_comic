package anime.comic.ui.comicreader.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import anime.comic.data.model.ImageModel
import anime.comic.domain.usecases.ComicReaderUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.ui.comicdetail.adapter.ChapterItemData
import anime.comic.util.toLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ComicReaderViewModel @Inject constructor(private val comicReaderUseCase: ComicReaderUseCase) :
    BaseViewModel() {
    val listServer = listOf("Server 1", "Server 2")

    private val _imgServer1 = mutableListOf<String>()
    private val _imgServer2 = mutableListOf<String>()

    private val _isFullScreen = MutableLiveData(true)
    val isFullScreen = _isFullScreen.toLiveData()

    private val _listImageUrl = MutableLiveData<List<String>>()
    val listImageUrl = _listImageUrl.toLiveData()

    fun fetchData(chapterItemData: ChapterItemData) {
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)

        viewModelScope.launch {
            comicReaderUseCase.getImagesChapter(chapterItemData.idChapter)
                .onSuccessSuspend {
                    it?.let {
                        Timber.e("getImageServer $it")
                        _imgServer1.addAll(it.serverImage1)
                        _imgServer2.addAll(it.serverImage2)
                        loadImageServer1()
                    } ?: listOf(
                        async {
                            Timber.e("crawl server 1")
                            comicReaderUseCase.getCrawlImgServer1(chapterItemData.href)
                                .onSuccess { data ->
                                    data?.let {
                                        Timber.e("listData server 1 ${data.size}")
                                        _imgServer1.addAll(data)
                                        loadImageServer1()
                                    }
                                }
                                .onFailure {
                                    mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
                                }
                        },
                        async {
                            Timber.e("crawl server 2")
                            comicReaderUseCase.getCrawlImgServer2(chapterItemData.href)
                                .onSuccess { data ->
                                    Timber.e("listData server 2 ${data?.size}")
                                    data?.let { _imgServer2.addAll(data) }
                                }.onFailure {
                                    mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
                                }
                        }
                    ).awaitAll().apply {
                        postImgChapter(chapterItemData.idChapter)
                    }
                }
                .onFailure {
                    showAlert(content = it.msg)
                }
        }
    }

    private fun postImgChapter(idChapter: String) {
        Timber.e("post crawl to server $_imgServer1  $_imgServer2")
        viewModelScope.launch {
            if(_imgServer1.isNotEmpty()||_imgServer2.isNotEmpty()){
                comicReaderUseCase.postImgChapterToServer(
                    idChapter,
                    ImageModel(_imgServer1, _imgServer2)
                ).onSuccess { Timber.e("success") }
                    .onFailure { Timber.e("${it.code} ${it.msg}") }
            }else{
                mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
            }

        }
    }

    private fun loadImageServer1() {
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
        _listImageUrl.postValue(_imgServer1)
    }

    private fun loadImageServer2() {
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
        _listImageUrl.postValue(_imgServer2)
    }

    fun setFullScreen(state: Boolean) {
        _isFullScreen.postValue(state)
    }

    fun onFullScreen() {
        Timber.e("onClick fullscreen")
        _isFullScreen.value?.let {
            _isFullScreen.postValue(!it)
        }
    }

    fun setLoaded() {
        Timber.e("isLoaded")
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
    }

    val onSelectedSpinner = fun(position: Int) {
        if (position == 0) {
            loadImageServer1()
        } else {
            loadImageServer2()
        }
    }
}