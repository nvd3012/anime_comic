package anime.comic.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.PointF
import android.graphics.RectF
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.RecyclerView

class RecyclerviewMovableAndZoomable : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var scale = 1f
    private val start = PointF()
    private val mid = PointF()
    private var oldDist = 1f
    private var distanceX = 0f
    private var distanceY = 0f
    private lateinit var contentSize: RectF

    private var sx = 0f
    private var sy = 0f
    private var totX = 0f
    private var totY = 0f
    private var maxDx = 0f
    private var maxDy = 0f

    // Matrices used to move and zoom image.
    private val matrixView = Matrix()
    private val matrixInverse = Matrix()
    private val savedMatrix = Matrix()

    private val mScaleGestureDetector: ScaleGestureDetector

    private val mGestureDetector: GestureDetectorCompat

    private val mGestureListener: GestureDetector.SimpleOnGestureListener =
        object : GestureDetector.SimpleOnGestureListener() {
            override fun onDown(event: MotionEvent): Boolean {
                savedMatrix.set(matrixView)
                start[event.x] = event.y
                return super.onDown(event)
            }

            override fun onScroll(e1: MotionEvent, e2: MotionEvent, dX: Float, dY: Float): Boolean {
                if (scale > 1f) {
                    setupTranslation(dX, dY)
                    matrixView.postTranslate(distanceX, distanceY)
                }
                return super.onScroll(e1, e2, dX, dY)
            }

            override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
                if (scale > 1f) {
                    scale = 1f
                    matrixView.setScale(
                        scale, scale, (width / 2).toFloat(),
                        (height / 2).toFloat()
                    )
                    matrixView.invert(matrixInverse)
                    savedMatrix.set(matrixView)
                }
                return super.onDoubleTapEvent(e)
            }

            override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
                performClick()
                return super.onSingleTapConfirmed(e)
            }

            override fun onFling(
                e1: MotionEvent,
                e2: MotionEvent,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                return super.onFling(e1, e2, velocityX, velocityY)
            }
        }

    private val mScaleGestureListener: ScaleGestureDetector.OnScaleGestureListener =
        object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            /**
             * This is the active focal point in terms of the viewport. Could be a local
             * variable but kept here to minimize per-frame allocations.
             */
            override fun onScaleBegin(scaleGestureDetector: ScaleGestureDetector): Boolean {
                oldDist = scaleGestureDetector.currentSpan
                if (oldDist > 10f) {
                    savedMatrix.set(matrixView)
                    mid[scaleGestureDetector.focusX] = scaleGestureDetector.focusY
                }
                return true
            }

            override fun onScaleEnd(detector: ScaleGestureDetector) {}
            override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
                scale = scaleGestureDetector.scaleFactor
                return true
            }
        }

    init {
        mScaleGestureDetector = ScaleGestureDetector(context, mScaleGestureListener)
        mGestureDetector = GestureDetectorCompat(
            context,
            mGestureListener
        )
        setContentSize(width.toFloat(), height.toFloat())
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        mGestureDetector.onTouchEvent(event)

        if (event.pointerCount > 1) {
            mScaleGestureDetector.onTouchEvent(event)
            if (checkScaleBounds()) {
                matrixView.postScale(scale, scale, mid.x, mid.y)
            }
        }
        val userAction = event.action and MotionEvent.ACTION_MASK
        if (userAction == 6 || userAction == 262) {
            refreshScreenParams()
            if (totX * -1 > maxDx / sx) {
                distanceX = totX * -1 - maxDx / sx
            }
            if (totY * -1 > maxDy / sx) {
                distanceY = totY * -1 - maxDy / sx
            }
            if (totX > 0 && distanceX < 1) {
                distanceX = -totX * scale
            }

            if (totY > 0 && distanceY < 1) {
                distanceY = -totY * scale
            }

            if (sx > 1f) {
                matrixView.postTranslate(distanceX, distanceY);
            } else {
                scale = 1f
                matrixView.setScale(
                    scale, scale, (width / 2).toFloat(),
                    (height / 2).toFloat()
                )
            }
        }
        matrixView.invert(matrixInverse)
        savedMatrix.set(matrixView)
        invalidate()
        return super.onTouchEvent(event)
    }

//    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
//        val childCount = childCount
//        for (i in 0 until childCount) {
//            val child = getChildAt(i)
//            if (child.visibility != GONE) {
//                child.layout(left, top, left + child.measuredWidth, top + child.measuredHeight)
//            }
//        }
//    }
//
//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        val childCount = childCount
//        for (i in 0 until childCount) {
//            val child = getChildAt(i)
//            if (child.visibility != GONE) {
//                measureChild(child, widthMeasureSpec, heightMeasureSpec)
//            }
//        }
//    }

    override fun dispatchDraw(canvas: Canvas) {
        val values = FloatArray(9)
        matrixView.getValues(values)
        canvas.save()
        canvas.translate(
            values[Matrix.MTRANS_X],
            values[Matrix.MTRANS_Y]
        )
        canvas.scale(values[Matrix.MSCALE_X], values[Matrix.MSCALE_Y])
        super.dispatchDraw(canvas)
        canvas.restore()
    }

    private fun checkScaleBounds(): Boolean {
        refreshScreenParams()
        val newSx = sx * scale
        val newSy = sy * scale
        if (newSx > MIN_ZOOM && newSx < MAX_ZOOM && newSy > MIN_ZOOM && newSy < MAX_ZOOM) {
            setContentSize(newSx * width, newSy * height)
            return true
        }
        return false
    }

    private fun setupTranslation(dX: Float, dY: Float) {
        distanceX = -1 * dX
        distanceY = -1 * dY
        refreshScreenParams()
        if (totX > 0 && distanceX > 0) {
            distanceX = 0f
        }
        if (totY > 0 && distanceY > 0) {
            distanceY = 0f
        }
        if (totX * -1 > maxDx / sx && distanceX < 0) {
            distanceX = 0f
        }
        if (totY * -1 > maxDy / sx && distanceY < 0) {
            distanceY = 0f
        }
    }

    private fun setContentSize(width: Float, height: Float) {
        contentSize = RectF(0f, 0f, width, height)
    }

    companion object {
        private const val MIN_ZOOM = 0.8f
        private const val MAX_ZOOM = 2.0f
    }

    private fun refreshScreenParams() {
        val values = FloatArray(9)
        matrixView.getValues(values)
        sy = values[Matrix.MSCALE_Y]
        sx = values[Matrix.MSCALE_X]
        totX = values[Matrix.MTRANS_X] + distanceX
        totY = values[Matrix.MTRANS_Y] + distanceY
        maxDx = (contentSize.width() - (contentSize.width() / sx)) * sx
        maxDy = (contentSize.height() - (contentSize.height() / sx)) * sx
    }
}