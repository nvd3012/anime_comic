package anime.comic.ui.pagingview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.ConcatAdapter
import anime.comic.R
import anime.comic.databinding.ActivityPagingBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.base.NativeAdAdapter
import anime.comic.ui.base.NativeAdType
import anime.comic.ui.base.PagingLoadStateAdapter
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.util.Const
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PagingActivity : BaseActivity<ActivityPagingBinding, PagingViewModel>() {
    override val viewModel: PagingViewModel by viewModels()
    override val layoutId = R.layout.activity_paging
    private val titlePaging by lazy { intent.getStringExtra(Const.TITLE_PAGING) }
    private val typeComic by lazy { intent.getStringExtra(Const.TYPE_COMIC) ?: "" }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val pagingAdapter = PagingListAdapter(viewModel)
        val nativeAdSmall = NativeAdAdapter(NativeAdType.NATIVE_SMALL, viewModel)
        val nativeAdMedium = NativeAdAdapter(NativeAdType.NATIVE_MEDIUM, viewModel)

        binding.toolbar.apply {
            title = titlePaging
            setSupportActionBar(this)
            setNavigationOnClickListener {
                onBackPressed()
            }
        }
        binding.includePaging.apply {
            swipeRefresh.setOnRefreshListener {
                pagingAdapter.refresh()
            }
            recyclerView.adapter = ConcatAdapter(nativeAdSmall,
                pagingAdapter.withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter { pagingAdapter.retry() },
                    footer = PagingLoadStateAdapter { pagingAdapter.retry() }
                ), nativeAdMedium)
        }

        viewModel.apply {
            pagingAdapter.addLoadStateListener { loadState ->
                handlePagingLoading(loadState)
                if (loadState.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached) {
                    checkPagingDataEmpty(pagingAdapter.itemCount)
                }
            }

            onMoveToDetailScreen.observe(this@PagingActivity, { data ->
                startActivity(ComicDetailActivity.createIntent(applicationContext, data))
            })
        }

        lifecycleScope.launch {
            viewModel.getComicsByType(typeComic).collectLatest {
                pagingAdapter.submitData(lifecycle, it)
            }
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onDestroy() {
        lifecycleScope.cancel()
        super.onDestroy()
    }

    companion object {
        fun createIntent(context: Context, type: String, title: String) =
            Intent(context, PagingActivity::class.java).apply {
                putExtra(Const.TYPE_COMIC, type)
                putExtra(Const.TITLE_PAGING, title)
            }
    }
}