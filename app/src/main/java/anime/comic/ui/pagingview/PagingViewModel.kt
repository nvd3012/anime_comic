package anime.comic.ui.pagingview

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.usecases.PagingUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.util.EventThrottleUtils
import anime.comic.util.toSingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class PagingViewModel @Inject constructor(private val pagingUseCase: PagingUseCase) :
    BaseViewModel() {
    private val _onMoveToDetailScreen = MutableLiveData<ComicDetailData>()
    val onMoveToDetailScreen = _onMoveToDetailScreen.toSingleEvent()

    private var currentResult: Flow<PagingData<ComicDetailData>>? = null

    fun getComicsByType(type: String): Flow<PagingData<ComicDetailData>> {
        val lastResult = currentResult
        return if (lastResult != null) {
            lastResult
        } else {
            val newResult =
                pagingUseCase.getPagingComicByType(type)
                    .cachedIn(viewModelScope)
            currentResult = newResult
            newResult
        }
    }

    fun onClickDetail(comicDetailData: ComicDetailData) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onMoveToDetailScreen.postValue(comicDetailData)
        }
    }
}