package anime.comic.ui.pagingview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ItemComicPagingBinding
import anime.comic.ui.base.LifecyclePagingAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.util.extension.lifecycleOwnerOrNull

class PagingListAdapter(private val viewModel: PagingViewModel) :
    LifecyclePagingAdapter<ComicDetailData, PagingListAdapter.PagingItemViewHolder>(
        COMICS_COMPARATOR
    ) {

    override fun onBindViewHolder(holder: PagingItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, viewModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PagingItemViewHolder(parent)

    companion object {
        private val COMICS_COMPARATOR = object : DiffUtil.ItemCallback<ComicDetailData>() {
            override fun areItemsTheSame(
                oldItem: ComicDetailData,
                newItem: ComicDetailData
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ComicDetailData,
                newItem: ComicDetailData
            ) = oldItem == newItem
        }
    }

    inner class PagingItemViewHolder(
        parent: ViewGroup,
        private val binding: ItemComicPagingBinding = ItemComicPagingBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {

        fun bind(data: ComicDetailData, viewModel: PagingViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                this.data = data
            }
        }
    }
}