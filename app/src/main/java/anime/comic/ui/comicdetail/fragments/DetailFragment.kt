package anime.comic.ui.comicdetail.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import anime.comic.R
import anime.comic.databinding.FragmentDetailBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.comicdetail.viewmodel.ComicDetailViewModel
import anime.comic.util.extension.loadAds

class DetailFragment : BaseFragment<FragmentDetailBinding, ComicDetailViewModel>() {
    override val viewModel: ComicDetailViewModel by activityViewModels()
    override val layoutId = R.layout.fragment_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let { binding.adsNativeMedium.loadAds(it) }
    }

    companion object{
        fun createInstance() = DetailFragment()
    }
}