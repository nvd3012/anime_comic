package anime.comic.ui.comicdetail.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import anime.comic.R
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ActivityComicDetailBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.comicdetail.adapter.ChapterItemData
import anime.comic.ui.comicdetail.adapter.ComicDetailViewPager
import anime.comic.ui.comicdetail.viewmodel.ComicDetailEvent
import anime.comic.ui.comicdetail.viewmodel.ComicDetailViewModel
import anime.comic.ui.comicreader.activity.ComicReaderActivity
import anime.comic.ui.login.activity.LoginActivity
import anime.comic.util.Const
import anime.comic.util.extension.handleFullContentCallBack
import anime.comic.util.extension.offsetChangedListener
import anime.comic.util.extension.shareWith
import anime.comic.util.helper.InterstitialAdHelper
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class ComicDetailActivity : BaseActivity<ActivityComicDetailBinding, ComicDetailViewModel>() {
    override val viewModel: ComicDetailViewModel by viewModels()
    override val layoutId = R.layout.activity_comic_detail
    private val comicDetailData: ComicDetailData? by lazy {
        intent.getParcelableExtra(Const.COMIC_OBJECT)
    }

    private var mInterstitialAd: InterstitialAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.e("data pass ${comicDetailData?.chapterCount()}  ${comicDetailData?.thumbnail}")

        viewModel.apply {
            initComicDetailData(comicDetailData)

            onComicDetailEvent.observe(this@ComicDetailActivity) {
                when (it) {
                    ComicDetailEvent.OnBackArrow ->
                        onBackPressed()
                    is ComicDetailEvent.OnReadComic -> {
                        mInterstitialAd?.apply {
                            handleFullContentCallBack {
                                moveToReader(it.chapter)
                            }
                            show(this@ComicDetailActivity)
                        } ?: moveToReader(it.chapter)
                    }
                    ComicDetailEvent.OnShareComic -> {
                        shareWith("This is best app about manga and comic")
                    }
                    ComicDetailEvent.OnMoveToLogin -> {
                        startActivity(LoginActivity.createIntent(applicationContext))
                    }
                }
            }
        }

        binding.apply {
            appBarLayout.offsetChangedListener {
                this@ComicDetailActivity.viewModel.handleOffsetChangedState(it)
            }
            viewPager.offscreenPageLimit = ComicDetailItem.values().size
            viewPager.adapter = ComicDetailViewPager(this@ComicDetailActivity)
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = getString(ComicDetailItem.getItemName(position))
            }.attach()
        }
    }

    override fun onResume() {
        super.onResume()
        val count = viewModel.getCountAds()
        if (count >= 3) {
            InterstitialAdHelper.loadInterstitialAd(this) {
                mInterstitialAd = it
            }
            viewModel.saveCountAds(0)
        } else {
            viewModel.saveCountAds(count + 1)
        }
    }

    private fun moveToReader(chapter: ChapterItemData) {
        Timber.e("onMoveRead")
        mInterstitialAd = null
        startActivity(
            ComicReaderActivity.createIntent(
                applicationContext,
                chapterItemData = chapter
            )
        )

    }

    override fun onBackPressed() {
        finish()
    }

    companion object {
        fun createIntent(context: Context, comicDetailData: ComicDetailData) =
            Intent(context, ComicDetailActivity::class.java).apply {
                putExtra(
                    Const.COMIC_OBJECT,
                    comicDetailData
                )
            }
    }
}

enum class ComicDetailItem(val itemName: Int) {
    DETAIL(R.string.text_comic_detail),
    CHAPTERS(R.string.text_comic_chapter);

    companion object {
        fun getItemName(position: Int) = values().getOrElse(position) {
            DETAIL
        }.itemName
    }
}