package anime.comic.ui.comicdetail.viewmodel

import androidx.lifecycle.*
import androidx.paging.cachedIn
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.data.local.pref.AppPref
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.usecases.ComicDetailUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.ui.comicdetail.adapter.ChapterItemData
import anime.comic.util.Const
import anime.comic.util.EventThrottleUtils
import anime.comic.util.toLiveData
import anime.comic.util.toSingleEvent
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ComicDetailViewModel @Inject constructor(
    private val comicDetailUseCase: ComicDetailUseCase,
    private val appPref: AppPref
) :
    BaseViewModel() {
    private val _isExpanded = MutableLiveData(false)
    val isExpanded: LiveData<Boolean> = _isExpanded

    private val _comicDetailData = MutableLiveData<ComicDetailData>()
    val data: LiveData<ComicDetailData> = _comicDetailData

    private val _onComicDetailEvent = MutableLiveData<ComicDetailEvent>()
    val onComicDetailEvent = _onComicDetailEvent.toSingleEvent()

    private val _isOrderPos = MutableLiveData(true)
    val isOrderPos: LiveData<Boolean> = _isOrderPos

    val isSubscribed = data.switchMap { comicDetailUseCase.getSubscribeById(it.id) }

    private var _firstChapterItemData = MutableLiveData<ChapterItemData>()
    val firstChapterItemData = _firstChapterItemData.toLiveData()

    val getChapterRecent = data.switchMap {
        comicDetailUseCase.getChapterRecent(it.id).asLiveData()
    }

    val getChaptersComicOrderRev = data.switchMap {
        comicDetailUseCase.getPagingChapterComicOrderRev(it.id)
            .cachedIn(viewModelScope).asLiveData()
    }

    val getChaptersComicOrderPos = data.switchMap {
        comicDetailUseCase.getPagingChapterComicOrderPos(it.id)
            .cachedIn(viewModelScope).asLiveData()
    }

    fun initFirstChapterItemData(data: ChapterItemData) {
        _firstChapterItemData.postValue(data)
    }

    fun initComicDetailData(comicDetailData: ComicDetailData?) {
        comicDetailData?.let {
            _comicDetailData.postValue(it)
        }
    }

    fun onClickSeeMore() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _isExpanded.value?.let {
                _isExpanded.postValue(!it)
            }
        }
    }

    fun onChangeOrderChapters(isOrderPos: Boolean) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            if (_isOrderPos.value != isOrderPos) {
                _isOrderPos.postValue(isOrderPos)
            }
        }
    }

    fun onClickChapter(chapterItemData: ChapterItemData) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            data.value?.let { comicDetailData ->
                saveChapterRecent(comicDetailData, chapterItemData)
                _onComicDetailEvent.postValue(ComicDetailEvent.OnReadComic(chapterItemData))
            }
        }
    }

    fun onClickSubscribe() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                Timber.e("onClick sub")
                mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
                data.value?.let { data ->
                    isSubscribed.value?.let {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(data.code)
                            .addOnCompleteListener {
                                if (!it.isSuccessful) return@addOnCompleteListener
                                viewModelScope.launch {
                                    Timber.e("delete sub")
                                    comicDetailUseCase.deleteSubscribeById(data.id)
                                    toastMessage("Unsubscribe complete!")
                                }
                                mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
                            }
                            .addOnFailureListener {
                                Timber.e("unsubscribe error ")
                                mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
                                toastMessage("Don't success. Something went wrong!")
                            }
                    } ?: FirebaseMessaging.getInstance().subscribeToTopic(data.code).apply {
                        postSubscribeForUser(data)
                    }.addOnCompleteListener {
                        if (!it.isSuccessful) return@addOnCompleteListener
                        viewModelScope.launch {
                            comicDetailUseCase.addSubscribe(data)
                        }
                        toastMessage("Subscribe complete!")
                        mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
                    }
                        .addOnFailureListener {
                            Timber.e("subscribe error")
                            mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
                            toastMessage("Don't success. Something went wrong!")
                        }
                }
            }
        }
    }

    private fun postSubscribeForUser(data: ComicDetailData) {
        viewModelScope.launch {
            if (comicDetailUseCase.isUserLogin()) {
                comicDetailUseCase.postSubscribeToServer(data.id).onSuccess {
                    Timber.e("success ")
                }.onFailure {
                    Timber.e("failure ${it.msg} ")
                }
            }
        }
    }

    val onClickBack = fun() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onComicDetailEvent.postValue(ComicDetailEvent.OnBackArrow)
        }
    }

    fun onClickShare() {
        Timber.e("Click share")
        _onComicDetailEvent.postValue(ComicDetailEvent.OnShareComic)
    }

    fun onClickRating() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                if (comicDetailUseCase.isUserLogin()) {
                    _onComicDetailEvent.postValue(ComicDetailEvent.OnMoveToLogin)
                } else {
                    //Post rating
                }
            }
        }
    }

    fun onClickRead() {
        Timber.e("Click read")
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            val chapterItem = if (getChapterRecent.value == null) {
                firstChapterItemData.value

            } else {
                getChapterRecent.value?.chapterItemData
            }
            data.value?.let { comicDetailData ->
                chapterItem?.let {
                    saveChapterRecent(comicDetailData, chapterItem)
                    _onComicDetailEvent.postValue(ComicDetailEvent.OnReadComic(chapterItem))
                }
            }
        }
    }

    private fun saveChapterRecent(comicDetailData: ComicDetailData, chapter: ChapterItemData) {
        viewModelScopeException().launch {
            async {
                comicDetailUseCase.upsertAppConfig {
                    if (it.isUser) return@upsertAppConfig null
                    it.copy(isUser = true)
                }
            }.start()
            async {
                comicDetailUseCase.saveChapterRecent(
                    ChapterRecentEntity(
                        comicDetailData.id,
                        comicDetailData,
                        chapter
                    )
                )
            }.start()
        }
    }

    fun getCountAds() = appPref.getInt(Const.KEY_COUNT_AD)


    fun saveCountAds(count: Int) {
        appPref.saveInt(Const.KEY_COUNT_AD,count)
    }

    companion object {
        const val MAX_LENGTH_TEXT = 2000
        const val MIN_LENGTH_TEXT = 200
        const val CHIP_COUNT = 6
        const val MAX_LINES = 2
    }
}

sealed class ComicDetailEvent {
    object OnShareComic : ComicDetailEvent()
    class OnReadComic(val chapter: ChapterItemData) : ComicDetailEvent()
    object OnBackArrow : ComicDetailEvent()
    object OnMoveToLogin : ComicDetailEvent()
}