package anime.comic.ui.comicdetail.adapter

import android.os.Parcelable
import anime.comic.util.helper.CommonHelper
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChapterItemData(
    val idChapter: String = "",
    val chapterName: String = "",
    val chapterUploaded: String = "",
    val chapterView: Long = 0,
    val href: String = "",
) : Parcelable{
    fun formatViewCount() = CommonHelper.formatNumberSocial(chapterView)

    fun formatUploaded() = chapterUploaded.substring(0, 10)
}