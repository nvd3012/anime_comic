package anime.comic.ui.comicdetail.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import anime.comic.R
import anime.comic.databinding.FragmentChapterBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.comicdetail.viewmodel.ComicDetailViewModel
import anime.comic.util.FragmentUtils
import timber.log.Timber

class ChaptersContainerFragment :
    BaseFragment<FragmentChapterBinding, ComicDetailViewModel>() {
    override val viewModel: ComicDetailViewModel by activityViewModels()
    override val layoutId = R.layout.fragment_chapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        val fragmentPos = ChapterChildFragment.newInstance(ChaptersOrder.POSITION)
        val fragmentRev = ChapterChildFragment.newInstance(ChaptersOrder.REVERSE)

        viewModel.isOrderPos.observe(viewLifecycleOwner, {
            if (it) {
                FragmentUtils.addOrShowFragment(
                    childFragmentManager,
                    R.id.nav_container,
                    fragmentPos,
                    ChaptersOrder.POSITION.name
                )
            } else {
                FragmentUtils.addOrShowFragment(
                    childFragmentManager,
                    R.id.nav_container,
                    fragmentRev,
                    ChaptersOrder.REVERSE.name
                )
            }
        })

    }

    companion object {
        fun createInstance() = ChaptersContainerFragment()
    }
}