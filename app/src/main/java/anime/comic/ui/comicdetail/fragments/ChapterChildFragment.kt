package anime.comic.ui.comicdetail.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import anime.comic.R
import anime.comic.databinding.FragmentPagingBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.base.PagingLoadStateAdapter
import anime.comic.ui.comicdetail.adapter.ChapterListAdapter
import anime.comic.ui.comicdetail.viewmodel.ComicDetailViewModel
import anime.comic.util.Const
import timber.log.Timber

class ChapterChildFragment : BaseFragment<FragmentPagingBinding, ComicDetailViewModel>() {
    override val viewModel: ComicDetailViewModel by activityViewModels()
    override val layoutId = R.layout.fragment_paging
    private val orderType: String by lazy {
        arguments?.getString(Const.CHAPTER_ORDER) ?: ChaptersOrder.POSITION.name
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagingAdapter = ChapterListAdapter(viewModel, orderType)
        binding.apply {
            swipeRefresh.setOnRefreshListener {
                pagingAdapter.refresh()
            }
            recyclerView.adapter = pagingAdapter.withLoadStateHeaderAndFooter(
                header = PagingLoadStateAdapter { pagingAdapter.retry() },
                footer = PagingLoadStateAdapter { pagingAdapter.retry() }
            )
        }
        viewModel.apply {
            if (orderType == ChaptersOrder.REVERSE.name) {
                getChaptersComicOrderRev.observe(viewLifecycleOwner) {
                    pagingAdapter.submitData(lifecycle, it)
                }
            } else {
                getChaptersComicOrderPos.observe(viewLifecycleOwner) {
                    pagingAdapter.submitData(lifecycle, it)
                }
            }

            pagingAdapter.addLoadStateListener {
                handlePagingLoading(it)
            }
        }
    }

    companion object {
        fun newInstance(order: ChaptersOrder) = ChapterChildFragment().apply {
            arguments = Bundle().apply {
                putString(Const.CHAPTER_ORDER, order.name)
            }
        }
    }
}

enum class ChaptersOrder {
    POSITION,
    REVERSE
}