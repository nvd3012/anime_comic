package anime.comic.ui.comicdetail.adapter

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import anime.comic.R
import anime.comic.ui.comicdetail.activity.ComicDetailItem
import anime.comic.ui.comicdetail.fragments.ChaptersContainerFragment
import anime.comic.ui.comicdetail.fragments.DetailFragment

class ComicDetailViewPager(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount() = ComicDetailItem.values().size

    override fun createFragment(position: Int) =
        if (ComicDetailItem.getItemName(position) == R.string.text_comic_detail)
            DetailFragment.createInstance()
        else ChaptersContainerFragment.createInstance()
}