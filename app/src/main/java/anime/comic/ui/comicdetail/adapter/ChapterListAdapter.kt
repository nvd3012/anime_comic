package anime.comic.ui.comicdetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import anime.comic.databinding.ItemChapterBinding
import anime.comic.ui.base.LifecyclePagingAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.comicdetail.fragments.ChaptersOrder
import anime.comic.ui.comicdetail.viewmodel.ComicDetailViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull

class ChapterListAdapter(
    private val viewModel: ComicDetailViewModel,
    private val order: String
) :
    LifecyclePagingAdapter<ChapterItemData, ChapterListAdapter.ChapterItemViewHolder>(
        COMICS_COMPARATOR
    ) {

    override fun onBindViewHolder(holder: ChapterItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, viewModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChapterItemViewHolder {
        if (order == ChaptersOrder.POSITION.name) {
            getItem(FIRST_POSITION)?.let {
                viewModel.initFirstChapterItemData(it)
            }
        }
        return ChapterItemViewHolder(parent)
    }

    companion object {
        private const val FIRST_POSITION = 0
        private val COMICS_COMPARATOR = object : DiffUtil.ItemCallback<ChapterItemData>() {
            override fun areItemsTheSame(
                oldItem: ChapterItemData,
                newItem: ChapterItemData
            ) = oldItem.idChapter == newItem.idChapter

            override fun areContentsTheSame(
                oldItem: ChapterItemData,
                newItem: ChapterItemData
            ) = oldItem == newItem
        }
    }

    inner class ChapterItemViewHolder(
        parent: ViewGroup,
        private val binding: ItemChapterBinding = ItemChapterBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {

        fun bind(data: ChapterItemData, viewModel: ComicDetailViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                this.data = data
            }
        }
    }
}