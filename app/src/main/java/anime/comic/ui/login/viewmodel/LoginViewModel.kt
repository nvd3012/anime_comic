package anime.comic.ui.login.viewmodel

import android.text.Editable
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import anime.comic.data.remote.api.auth.LoginRequest
import anime.comic.data.remote.api.auth.SignUpRequest
import anime.comic.domain.repositories.UserRepository
import anime.comic.ui.base.BaseViewModel
import anime.comic.util.EventThrottleUtils
import anime.comic.util.toSingleEvent
import anime.comic.util.updateObjectProperties
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class LoginViewModel @ViewModelInject constructor(private val userRepository: UserRepository) :
    BaseViewModel() {
    private val _loginRequest = MutableLiveData(LoginRequest())
    val loginRequest: LiveData<LoginRequest> = _loginRequest
    private val _signUpRequest = MutableLiveData(SignUpRequest())
    val signUpRequest: LiveData<SignUpRequest> = _signUpRequest

    private val _onLoginEvents = MutableLiveData<OnLoginEvents>()
    val onLoginEvents: LiveData<OnLoginEvents> = _onLoginEvents.toSingleEvent()

    fun login() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                showLoading()
                loginRequest.value?.let {
                    userRepository.login(it).onSuccess {
                        _onLoginEvents.postValue(OnLoginEvents.MovePreviousScreen)
                        async(SupervisorJob()) {
                            userRepository.sendTokenFcm()
                        }.start()
                    }.onFailure { error -> showAlert(content = error.msg) }
                }
                hideLoading()
            }
        }
    }

    fun signUp() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                showLoading()
                signUpRequest.value?.let {
                    userRepository.signUp(it).onSuccess {
                        showAlert(
                            title = "Register success!",
                            content = "Please verified email before login.",
                            posEvent = {
                                _onLoginEvents.postValue(OnLoginEvents.PopBackStack)
                            }
                        )
                    }.onFailure { error ->
                        showAlert(
                            title = "Register Error!",
                            content = error.msg
                        )
                    }
                }
                hideLoading()
            }
        }
    }

    fun resetPassword() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                showLoading()
                loginRequest.value?.let {
                    userRepository.resetPassword(it.email)
                        .onSuccess {
                            showAlert(
                                title = "Reset success!",
                                content = "Please check email to receive new password.",
                                posEvent = {
                                    _onLoginEvents.postValue(OnLoginEvents.PopBackStack)
                                }
                            )
                        }.onFailure { error ->
                            showAlert(
                                title = "Reset password Error!",
                                content = error.msg
                            )
                        }
                }
                hideLoading()
            }
        }
    }

    fun onChangeTextInputEmailLogin(content: Editable?) {
        _loginRequest.updateObjectProperties {
            it.email = content.toString()
        }
    }

    fun onChangeTextInputPasswordLogin(content: Editable?) {
        _loginRequest.updateObjectProperties {
            it.password = content.toString()
        }
    }

    fun onChangeTextInputEmailSignUp(content: Editable?) {
        _signUpRequest.updateObjectProperties {
            it.email = content.toString()
        }
    }

    fun onChangeTextInputPasswordSignUp(content: Editable?) {
        _signUpRequest.updateObjectProperties {
            it.password = content.toString()
        }
    }

    fun onChangeTextInputConfirmPasswordSignUp(content: Editable?) {
        _signUpRequest.updateObjectProperties {
            it.confirmPassword = content.toString()
        }
    }

    fun onAgreeTerm() {
        _signUpRequest.updateObjectProperties {
            it.isAcceptTerm = !it.isAcceptTerm
        }
    }

    fun moveToRegister() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onLoginEvents.postValue(OnLoginEvents.MoveToRegister)
        }
    }

    fun moveToResetPassword() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onLoginEvents.postValue(OnLoginEvents.MoveToResetPassword)
        }
    }

    fun popBackStack() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onLoginEvents.postValue(OnLoginEvents.PopBackStack)
        }
    }
}

sealed class OnLoginEvents {
    object MovePreviousScreen : OnLoginEvents()
    object PopBackStack : OnLoginEvents()
    object MoveToRegister : OnLoginEvents()
    object MoveToResetPassword : OnLoginEvents()
}