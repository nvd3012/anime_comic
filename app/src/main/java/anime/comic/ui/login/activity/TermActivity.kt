package anime.comic.ui.login.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import anime.comic.R
import anime.comic.util.Const

class TermActivity : AppCompatActivity() {
    private val policyUrl by lazy {
        intent.getStringExtra(Const.POLICY_URL)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

    }

    companion object {
        fun createIntent(context: Context, policyUrl: String) =
            Intent(context, TermActivity::class.java).apply {
                putExtra(Const.POLICY_URL, policyUrl)
            }
    }
}