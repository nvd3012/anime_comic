package anime.comic.ui.login.fragments

import androidx.fragment.app.activityViewModels
import anime.comic.R
import anime.comic.databinding.FragmentLoginBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.login.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>() {
    override val viewModel: LoginViewModel by activityViewModels()
    override val layoutId: Int = R.layout.fragment_login
}