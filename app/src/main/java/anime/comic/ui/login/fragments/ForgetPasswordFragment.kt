package anime.comic.ui.login.fragments

import androidx.fragment.app.activityViewModels
import anime.comic.R
import anime.comic.databinding.FragmentResetPasswordBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.login.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgetPasswordFragment : BaseFragment<FragmentResetPasswordBinding, LoginViewModel>() {
    override val viewModel: LoginViewModel by activityViewModels()
    override val layoutId: Int = R.layout.fragment_reset_password
}
