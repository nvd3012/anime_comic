package anime.comic.ui.login.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import anime.comic.R
import anime.comic.ui.login.viewmodel.LoginViewModel
import anime.comic.ui.login.viewmodel.OnLoginEvents
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val viewModel by viewModels<LoginViewModel>()
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        viewModel.onLoginEvents.observe(this, {
            when (it) {
                OnLoginEvents.MovePreviousScreen -> {
                    onBackPressed()
                }
                OnLoginEvents.PopBackStack -> {
                    navController.popBackStack()
                }
                OnLoginEvents.MoveToRegister -> {
                    navController.navigate(R.id.action_loginFragment_to_registerFragment)
                }
                OnLoginEvents.MoveToResetPassword -> {
                    navController.navigate(R.id.action_loginFragment_to_forgetPasswordFragment)
                }
            }
        })
    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id == R.id.loginFragment) {
            finish()
        }
        navController.popBackStack()
    }

    companion object {
        fun createIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }
}