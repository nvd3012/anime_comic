package anime.comic.ui.home.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import anime.comic.R
import anime.comic.databinding.ActivityHomeInBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.home.viewmodel.HomeEvent
import anime.comic.ui.home.viewmodel.HomeViewModel
import anime.comic.ui.pagingview.PagingActivity
import anime.comic.ui.search.activity.SearchActivity
import anime.comic.util.EventThrottleUtils
import com.google.android.gms.ads.MobileAds
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeInActivity : BaseActivity<ActivityHomeInBinding, HomeViewModel>() {
    private lateinit var navController: NavController
    override val viewModel by viewModels<HomeViewModel>()
    override val layoutId: Int
        get() = R.layout.activity_home_in

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MobileAds.initialize(this)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        binding.navView.setupWithNavController(navController)

        viewModel.onHomeEvent.observe(this) {
            when (it) {
                is HomeEvent.OnMoveToSeeAll -> {
                    startActivity(
                        PagingActivity.createIntent(
                            applicationContext,
                            it.type,
                            getString(if (it.type == HomeViewModel.TYPE_NEWEST) R.string.label_new_manga else R.string.label_latest_update)
                        )
                    )
                }
                is HomeEvent.OnMoveToDetail -> {
                    startActivity(
                        ComicDetailInActivity.createIntent(
                            applicationContext,
                            it.comicDetailData
                        )
                    )
                }
                HomeEvent.OnMoveToSearch -> {
                    startActivity(SearchActivity.createIntent(applicationContext))
                }
            }
        }

    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id == R.id.navigation_home) {
            EventThrottleUtils.doubleToAction({
                finishAndRemoveTask()
            }, {
                Toast.makeText(this, getString(R.string.text_tap_again), Toast.LENGTH_SHORT).show()
            })
            return
        }
        super.onBackPressed()
    }

    companion object {
        fun createIntent(context: Context) = Intent(context, HomeInActivity::class.java).apply {
        }
    }
}