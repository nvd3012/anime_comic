package anime.comic.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ItemComicNewBinding
import anime.comic.ui.base.LifecycleRecyclerAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.home.viewmodel.HomeViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull
import com.google.android.flexbox.FlexboxLayoutManager
import timber.log.Timber

class NewAdapter(private val homeViewModel: HomeViewModel) :
    LifecycleRecyclerAdapter<NewAdapter.NewItemViewHolder>() {
    private var listData = listOf<ComicDetailData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = NewItemViewHolder(parent)

    override fun onBindViewHolder(holder: NewItemViewHolder, position: Int) {
            holder.bind(listData[position], homeViewModel)
    }

    override fun getItemCount() = listData.size

    fun submitList(listData: List<ComicDetailData>) {
        this.listData = listData
        notifyDataSetChanged()
    }

    inner class NewItemViewHolder(
        private val parent: ViewGroup,
        private val binding: ItemComicNewBinding = ItemComicNewBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {

        fun bind(data: ComicDetailData, homeViewModel: HomeViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                val lp = cstLayout.layoutParams
                if (lp is FlexboxLayoutManager.LayoutParams) {
                    lp.flexGrow = 1f
                }
                this.data = data
                viewModel = homeViewModel
                executePendingBindings()
            }
        }
    }
}