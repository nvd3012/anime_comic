package anime.comic.ui.home.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import anime.comic.R
import anime.comic.databinding.FragmentPagingBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.base.PagingLoadStateAdapter
import anime.comic.ui.genre.adapter.GenreListAdapter
import anime.comic.ui.genre.viewmodel.GenreViewModel
import anime.comic.ui.genre.viewmodel.OnGenreEvent
import anime.comic.ui.home.activity.ComicDetailInActivity
import anime.comic.util.Const
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class GenreFragmentChildIn : BaseFragment<FragmentPagingBinding, GenreViewModel>() {
    override val viewModel by viewModels<GenreViewModel>()
    override val layoutId = R.layout.fragment_paging
    private val genreName by lazy { arguments?.getString(Const.ID_GENRE) ?: "" }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagingAdapter = GenreListAdapter(viewModel)
        binding.apply {
            swipeRefresh.apply {
                isRefreshing = true
                setOnRefreshListener {
                    pagingAdapter.refresh()
                }
                postDelayed({
                    recyclerView.adapter = pagingAdapter.withLoadStateHeaderAndFooter(
                        header = PagingLoadStateAdapter { pagingAdapter.retry() },
                        footer = PagingLoadStateAdapter { pagingAdapter.retry() }
                    )
                    this@GenreFragmentChildIn.viewModel.apply {
                        pagingAdapter.addLoadStateListener {
                            handlePagingLoading(it)
                        }

                        onGenreEvent.observe(viewLifecycleOwner, { event ->
                            if (event is OnGenreEvent.OnMoveToDetailScreen) {
                                activity?.let {
                                    startActivity(
                                        ComicDetailInActivity.createIntent(
                                            it.applicationContext,
                                            event.data
                                        )
                                    )
                                }
                            }
                        })
                    }
                    lifecycleScope.launch {
                        this@GenreFragmentChildIn.viewModel.getComicsByGenreName(genreName)
                            .collectLatest {
                                pagingAdapter.submitData(lifecycle, it)
                            }
                    }
                }, 1000)
            }
        }
    }

    companion object {
        fun newInstance(genre: String) = GenreFragmentChildIn().apply {
            arguments = Bundle().apply {
                putString(Const.ID_GENRE, genre)
            }
        }
    }
}