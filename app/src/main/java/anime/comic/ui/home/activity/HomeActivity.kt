package anime.comic.ui.home.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import anime.comic.R
import anime.comic.databinding.ActivityHomeBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.base.MaintenanceActivity
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.ui.home.viewmodel.AppState
import anime.comic.ui.home.viewmodel.HomeEvent
import anime.comic.ui.home.viewmodel.HomeViewModel
import anime.comic.ui.pagingview.PagingActivity
import anime.comic.ui.search.activity.SearchActivity
import anime.comic.util.EventThrottleUtils
import com.google.android.gms.ads.MobileAds
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>() {
    private lateinit var navController: NavController
    override val viewModel by viewModels<HomeViewModel>()
    override val layoutId = R.layout.activity_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        binding.navView.setupWithNavController(navController)
        MobileAds.initialize(this)

        viewModel.apply {
            onHomeEvent.observe(this@HomeActivity) {
                when (it) {
                    is HomeEvent.OnMoveToSeeAll -> {
                        startActivity(
                            PagingActivity.createIntent(
                                applicationContext,
                                it.type,
                                getString(if (it.type == HomeViewModel.TYPE_NEWEST) R.string.label_new_manga else R.string.label_latest_update)
                            )
                        )
                    }
                    is HomeEvent.OnMoveToDetail -> {
                        startActivity(
                            ComicDetailActivity.createIntent(
                                applicationContext,
                                it.comicDetailData
                            )
                        )
                    }
                    HomeEvent.OnMoveToSearch -> {
                        startActivity(SearchActivity.createIntent(applicationContext))
                    }
                }
            }

            getStateApp.observe(this@HomeActivity) {
                Timber.e("aaaaaaaaaaaaaaaa${it.name}")
                when (it) {
                    AppState.NONE -> {
                        //Network error
                    }
                    AppState.MAINTENANCE -> {
                        startActivity(MaintenanceActivity.createIntent(applicationContext))
                        finish()
                    }
                    AppState.REVIEW -> {
                        startActivity(HomeInActivity.createIntent(applicationContext))
                        finish()
                    }
                    else -> {
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id == R.id.navigation_home) {
            EventThrottleUtils.doubleToAction({
                finishAndRemoveTask()
            }, {
                Toast.makeText(this, getString(R.string.text_tap_again), Toast.LENGTH_SHORT).show()
            })
            return
        }
        super.onBackPressed()
    }

    companion object {
        fun createIntent(context: Context) = Intent(context, HomeActivity::class.java).apply {
        }
    }
}