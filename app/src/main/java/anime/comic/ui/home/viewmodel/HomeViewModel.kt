package anime.comic.ui.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.usecases.HomeUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.ui.home.adapter.HomeData
import anime.comic.util.Const
import anime.comic.util.EventThrottleUtils
import anime.comic.util.helper.MoshiHelper
import anime.comic.util.toLiveData
import anime.comic.util.toSingleEvent
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeUseCase: HomeUseCase) :
    BaseViewModel() {

    private val _onHomeEvent = MutableLiveData<HomeEvent>()
    val onHomeEvent = _onHomeEvent.toSingleEvent()

    private val _mutableHomeData = MutableLiveData<HomeData>()
    val dataHome: LiveData<HomeData> = _mutableHomeData

    private var _isUser = false

    private val _stateApp = MutableLiveData<AppState>()
    val getStateApp = _stateApp.toLiveData()

    private val _splashImage = MutableLiveData<String>()
    val splashImage = _splashImage.toLiveData()

    init {
        fetchInitialSettingInfo()
    }

    private fun fetchInitialSettingInfo() {
        viewModelScope.launch {
            _isUser = homeUseCase.getAppConfig()?.isUser == true
            getAppConfigFromServer()
            listOf(
                async { homeUseCase.sendTokenFcm() },
                async { fetchData() },
            ).awaitAll()
            delay(3000)
            _stateApp.postValue(AppState.NORMAL)
            Timber.e("init success")
        }
    }

    private suspend fun fetchData() {
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
        FirebaseMessaging.getInstance().subscribeToTopic(Const.TOPIC_ALL)
        homeUseCase.getDataHomeScreen()
            .onSuccess {
                _mutableHomeData.postValue(it)
                mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
            }
            .onFailure {
                mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
                _stateApp.postValue(AppState.MAINTENANCE)
            }
    }

    private suspend fun getAppConfigFromServer() {
        homeUseCase.getAppConfigFromServer().onSuccessSuspend {

            when (val stateApp =
                it?.status?.let { state -> AppState.getState(state) } ?: AppState.NONE) {
                AppState.REVIEW -> {
                    if (_isUser) {
                        _splashImage.postValue(Const.SPLASH)
                        return@onSuccessSuspend
                    }
                    _splashImage.postValue(Const.SPLASH_FREE)
                    Timber.e("getAppConfigFromServer post review")
                    _stateApp.postValue(stateApp)
                }
                else -> {
                    if(stateApp.isNormal()) {
                        _splashImage.postValue(Const.SPLASH)
                        return@onSuccessSuspend
                    }
                    _splashImage.postValue(Const.SPLASH_FREE)
                    _stateApp.postValue(stateApp)
                }
            }
        }.onFailure {
            _stateApp.postValue(AppState.MAINTENANCE)
            Timber.e("getAppConfigFromServer ${it.msg}")
            viewModelScope.cancel()
//            if (!_isUser) return@onFailure
        }
    }

    val onSwipeRefresh = fun() {
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                fetchData()
            }
        }
    }

    fun onClickSeeAll(type: String) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onHomeEvent.postValue(HomeEvent.OnMoveToSeeAll(type))
        }
    }

    fun onClickDetail(comicDetailData: ComicDetailData) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            Timber.e("latest ${MoshiHelper.toJson(ComicDetailData::class.java, comicDetailData)}")
            _onHomeEvent.postValue(HomeEvent.OnMoveToDetail(comicDetailData))
        }
    }

    fun onClickSearch() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onHomeEvent.postValue(HomeEvent.OnMoveToSearch)
        }
    }

    fun postFID() {
        viewModelScope.launch {
            if (homeUseCase.getAppConfig() == null || homeUseCase.getAppConfig()?.fid.isNullOrBlank()) {
                FirebaseInstallations.getInstance().id.addOnCompleteListener { data ->
                    if (data.isSuccessful) {
                        viewModelScope.launch {
                            homeUseCase.postFID(data.result).onSuccessSuspend {
//                                homeUseCase.upsertAppConfig(data.result)
                            }.onFailure {
                                Timber.e(it.msg)
                            }
                        }
                        Timber.e("Success")
                    }
                }
            }
        }
    }

//    fun setAppStateNormal() {
//        viewModelScope.launch {
//            homeUseCase.upsertAppConfig {
//                it.copy(status = AppState.NORMAL.ordinal)
//            }
//        }
//    }
//
//    fun setAppStateNone() {
//        if (isUser) {
//            setAppStateNormal()
//        } else {
//            viewModelScope.launch {
//                homeUseCase.upsertAppConfig {
//                    it.copy(status = AppState.NONE.ordinal)
//                }
//            }
//        }
//    }

    companion object {
        const val TYPE_NEWEST = "newest"
        const val TYPE_LATEST = "latest"
    }
}

sealed class HomeEvent {
    class OnMoveToDetail(val comicDetailData: ComicDetailData) : HomeEvent()
    class OnMoveToSeeAll(val type: String) : HomeEvent()
    object OnMoveToSearch : HomeEvent()
}

enum class AppState {
    NONE,
    NORMAL,
    MAINTENANCE,
    REVIEW;

    fun isNormal() = this.name == NORMAL.name
    fun isMaintenance() = this.name == MAINTENANCE.name
    fun isReview() = this.name == REVIEW.name

    companion object {
        fun getState(index: Int) = values().getOrElse(index) { NORMAL }
    }
}