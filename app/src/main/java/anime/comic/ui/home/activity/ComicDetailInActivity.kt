package anime.comic.ui.home.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import anime.comic.R
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ActivityComicDetailInBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.comicdetail.viewmodel.ComicDetailEvent
import anime.comic.ui.comicdetail.viewmodel.ComicDetailViewModel
import anime.comic.ui.comicreader.activity.ComicReaderActivity
import anime.comic.ui.login.activity.LoginActivity
import anime.comic.util.Const
import anime.comic.util.extension.offsetChangedListener
import anime.comic.util.extension.shareWith
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class ComicDetailInActivity : BaseActivity<ActivityComicDetailInBinding, ComicDetailViewModel>() {
    override val viewModel: ComicDetailViewModel by viewModels()
    override val layoutId = R.layout.activity_comic_detail_in
    private val comicDetailData: ComicDetailData? by lazy {
        intent.getParcelableExtra(Const.COMIC_OBJECT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.e("data pass ${comicDetailData?.chapterCount()}  ${comicDetailData?.thumbnail}")

        viewModel.apply {
            initComicDetailData(comicDetailData)

            onComicDetailEvent.observe(this@ComicDetailInActivity) {
                when (it) {
                    ComicDetailEvent.OnBackArrow ->
                        onBackPressed()
                    is ComicDetailEvent.OnReadComic -> {
                        startActivity(
                            ComicReaderActivity.createIntent(
                                applicationContext,
                                chapterItemData = it.chapter
                            )
                        )
                    }
                    ComicDetailEvent.OnShareComic -> {
                        shareWith("App")
                    }
                    ComicDetailEvent.OnMoveToLogin -> {
                        startActivity(LoginActivity.createIntent(applicationContext))
                    }
                }
            }
        }
        binding.appBarLayout.offsetChangedListener {
            viewModel.handleOffsetChangedState(it)
        }
    }

    override fun onBackPressed() {
        finish()
    }

    companion object {
        fun createIntent(context: Context, comicDetailData: ComicDetailData) =
            Intent(context, ComicDetailInActivity::class.java).apply {
                putExtra(
                    Const.COMIC_OBJECT,
                    comicDetailData
                )
            }
    }
}