package anime.comic.ui.home.adapter

import anime.comic.data.model.ComicDetailData

data class HomeData(
    val latest: List<ComicDetailData> = listOf(),
    val newest: List<ComicDetailData> = listOf(),
    val popular: List<ComicDetailData> = listOf(),
    val topWeek: List<ComicDetailData> = listOf(),
    val banners: List<String> = listOf()
) {}