package anime.comic.ui.home.adapter

data class HomeItemData(
    val id: String = "",
    val name: String = "",
    val otherName: String = "",
    val thumbnail: String = "",
    val updated: String = "",
    val lastChapter: String = "",
    val newestChapter: String = ""
) {
}