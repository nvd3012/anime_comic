package anime.comic.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ItemComicTopWeekBinding
import anime.comic.ui.base.LifecycleRecyclerAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.home.viewmodel.HomeViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull

class TopAdapter(private val homeViewModel: HomeViewModel) :
    LifecycleRecyclerAdapter<TopAdapter.TopItemViewHolder>() {

    private var listData = listOf<ComicDetailData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TopItemViewHolder(parent)

    override fun onBindViewHolder(holder: TopItemViewHolder, position: Int) {
        holder.bind(listData[position], homeViewModel)
    }

    override fun getItemCount() = listData.size

    fun submitList(listData: List<ComicDetailData>) {
        this.listData = listData
        notifyDataSetChanged()
    }

    inner class TopItemViewHolder(
        private val parent: ViewGroup,
        private val binding: ItemComicTopWeekBinding = ItemComicTopWeekBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {
        fun bind(data: ComicDetailData, viewModel: HomeViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.data = data
                this.viewModel = viewModel
            }
        }
    }
}