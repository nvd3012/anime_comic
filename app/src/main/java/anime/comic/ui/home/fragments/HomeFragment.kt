package anime.comic.ui.home.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import anime.comic.R
import anime.comic.databinding.FragmentHomeBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.home.adapter.*
import anime.comic.ui.home.viewmodel.HomeViewModel
import anime.comic.ui.search.activity.SearchActivity
import anime.comic.util.extension.loadAds
import anime.comic.util.extension.offsetChangedListener
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    override val viewModel by activityViewModels<HomeViewModel>()
    override val layoutId = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sliderAdapter = SliderAdapter()
        val topAdapter = TopAdapter(viewModel)
        val newAdapter = NewAdapter(viewModel)
        val latestAdapter = LatestAdapter(viewModel)
        val mostAdapter = MostAdapter(viewModel)

        binding.apply {
            context?.let {
                adsNativeMedium.loadAds(it)
                adsNativeSmall.loadAds(it)
            }
            imageSlider.apply {
                setSliderAdapter(sliderAdapter)
                setIndicatorAnimation(IndicatorAnimationType.WORM)
                setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
                startAutoCycle()
            }

            appBarLayout.offsetChangedListener {
                this@HomeFragment.viewModel.handleOffsetChangedState(
                    it
                )
            }

            rvLatestUpdate.adapter = latestAdapter

            rvMostPopular.adapter = mostAdapter

            rvNewManga.adapter = newAdapter

            rvTopWeek.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = topAdapter
                setHasFixedSize(true)
            }
        }

        viewModel.dataHome.observe(viewLifecycleOwner) {
            it.apply {
                sliderAdapter.submitList(banners)
                latestAdapter.submitList(latest)
                newAdapter.submitList(newest)
                mostAdapter.submitList(popular)
                topAdapter.submitList(topWeek)
            }
        }
    }
}