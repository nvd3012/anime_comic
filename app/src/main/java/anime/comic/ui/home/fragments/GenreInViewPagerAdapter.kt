package anime.comic.ui.home.fragments


import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import anime.comic.data.model.GenreModel

class GenreInViewPagerAdapter(fragment: Fragment, private val listGenres: List<GenreModel>) : FragmentStateAdapter(fragment) {
    override fun getItemCount() = listGenres.size

    override fun createFragment(position: Int) =
        GenreFragmentChildIn.newInstance(listGenres[position].genre)
}