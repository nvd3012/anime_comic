package anime.comic.ui.home.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import anime.comic.R
import anime.comic.data.model.GenreModel
import anime.comic.databinding.FragmentGenreBinding
import anime.comic.databinding.FragmentGenreInBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.genre.adapter.GenreViewPagerAdapter
import anime.comic.ui.genre.viewmodel.GenreViewModel
import anime.comic.ui.genre.viewmodel.OnGenreEvent
import anime.comic.ui.search.activity.SearchActivity
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeInGenreFragment : BaseFragment<FragmentGenreInBinding, GenreViewModel>() {
    override val viewModel by viewModels<GenreViewModel>()
    override val layoutId = R.layout.fragment_genre_in

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleScope.launch {
                this@HomeInGenreFragment.viewModel.getGenreFromServer()?.let {
                    viewPager.adapter =
                        GenreInViewPagerAdapter(this@HomeInGenreFragment, it)
                    TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                        tab.text = it[position].genre
                    }.attach()
                }
            }
        }
        viewModel.onGenreEvent.observe(viewLifecycleOwner){
            if(it is OnGenreEvent.OnMoveToSearchScreen){
                startActivity(context?.let { context -> SearchActivity.createIntent(context) })
            }
        }
    }
}