package anime.comic.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ItemComicLatestBinding
import anime.comic.ui.base.LifecycleRecyclerAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.home.viewmodel.HomeViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull
import com.google.android.flexbox.FlexboxLayoutManager

class LatestAdapter(private val homeViewModel: HomeViewModel) :
    LifecycleRecyclerAdapter<LatestAdapter.LatestItemViewHolder>() {

    private var listData = listOf<ComicDetailData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LatestItemViewHolder(parent)

    override fun onBindViewHolder(holder: LatestItemViewHolder, position: Int) {
        holder.bind(listData[position], homeViewModel)
    }

    override fun getItemCount() = listData.size

    fun submitList(listData: List<ComicDetailData>) {
        this.listData = listData
        notifyDataSetChanged()
    }

    inner class LatestItemViewHolder(
        private val parent: ViewGroup,
        private val binding: ItemComicLatestBinding = ItemComicLatestBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {
        fun bind(data: ComicDetailData, viewModel: HomeViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                val lp = cstLayout.layoutParams
                if (lp is FlexboxLayoutManager.LayoutParams) {
                    lp.flexGrow = 1f
                }
                this.data = data
                this.viewModel = viewModel
            }
        }
    }
}