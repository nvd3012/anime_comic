package anime.comic.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import anime.comic.databinding.ItemSliderBinding
import anime.comic.util.extension.lifecycleOwnerOrNull
import com.smarteist.autoimageslider.SliderViewAdapter

class SliderAdapter : SliderViewAdapter<SliderAdapter.SliderAdapterVH>() {
    private var mSliderItems = mutableListOf<String>()

    fun submitList(data: List<String>) {
        mSliderItems.clear()
        mSliderItems.addAll(data)
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return mSliderItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup) = SliderAdapterVH(parent)

    override fun onBindViewHolder(viewHolder: SliderAdapter.SliderAdapterVH, position: Int) {
        viewHolder.binData(mSliderItems[position])
    }

    inner class SliderAdapterVH(
        parent: ViewGroup, private val binding: ItemSliderBinding = ItemSliderBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : SliderViewAdapter.ViewHolder(binding.root) {

        fun binData(url: String) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                imgUrl = url
            }
        }
    }
}