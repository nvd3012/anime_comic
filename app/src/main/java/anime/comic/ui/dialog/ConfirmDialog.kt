package anime.comic.ui.dialog

import androidx.fragment.app.FragmentManager
import anime.comic.data.model.AlertDialogModel

class ConfirmDialog(
    title: String,
    content: String,
    postEvent: () -> Unit = {},
    negEvent: () -> Unit = {}
) :
    BaseDialog() {
    override var alertDialogModel: AlertDialogModel? =
        AlertDialogModel(title, content, textPos = "Ok", textNeg = "Cancel", posEvent = {
            postEvent()
        }, negEvent = {
            negEvent()
        })

    override fun showDialog(fragmentManager: FragmentManager) {
        show(fragmentManager, ConfirmDialog::class.java.simpleName)
    }
}