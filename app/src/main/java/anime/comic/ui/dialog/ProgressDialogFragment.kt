package anime.comic.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import anime.comic.R

class ProgressDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val context = context ?: throw RuntimeException("context is not found")
        return Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.fragment_progress_dialog)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            isCancelable = false
        }
    }

    fun showDialogFragment(fragmentManager: FragmentManager) {
        show(fragmentManager, ProgressDialogFragment::class.java.simpleName)
    }

    fun cancelDialog(){
        dialog?.cancel()
    }

    companion object {
        fun newInstance() = ProgressDialogFragment()
    }
}