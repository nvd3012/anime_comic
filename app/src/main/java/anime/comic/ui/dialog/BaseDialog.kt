package anime.comic.ui.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import anime.comic.data.model.AlertDialogModel
import anime.comic.databinding.DialogAlertBinding
import anime.comic.util.autoCleared

abstract class BaseDialog : DialogFragment() {
    private var binding by autoCleared<DialogAlertBinding>()
    abstract var alertDialogModel: AlertDialogModel?

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val context = context ?: throw RuntimeException("context is not found")
        return Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            binding = DialogAlertBinding.inflate(LayoutInflater.from(context), null, false)
            setContentView(binding.root)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            binding.apply {
                alertDialogModel?.let { model ->
                    data = model
                    tvDialogNegative.setOnClickListener {
                        model.negEvent()
                        this@BaseDialog.dismiss()
                    }
                    tvDialogPositive.setOnClickListener {
                        model.posEvent()
                        this@BaseDialog.dismiss()
                    }
                }
            }
        }
    }

    abstract fun showDialog( fragmentManager: FragmentManager)
}


