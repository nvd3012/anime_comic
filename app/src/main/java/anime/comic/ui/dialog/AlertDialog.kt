package anime.comic.ui.dialog

import androidx.fragment.app.FragmentManager
import anime.comic.data.model.AlertDialogModel

class AlertDialog(title: String, content: String, posEvent: () -> Unit = {}) : BaseDialog() {
    override var alertDialogModel: AlertDialogModel? =
        AlertDialogModel(title, content, posEvent = {
            posEvent()
        })

    override fun showDialog(fragmentManager: FragmentManager) {
        show(fragmentManager, AlertDialog::class.java.simpleName)
    }
}