package anime.comic.ui.genre.adapter

data class GenreComicItemData(
    val id: String = "",
    val name: String = "",
    val genres: List<String> = listOf(),
    val thumbnail: String = "",
    val viewCount: String = "",
    val chapterCount: String = ""
) {
}