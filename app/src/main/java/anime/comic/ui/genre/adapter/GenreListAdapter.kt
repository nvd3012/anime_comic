package anime.comic.ui.genre.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ItemComicGenreBinding
import anime.comic.ui.base.LifecyclePagingAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.genre.viewmodel.GenreViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull

class GenreListAdapter(private val viewModel: GenreViewModel) :
    LifecyclePagingAdapter<ComicDetailData, GenreListAdapter.GenreItemViewHolder>(
        COMICS_COMPARATOR
    ) {

    override fun onBindViewHolder(holder: GenreItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, viewModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = GenreItemViewHolder(parent)

    companion object {
        private val COMICS_COMPARATOR = object : DiffUtil.ItemCallback<ComicDetailData>() {
            override fun areItemsTheSame(
                oldItem: ComicDetailData,
                newItem: ComicDetailData
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ComicDetailData,
                newItem: ComicDetailData
            ) = oldItem == newItem
        }
    }

    inner class GenreItemViewHolder(
        parent: ViewGroup,
        private val binding: ItemComicGenreBinding = ItemComicGenreBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {

        fun bind(data: ComicDetailData, viewModel: GenreViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                this.data = data
            }
        }
    }
}