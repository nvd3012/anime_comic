package anime.comic.ui.genre.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import anime.comic.R
import anime.comic.databinding.FragmentGenreBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.genre.adapter.GenreViewPagerAdapter
import anime.comic.ui.genre.viewmodel.GenreViewModel
import anime.comic.ui.genre.viewmodel.OnGenreEvent
import anime.comic.ui.search.activity.SearchActivity
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class GenreFragment : BaseFragment<FragmentGenreBinding, GenreViewModel>() {
    override val viewModel by viewModels<GenreViewModel>()
    override val layoutId = R.layout.fragment_genre

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            lifecycleScope.launch {
                this@GenreFragment.viewModel.getGenreFromServer()?.let {
                    viewPager.adapter =
                        GenreViewPagerAdapter(this@GenreFragment, it)
                    TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                        tab.text = it[position].genre
                    }.attach()
                }
            }
        }
        viewModel.onGenreEvent.observe(viewLifecycleOwner){
            if(it is OnGenreEvent.OnMoveToSearchScreen){
                startActivity(context?.let { context -> SearchActivity.createIntent(context) })
            }
        }
    }
}