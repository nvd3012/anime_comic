package anime.comic.ui.genre.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import anime.comic.data.model.ComicDetailData
import anime.comic.data.model.GenreModel
import anime.comic.domain.usecases.GenreUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.util.EventThrottleUtils
import anime.comic.util.toSingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class GenreViewModel @Inject constructor(private val genreUseCase: GenreUseCase) :
    BaseViewModel() {
    private val _onGenreEvent = MutableLiveData<OnGenreEvent>()
    val onGenreEvent = _onGenreEvent.toSingleEvent()

    private var currentResult: Flow<PagingData<ComicDetailData>>? = null

    fun getComicsByGenreName(genreName: String): Flow<PagingData<ComicDetailData>> {
        val lastResult = currentResult
        return if (lastResult != null) {
            lastResult
        } else {
            val newResult =
                genreUseCase.getPagingComicByGenreName(genreName)
                    .cachedIn(viewModelScope)
            currentResult = newResult
            newResult
        }
    }

    suspend fun getGenreFromServer(): List<GenreModel>? {
        var data: List<GenreModel>? = null
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
        genreUseCase.getGenresFromServer().onSuccess {
            data = it
            mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
        }.onFailure {
            mLoadingStateViewModel.postValue(ViewModelLoadState.Error)
        }
        return data
    }

    fun onClickDetail(comicDetailData: ComicDetailData) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onGenreEvent.postValue(OnGenreEvent.OnMoveToDetailScreen(comicDetailData))
        }
    }

    fun onClickSearch() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onGenreEvent.postValue(OnGenreEvent.OnMoveToSearchScreen)
        }
    }

    override fun onCleared() {
        Timber.e("clear genreviewmodel")
        super.onCleared()
    }
}

sealed class OnGenreEvent {
    object OnMoveToSearchScreen : OnGenreEvent()
    class OnMoveToDetailScreen(val data: ComicDetailData) : OnGenreEvent()
}