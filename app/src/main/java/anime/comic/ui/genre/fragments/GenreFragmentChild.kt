package anime.comic.ui.genre.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.ConcatAdapter
import anime.comic.R
import anime.comic.databinding.FragmentPagingBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.base.NativeAdAdapter
import anime.comic.ui.base.NativeAdType
import anime.comic.ui.base.PagingLoadStateAdapter
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.ui.genre.adapter.GenreListAdapter
import anime.comic.ui.genre.viewmodel.GenreViewModel
import anime.comic.ui.genre.viewmodel.OnGenreEvent
import anime.comic.util.Const
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class GenreFragmentChild :
    BaseFragment<FragmentPagingBinding, GenreViewModel>() {
    override val viewModel by viewModels<GenreViewModel>()
    override val layoutId = R.layout.fragment_paging
    private val genreName by lazy { arguments?.getString(Const.ID_GENRE) ?: "" }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val nativeAdSmall = NativeAdAdapter(NativeAdType.NATIVE_SMALL, viewModel)
        val nativeAdMedium = NativeAdAdapter(NativeAdType.NATIVE_MEDIUM, viewModel)
        val pagingAdapter = GenreListAdapter(viewModel)
        binding.apply {
            swipeRefresh.apply {
                isRefreshing = true
                setOnRefreshListener {
                    pagingAdapter.refresh()
                }
                postDelayed({
                    recyclerView.adapter =
                        ConcatAdapter(nativeAdSmall, pagingAdapter.withLoadStateHeaderAndFooter(
                            header = PagingLoadStateAdapter { pagingAdapter.retry() },
                            footer = PagingLoadStateAdapter { pagingAdapter.retry() }
                        ), nativeAdMedium)

                    this@GenreFragmentChild.viewModel.apply {
                        pagingAdapter.addLoadStateListener { loadState ->
                            handlePagingLoading(loadState)
                            if (loadState.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached) {
                                checkPagingDataEmpty(pagingAdapter.itemCount)
                            }
                        }

                        this@GenreFragmentChild.view?.findViewTreeLifecycleOwner()?.let {
                            onGenreEvent.observe(it, { event ->
                                if (event is OnGenreEvent.OnMoveToDetailScreen) {
                                    startActivity(
                                        ComicDetailActivity.createIntent(
                                            context,
                                            event.data
                                        )
                                    )
                                }
                            })
                        }
                    }
                    lifecycleScope.launch {
                        this@GenreFragmentChild.viewModel.getComicsByGenreName(genreName)
                            .collectLatest {
                                pagingAdapter.submitData(lifecycle, it)
                            }
                    }
                }, 1000)
            }
        }
    }

    companion object {
        fun newInstance(genre: String) = GenreFragmentChild().apply {
            arguments = Bundle().apply {
                putString(Const.ID_GENRE, genre)
            }
        }
    }
}