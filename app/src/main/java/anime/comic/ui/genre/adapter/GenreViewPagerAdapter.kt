package anime.comic.ui.genre.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import anime.comic.data.local.entity.GenreEntity
import anime.comic.data.model.GenreModel
import anime.comic.ui.genre.fragments.GenreFragmentChild

class GenreViewPagerAdapter(fragment: Fragment, private val listGenres: List<GenreModel>) : FragmentStateAdapter(fragment) {
    override fun getItemCount() = listGenres.size

    override fun createFragment(position: Int) =
        GenreFragmentChild.newInstance(listGenres[position].genre)
}