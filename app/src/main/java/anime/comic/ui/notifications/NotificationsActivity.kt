package anime.comic.ui.notifications

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import anime.comic.R
import anime.comic.databinding.ActivityNotificationBinding
import anime.comic.ui.base.BaseActivity

class NotificationsActivity : BaseActivity<ActivityNotificationBinding,NotificationsViewModel>() {
    override val viewModel: NotificationsViewModel by viewModels()
    override val layoutId = R.layout.activity_notification

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    companion object{
        fun createIntent(context : Context) = Intent(context,NotificationsActivity::class.java)
    }

}