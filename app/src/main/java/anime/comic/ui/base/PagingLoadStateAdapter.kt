package anime.comic.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import anime.comic.databinding.ItemLoadStateBinding
import anime.comic.util.extension.lifecycleOwnerOrNull
import timber.log.Timber

class PagingLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<PagingLoadStateAdapter.PagingLoadStateViewHolder>() {

    override fun onBindViewHolder(holder: PagingLoadStateViewHolder, loadState: LoadState) {
        Timber.e("paging string load $loadState")
        holder.bind(loadState, retry)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ) = PagingLoadStateViewHolder(parent)

    inner class PagingLoadStateViewHolder(
        parent: ViewGroup, private val binding: ItemLoadStateBinding = ItemLoadStateBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {

        fun bind(loadState: LoadState, retry: () -> Unit) {

            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                progressBar.isVisible = loadState is LoadState.Loading
                tvMesErr.isVisible = loadState !is LoadState.Loading
                tvMesErr.setOnClickListener {
                    retry()
                }
                executePendingBindings()
            }
        }
    }
}