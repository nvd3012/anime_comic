package anime.comic.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import anime.comic.databinding.NativeAdMediumBinding
import anime.comic.databinding.NativeAdSmallBinding
import anime.comic.util.extension.lifecycleOwnerOrNull
import anime.comic.util.extension.loadAds

class NativeAdAdapter(private val type: NativeAdType, private val viewModel: BaseViewModel) :
    LifecycleRecyclerAdapter<LifecycleViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = if (type.isSmall()) NativeAdSmallViewHolder(parent) else NativeAdMediumViewHolder(parent)

    override fun onBindViewHolder(holder: LifecycleViewHolder, position: Int) {
        when (holder) {
            is NativeAdSmallViewHolder -> holder.bind(viewModel)
            is NativeAdMediumViewHolder -> holder.bind(viewModel)
        }
    }

    override fun getItemCount() = 1

    inner class NativeAdSmallViewHolder(
        parent: ViewGroup, private val binding: NativeAdSmallBinding =
            NativeAdSmallBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) : LifecycleViewHolder(binding.root) {
        fun bind(viewModel: BaseViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                adsNativeSmall.loadAds(itemView.context)
                executePendingBindings()
            }
        }
    }

    inner class NativeAdMediumViewHolder(
        parent: ViewGroup, private val binding: NativeAdMediumBinding =
            NativeAdMediumBinding.inflate(LayoutInflater.from(parent.context), parent, false)

    ) : LifecycleViewHolder(binding.root) {
        fun bind(viewModel: BaseViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                adsNativeMedium.loadAds(itemView.context)
                executePendingBindings()
            }
        }
    }
}

enum class NativeAdType {
    NATIVE_SMALL,
    NATIVE_MEDIUM;

    fun isSmall() = this.name == NATIVE_SMALL.name
}