package anime.comic.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import anime.comic.BR
import anime.comic.ui.dialog.AlertDialog
import anime.comic.ui.dialog.ConfirmDialog
import anime.comic.ui.dialog.ProgressDialogFragment
import anime.comic.util.autoCleared
import kotlinx.coroutines.cancel
import timber.log.Timber

abstract class BaseFragment<T : ViewDataBinding, VM : BaseViewModel> : Fragment() {
    protected var binding by autoCleared<T>()

    protected abstract val viewModel: VM



    @get:LayoutRes
    protected abstract val layoutId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            setVariable(BR.viewModel, viewModel)
            root.isClickable = true
            executePendingBindings()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onHandleEvents.observe(viewLifecycleOwner, { event ->
            Timber.e("response AlertMessage Fragment $event")
            when (event) {
                is OnHandleEvents.OnAlertMessage -> {
                    AlertDialog(event.title ?: "Alert", event.content, event.posEvent)
                        .showDialog(childFragmentManager)
                }
                is OnHandleEvents.OnConfirmDialog -> {
                    ConfirmDialog(
                        event.title ?: "Confirm",
                        event.content,
                        event.posEvent,
                        event.negEvent
                    ).showDialog(
                        childFragmentManager
                    )
                }
//                is OnHandleEvents.OnShowLoading -> {
//                    progressDialogFragment.showDialogFragment(childFragmentManager)
//                }
//                is OnHandleEvents.OnHideLoading -> {
//                    try {
//                        progressDialogFragment.dismiss()
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
                is OnHandleEvents.OnBackNavigation -> {
                    requireActivity().onBackPressed()
                    Timber.e("backPressed")
                }
            }
        })
    }

    override fun onDestroy() {
        lifecycleScope.cancel()
        super.onDestroy()
    }
}