package anime.comic.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import anime.comic.R
import anime.comic.util.EventThrottleUtils

class MaintenanceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maintenance)

    }

    companion object {
        fun createIntent(context: Context) = Intent(context, MaintenanceActivity::class.java)
    }

    override fun onBackPressed() {
        EventThrottleUtils.doubleToAction({
            finishAndRemoveTask()
        }, {
            Toast.makeText(this, getString(R.string.text_tap_again), Toast.LENGTH_SHORT).show()
        })
    }
}