package anime.comic.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import anime.comic.BR
import anime.comic.ui.dialog.AlertDialog
import timber.log.Timber

abstract class BaseActivity<T : ViewDataBinding, S : BaseViewModel> :
    AppCompatActivity() {
    lateinit var binding: T
    abstract val viewModel: S
//    private val progressDialogFragment = ProgressDialogFragment.newInstance()

    @get:LayoutRes
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (::binding.isInitialized.not()) {
            binding = DataBindingUtil.setContentView(this, layoutId)
            binding.apply {
                lifecycleOwner = this@BaseActivity
                setVariable(BR.viewModel, viewModel)
                root.isClickable = true
            }
        }
        viewModel.onHandleEvents.observe(this, { event ->
            Timber.e("response AlertMessage activity")
            when (event) {
                is OnHandleEvents.OnAlertMessage -> {
                    AlertDialog(event.title ?: "Alert", event.content, event.posEvent)
                        .showDialog(supportFragmentManager)
                }
                is OnHandleEvents.OnToastMessage -> {
                    Toast.makeText(applicationContext, event.content, Toast.LENGTH_SHORT).show()
                }
//                is OnHandleEvents.OnShowLoading -> {
//                    progressDialogFragment.showDialogFragment(supportFragmentManager)
//                }
//                is OnHandleEvents.OnHideLoading -> {
//                    progressDialogFragment.cancelDialog()
//                }
                is OnHandleEvents.OnBackNavigation -> {
                    finish()
                }
            }
        })
    }
}