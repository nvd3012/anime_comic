package anime.comic.ui.base

import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil

abstract class LifecyclePagingAdapter<T : Any, VH : LifecycleViewHolder>(diffCallback: DiffUtil.ItemCallback<T>) :
    PagingDataAdapter<T, VH>(diffCallback) {
    override fun onViewAttachedToWindow(holder: VH) {
        super.onViewAttachedToWindow(holder)
        holder.onCreate()
    }

    override fun onViewDetachedFromWindow(holder: VH) {
        super.onViewDetachedFromWindow(holder)
        holder.onDestroy()
    }
}