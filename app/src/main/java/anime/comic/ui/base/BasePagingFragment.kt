package anime.comic.ui.base

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import anime.comic.R
import anime.comic.databinding.FragmentPagingBinding

abstract class BasePagingFragment<T : Any, VM : BaseViewModel, VH : LifecycleViewHolder> :
    BaseFragment<FragmentPagingBinding, VM>() {
    abstract val pagingAdapter: LifecyclePagingAdapter<T, VH>
    override val layoutId = R.layout.fragment_paging

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            swipeRefresh.setOnRefreshListener {
                pagingAdapter.refresh()
            }
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = pagingAdapter
                setHasFixedSize(true)
            }
        }
    }
}