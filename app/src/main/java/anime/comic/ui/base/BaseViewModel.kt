package anime.comic.ui.base

import androidx.lifecycle.*
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import anime.comic.data.toBaseException
import anime.comic.util.extension.OffsetChangedState
import anime.comic.util.toSingleEvent
import kotlinx.coroutines.*
import timber.log.Timber
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseViewModel : ViewModel() {
    private val _onHandleEvent = MutableLiveData<OnHandleEvents>()
    val onHandleEvents = _onHandleEvent.toSingleEvent()

    private val _offsetAppBarState = MutableLiveData<OffsetChangedState>()
    val offsetChangedState: LiveData<OffsetChangedState> = _offsetAppBarState

    protected val mLoadingStateViewModel: MutableLiveData<ViewModelLoadState> =
        MutableLiveData(ViewModelLoadState.None)
    val loadingStateViewModel: LiveData<ViewModelLoadState> =
        mLoadingStateViewModel.distinctUntilChanged()

    protected val mPagingLoadingState = MutableLiveData<PagingLoadState>(PagingLoadState.None)
    val pagingLoadingState: LiveData<PagingLoadState> = mPagingLoadingState

    protected val mPagingErrorMessage = MutableLiveData<String>()
    val pagingErrorMessage: LiveData<String> = mPagingErrorMessage.distinctUntilChanged()

    fun handlePagingLoading(loadState: CombinedLoadStates) {
        when (val stt = loadState.source.refresh) {
            // Only show the list if refresh succeeds.
            is LoadState.NotLoading -> mPagingLoadingState.postValue(PagingLoadState.NotLoading)
            // Show loading spinner during initial load or refresh.
            is LoadState.Loading -> mPagingLoadingState.postValue(PagingLoadState.Loading)
            is LoadState.Error -> {
                mPagingErrorMessage.postValue("Something error!")
                mPagingLoadingState.postValue(PagingLoadState.Error)
            }
        }

        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
            ?: loadState.append as? LoadState.Error
            ?: loadState.prepend as? LoadState.Error
            ?: loadState.source.refresh as? LoadState.Error
        if (errorState != null) {
            // Show the retry state if initial load fails.
            mPagingErrorMessage.postValue("Something error!")
            mPagingLoadingState.postValue(PagingLoadState.Error)
        }
    }

    fun checkPagingDataEmpty(itemCount: Int) {
        if (itemCount > 0) return
        mPagingLoadingState.postValue(PagingLoadState.Error)
        mPagingErrorMessage.postValue("Data is empty!")
    }

    fun clearPagingState() {
        mPagingErrorMessage.postValue("")
        mPagingLoadingState.postValue(PagingLoadState.None)
    }

    sealed class ViewModelLoadState {
        object None : ViewModelLoadState()
        object Loading : ViewModelLoadState()
        object Loaded : ViewModelLoadState()
        object Empty : ViewModelLoadState()
        object Error : ViewModelLoadState()

        fun isNone(): Boolean = this is None
        fun isLoading(): Boolean = this is Loading
        fun isError(): Boolean = this is Error
        fun isEmpty(): Boolean = this is Empty
        fun isLoaded(): Boolean = this is Loaded
    }

    sealed class PagingLoadState {
        object None : PagingLoadState()
        object NotLoading : PagingLoadState()
        object Loading : PagingLoadState()
        object Error : PagingLoadState()

        fun isNone(): Boolean = this is None
        fun isLoading(): Boolean = this is Loading
        fun isError(): Boolean = this is Error
        fun isNotLoading(): Boolean = this is NotLoading
    }

    private fun coroutineException(showDialog: Boolean) =
        CoroutineExceptionHandler { _, throwable ->
            viewModelScope.launch {
                handleError(throwable, showDialog)
            }
        }

    protected fun viewModelScopeException(showDialog: Boolean = false) =
        viewModelScope + coroutineException(showDialog)

    open suspend fun handleError(throwable: Throwable, showDialog: Boolean) {
        throwable.printStackTrace()
        withContext(Dispatchers.Main) {
            val errorMessage: String
            when (throwable) {
                is ConnectException, is UnknownHostException -> {
                    errorMessage = "Connection error occur!"
                }
                is SocketTimeoutException -> {
                    errorMessage = "Socket timeout!"
                }
                else -> {
                    val baseException = throwable.toBaseException()
                    errorMessage = when (baseException.httpCode) {
                        HttpURLConnection.HTTP_UNAUTHORIZED -> {
                            baseException.message ?: "Unknown"
                        }
                        HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                            baseException.message ?: "Unknown"
                        }
                        else -> {
                            "Unknown error! ${baseException.httpCode}"
                        }
                    }
                }
            }
            if (showDialog) {
                showAlert(content = errorMessage)
            }
            mLoadingStateViewModel.value = ViewModelLoadState.Error
            hideLoading()
        }
    }

    fun showAlert(title: String? = null, content: String, posEvent: () -> Unit = {}) {
        _onHandleEvent.value = OnHandleEvents.OnAlertMessage(title, content, posEvent)
    }

    fun showConfirmDialog(
        title: String? = null,
        content: String,
        posEvent: () -> Unit = {},
        negEvent: () -> Unit = {}
    ) {
        _onHandleEvent.value =
            OnHandleEvents.OnConfirmDialog(title, content, posEvent, negEvent)
    }

    fun showLoading() {
        _onHandleEvent.value = OnHandleEvents.OnShowLoading
    }

    fun hideLoading() {
        _onHandleEvent.value = OnHandleEvents.OnHideLoading
    }

    fun onBackNavigationActivity() {
        Timber.e("onBack")
        _onHandleEvent.value = OnHandleEvents.OnBackNavigation
    }

    fun handleOffsetChangedState(offsetChangedState: OffsetChangedState) {
        _offsetAppBarState.postValue(offsetChangedState)
    }

    fun toastMessage(content: String){
        _onHandleEvent.value = OnHandleEvents.OnToastMessage(content)
    }
}

class DefaultViewModel : BaseViewModel()

sealed class OnHandleEvents {
    class OnAlertMessage(val title: String?, val content: String, val posEvent: () -> Unit) :
        OnHandleEvents()

    class OnConfirmDialog(
        val title: String?,
        val content: String,
        val posEvent: () -> Unit,
        val negEvent: () -> Unit
    ) : OnHandleEvents()

    class OnToastMessage(val content: String) : OnHandleEvents()
    object OnShowLoading : OnHandleEvents()
    object OnHideLoading : OnHandleEvents()
    object OnBackNavigation : OnHandleEvents()
}