package anime.comic.ui.library

enum class LibraryTabItemEnum(val itemName: String) {
    RECENT("Recent"),
    SUBSCRIBED("Subscribed"),
    DOWNLOAD("Downloads");

    companion object {
        fun getItem(ordinal: Int) = values()
            .getOrElse(ordinal) { RECENT }
    }
}