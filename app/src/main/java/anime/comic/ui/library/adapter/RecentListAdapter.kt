package anime.comic.ui.library.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.databinding.ItemRecentBinding
import anime.comic.ui.base.LifecycleRecyclerAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.library.viewmodel.LibraryViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull

class RecentListAdapter(private val viewModel: LibraryViewModel) :
    LifecycleRecyclerAdapter<RecentListAdapter.RecentViewHolder>() {

    private val differ = AsyncListDiffer(this, DIFF_UTIL)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RecentViewHolder(parent)

    override fun onBindViewHolder(holder: RecentViewHolder, position: Int) {
        holder.bind(differ.currentList[position], viewModel)
    }

    override fun getItemCount() = differ.currentList.size

    fun submitList(data: List<ChapterRecentEntity>) {
        differ.submitList(data)
    }

    companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<ChapterRecentEntity>() {
            override fun areItemsTheSame(
                oldItem: ChapterRecentEntity,
                newItem: ChapterRecentEntity
            ) = oldItem.idComic == newItem.idComic

            override fun areContentsTheSame(
                oldItem: ChapterRecentEntity,
                newItem: ChapterRecentEntity
            ) = oldItem == newItem
        }
    }

    inner class RecentViewHolder(
        parent: ViewGroup, val binding: ItemRecentBinding = ItemRecentBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {
        fun bind(data: ChapterRecentEntity, viewModel: LibraryViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                this.data = data
                executePendingBindings()
            }
        }
    }
}