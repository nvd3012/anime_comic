package anime.comic.ui.library.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import anime.comic.R
import anime.comic.databinding.FragmentLibraryChildBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.base.NativeAdAdapter
import anime.comic.ui.base.NativeAdType
import anime.comic.ui.comicreader.activity.ComicReaderActivity
import anime.comic.ui.library.LibraryTabItemEnum
import anime.comic.ui.library.adapter.RecentListAdapter
import anime.comic.ui.library.viewmodel.LibraryViewModel
import anime.comic.ui.library.viewmodel.OnLibraryEvent

class LibraryFragmentRecent : BaseFragment<FragmentLibraryChildBinding, LibraryViewModel>() {
    override val viewModel by activityViewModels<LibraryViewModel>()
    override val layoutId = R.layout.fragment_library_child

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val nativeAdSmall = NativeAdAdapter(NativeAdType.NATIVE_SMALL, viewModel)
        val nativeAdMedium = NativeAdAdapter(NativeAdType.NATIVE_MEDIUM, viewModel)
        val recentAdapter = RecentListAdapter(viewModel)
        binding.apply {
            typeScreen = LibraryTabItemEnum.RECENT
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = ConcatAdapter(
                    nativeAdSmall,
                    recentAdapter, nativeAdMedium
                )
            }
        }

        viewModel.listChapterRecent.observe(viewLifecycleOwner) {
            recentAdapter.submitList(it)
        }

        viewModel.onLibraryEvent.observe(viewLifecycleOwner) {
            if (it is OnLibraryEvent.OnMoveToReader) {
                startActivity(context?.let { context ->
                    ComicReaderActivity.createIntent(
                        context,
                        it.data.comicDetailData,
                        it.data.chapterItemData
                    )
                })
            }
        }
    }

    companion object {
        fun newInstances() = LibraryFragmentRecent()
    }
}