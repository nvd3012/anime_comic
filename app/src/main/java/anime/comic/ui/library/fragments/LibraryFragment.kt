package anime.comic.ui.library.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import anime.comic.R
import anime.comic.databinding.FragmentLibraryBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.library.LibraryTabItemEnum
import anime.comic.ui.library.adapter.LibraryViewPagerAdapter
import anime.comic.ui.library.viewmodel.LibraryViewModel
import com.google.android.material.tabs.TabLayoutMediator

class LibraryFragment : BaseFragment<FragmentLibraryBinding, LibraryViewModel>() {
    override val viewModel by activityViewModels<LibraryViewModel>()
    override val layoutId = R.layout.fragment_library

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            viewPager.postDelayed({
                viewPager.adapter = LibraryViewPagerAdapter(this@LibraryFragment)
                TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                    resources.apply {
                        this@LibraryFragment.viewModel.clearView()
                        tab.text = LibraryTabItemEnum.getItem(position).itemName
                    }
                }.attach()

                viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                    override fun onPageSelected(position: Int) {
                        this@LibraryFragment.viewModel.clearView()
                        super.onPageSelected(position)
                        this@LibraryFragment.viewModel.setTypeScreen(LibraryTabItemEnum.getItem(position))
                    }
                })
            },500)
        }
    }
}