package anime.comic.ui.library.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import anime.comic.R
import anime.comic.databinding.FragmentLibraryChildBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.base.NativeAdAdapter
import anime.comic.ui.base.NativeAdType
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.ui.library.LibraryTabItemEnum
import anime.comic.ui.library.adapter.SubscribedListAdapter
import anime.comic.ui.library.viewmodel.LibraryViewModel
import anime.comic.ui.library.viewmodel.OnLibraryEvent
import anime.comic.util.extension.getWidthScreen

class LibraryFragmentBookmarks :
    BaseFragment<FragmentLibraryChildBinding, LibraryViewModel>() {

    override val viewModel by activityViewModels<LibraryViewModel>()
    override val layoutId = R.layout.fragment_library_child

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let { context ->
            val widthItem = context.getWidthScreen() / SPAN_COUNT
            val bookmarksAdapter = SubscribedListAdapter(viewModel, widthItem)
            val nativeAdSmall = NativeAdAdapter(NativeAdType.NATIVE_SMALL, viewModel)
            val nativeAdMedium = NativeAdAdapter(NativeAdType.NATIVE_MEDIUM, viewModel)
            val concatAdapter = ConcatAdapter(
                nativeAdSmall,
                bookmarksAdapter, nativeAdMedium
            )
            binding.apply {
                typeScreen = LibraryTabItemEnum.SUBSCRIBED
                recyclerView.apply {
                    adapter = concatAdapter
                    layoutManager = GridLayoutManager(context, SPAN_COUNT).apply {
                        spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                            override fun getSpanSize(position: Int): Int {
                                return when(position){
                                    0,concatAdapter.itemCount-1 -> 3
                                    else -> 1
                                }
                            }
                        }
                    }
                }
            }

            viewModel.listBookmarks.observe(viewLifecycleOwner) {
                bookmarksAdapter.submitList(it)
            }

            viewModel.onLibraryEvent.observe(viewLifecycleOwner) {
                if (it is OnLibraryEvent.OnMoveToComicDetail) {
                    startActivity(
                        ComicDetailActivity.createIntent(
                            context,
                            it.data
                        )
                    )
                }
            }
        }
    }

    companion object {
        private const val SPAN_COUNT = 3
        fun newInstances() = LibraryFragmentBookmarks()
    }
}