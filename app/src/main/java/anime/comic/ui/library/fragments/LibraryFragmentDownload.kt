package anime.comic.ui.library.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import anime.comic.R

class LibraryFragmentDownload : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.developing_layout,container,false)
    }

    companion object {
        fun newInstance() = LibraryFragmentDownload()
    }
}