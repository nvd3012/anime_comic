package anime.comic.ui.library.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import anime.comic.data.local.entity.BookmarksEntity
import anime.comic.databinding.ItemSubscribeBinding
import anime.comic.ui.base.LifecycleRecyclerAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.library.viewmodel.LibraryViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull
import com.google.android.flexbox.FlexboxLayoutManager

class SubscribedListAdapter(private val viewModel: LibraryViewModel,private val widthItem: Int) :
    LifecycleRecyclerAdapter<SubscribedListAdapter.SubscribedViewHolder>() {

    private val differ = AsyncListDiffer(this, DIFF_UTIL)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SubscribedViewHolder(parent)

    override fun onBindViewHolder(holder: SubscribedViewHolder, position: Int) {
        holder.bind(differ.currentList[position], viewModel)
    }

    override fun getItemCount() = differ.currentList.size

    fun submitList(data: List<BookmarksEntity>) {
        differ.submitList(data)
    }

    companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<BookmarksEntity>() {
            override fun areItemsTheSame(
                oldItem: BookmarksEntity,
                newItem: BookmarksEntity
            ) = oldItem.idComic == newItem.idComic

            override fun areContentsTheSame(
                oldItem: BookmarksEntity,
                newItem: BookmarksEntity
            ) = oldItem == newItem
        }
    }

    inner class SubscribedViewHolder(
        parent: ViewGroup, val binding: ItemSubscribeBinding = ItemSubscribeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {
        fun bind(data: BookmarksEntity, viewModel: LibraryViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                val lp = imgComic.layoutParams
                lp.width = widthItem
                this.viewModel = viewModel
                this.data = data
                executePendingBindings()
            }
        }
    }
}