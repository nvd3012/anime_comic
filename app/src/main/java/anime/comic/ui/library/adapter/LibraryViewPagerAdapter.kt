package anime.comic.ui.library.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import anime.comic.ui.library.LibraryTabItemEnum
import anime.comic.ui.library.fragments.LibraryFragmentDownload
import anime.comic.ui.library.fragments.LibraryFragmentRecent
import anime.comic.ui.library.fragments.LibraryFragmentBookmarks

class LibraryViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount() = LibraryTabItemEnum.values().size

    override fun createFragment(position: Int): Fragment {
        return when (LibraryTabItemEnum.getItem(position)) {
            LibraryTabItemEnum.RECENT -> LibraryFragmentRecent.newInstances()
            LibraryTabItemEnum.SUBSCRIBED -> LibraryFragmentBookmarks.newInstances()
            LibraryTabItemEnum.DOWNLOAD -> LibraryFragmentDownload.newInstance()
        }
    }
}