package anime.comic.ui.library.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import anime.comic.data.local.entity.BookmarksEntity
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.data.local.entity.LibraryItem
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.usecases.LibraryUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.ui.library.LibraryTabItemEnum
import anime.comic.util.EventThrottleUtils
import anime.comic.util.toLiveData
import anime.comic.util.toSingleEvent
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
@HiltViewModel
class LibraryViewModel @Inject constructor(private val libraryUseCase: LibraryUseCase) :
    BaseViewModel() {
    private val _onLibraryEvent = MutableLiveData<OnLibraryEvent>()
    val onLibraryEvent = _onLibraryEvent.toSingleEvent()

    private var typeScreen = LibraryTabItemEnum.RECENT

    private val listItemRemove = mutableListOf<LibraryItem>()

    private val _isEdit = MutableLiveData(true)
    val isEdit = _isEdit.toLiveData()

    private val _isSelectAll = MutableLiveData(false)
    val isSelectAll = _isSelectAll.toLiveData()

    val listChapterRecent by lazy { libraryUseCase.getListChaptersRecent() }
    val listBookmarks by lazy { libraryUseCase.getListBookmarks() }

    private val isRecentEmpty by lazy { listChapterRecent.map { it.isEmpty() } }
    private val isBookmarksEmpty by lazy { listBookmarks.map { it.isEmpty() } }
    private val isDownloadedEmpty by lazy { listBookmarks.map { it.isEmpty() } }

    fun setTypeScreen(typeScreen: LibraryTabItemEnum) {
        this.typeScreen = typeScreen
    }

    fun onClickToReader(data: ChapterRecentEntity) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onLibraryEvent.postValue(OnLibraryEvent.OnMoveToReader(data))
        }
    }

    fun onClickToDetail(data: ComicDetailData) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onLibraryEvent.postValue(OnLibraryEvent.OnMoveToComicDetail(data))
        }
    }

    fun setEdit() {
        _isEdit.value?.let {
            if (!it) {
                clearView()
            } else {
                _isEdit.postValue(!it)
            }
        }
    }

    fun clearView() {
        _isSelectAll.postValue(false)
        listItemRemove.clear()
        _isEdit.postValue(true)
    }

    fun onItemSelected(view: View, data: LibraryItem) {
        view as MaterialCheckBox
        if (view.isChecked) {
            listItemRemove.add(data)
        } else {
            listItemRemove.remove(data)
        }
    }

    fun onClickSelectAll() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _isSelectAll.value?.let {
                listItemRemove.clear()
                when (typeScreen) {
                    LibraryTabItemEnum.RECENT -> {
                        listChapterRecent.value?.let { data ->
                            listItemRemove.addAll(data)
                        }
                    }
                    LibraryTabItemEnum.SUBSCRIBED -> {
                        listBookmarks.value?.let { data ->
                            listItemRemove.addAll(data)
                        }
                    }
                    LibraryTabItemEnum.DOWNLOAD -> {

                    }
                }
                _isSelectAll.postValue(!it)
            }
        }
    }

    fun onClickDelete() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScopeException().launch {
                if (listItemRemove.isNotEmpty()) {
                    when (typeScreen) {
                        LibraryTabItemEnum.RECENT -> {
                            viewModelScope.launch {
                                if (isSelectAll.value == true) {
                                    libraryUseCase.deleteAllChapterRecent()
                                } else {
                                    libraryUseCase.deleteChaptersRecent(listItemRemove as List<ChapterRecentEntity>)
                                }
                            }
                        }
                        LibraryTabItemEnum.SUBSCRIBED -> {
                            listItemRemove.forEach { data ->
                                data as BookmarksEntity
                                FirebaseMessaging.getInstance()
                                    .unsubscribeFromTopic(data.comicDetailData.code)
                                    .addOnCompleteListener {
                                        viewModelScope.launch {
                                            libraryUseCase.deleteBookmarks(data)
                                        }
                                    }
                            }
                        }
                        LibraryTabItemEnum.DOWNLOAD -> {

                        }
                    }

                }
            }
        }
    }

    fun isEmpty(typeScreen: LibraryTabItemEnum?) = when (typeScreen) {
        LibraryTabItemEnum.DOWNLOAD -> isDownloadedEmpty
        LibraryTabItemEnum.SUBSCRIBED -> isBookmarksEmpty
        else -> isRecentEmpty
    }
}

sealed class OnLibraryEvent {
    class OnMoveToReader(val data: ChapterRecentEntity) : OnLibraryEvent()
    class OnMoveToComicDetail(val data: ComicDetailData) : OnLibraryEvent()
}