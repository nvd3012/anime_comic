package anime.comic.ui.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import anime.comic.data.model.ComicDetailData
import anime.comic.databinding.ItemComicSearchBinding
import anime.comic.ui.base.LifecyclePagingAdapter
import anime.comic.ui.base.LifecycleViewHolder
import anime.comic.ui.search.viewmodel.SearchViewModel
import anime.comic.util.extension.lifecycleOwnerOrNull

class SearchListAdapter(private val viewModel: SearchViewModel) :
    LifecyclePagingAdapter<ComicDetailData, SearchListAdapter.SearchItemViewHolder>(
        COMICS_COMPARATOR
    ) {

    override fun onBindViewHolder(holder: SearchItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it,viewModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SearchItemViewHolder(parent)

    companion object {
        private val COMICS_COMPARATOR = object : DiffUtil.ItemCallback<ComicDetailData>() {
            override fun areItemsTheSame(
                oldItem: ComicDetailData,
                newItem: ComicDetailData
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ComicDetailData,
                newItem: ComicDetailData
            ) = oldItem == newItem
        }
    }

    inner class SearchItemViewHolder(
        parent: ViewGroup,
        private val binding: ItemComicSearchBinding = ItemComicSearchBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    ) : LifecycleViewHolder(binding.root) {

        fun bind(data: ComicDetailData, viewModel: SearchViewModel) {
            binding.apply {
                lifecycleOwner = itemView.lifecycleOwnerOrNull()
                this.viewModel = viewModel
                this.data = data
            }
        }
    }
}