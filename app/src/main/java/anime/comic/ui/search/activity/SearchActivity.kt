package anime.comic.ui.search.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import anime.comic.R
import anime.comic.databinding.ActivitySearchBinding
import anime.comic.ui.base.BaseActivity
import anime.comic.ui.comicdetail.activity.ComicDetailActivity
import anime.comic.ui.search.fragments.FragmentSearch
import anime.comic.ui.search.viewmodel.OnSearchScreenEvent
import anime.comic.ui.search.viewmodel.SearchViewModel
import anime.comic.util.FragmentUtils
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class SearchActivity : BaseActivity<ActivitySearchBinding, SearchViewModel>() {
    override val viewModel by viewModels<SearchViewModel>()
    override val layoutId = R.layout.activity_search

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onSearchScreenEvent.observe(this) {
            Timber.e("onClick $it")

            when (it) {
                is OnSearchScreenEvent.OnMoveToDetail -> {
                    startActivity(
                        ComicDetailActivity.createIntent(
                            applicationContext,
                            it.comicDetailData
                        )
                    )
                }
                OnSearchScreenEvent.OnClickCancelSearch -> {
                    onBackPressed()
                }
                is OnSearchScreenEvent.OnSearch -> {
                    binding.tvInputSearch.setText(it.keyword)
                }
                else -> {
                }
            }
        }
        val fragmentSearch = FragmentSearch.newInstance()
        FragmentUtils.replaceFragment(supportFragmentManager, R.id.nav_container, fragmentSearch)
    }

    override fun onBackPressed() {
        finish()
    }

    companion object {
        fun createIntent(context: Context) = Intent(context, SearchActivity::class.java)
    }
}