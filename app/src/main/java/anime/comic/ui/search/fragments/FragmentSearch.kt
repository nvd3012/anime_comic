package anime.comic.ui.search.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.paging.LoadState
import androidx.recyclerview.widget.ConcatAdapter
import anime.comic.R
import anime.comic.databinding.FragmentPagingBinding
import anime.comic.ui.base.BaseFragment
import anime.comic.ui.base.NativeAdAdapter
import anime.comic.ui.base.NativeAdType
import anime.comic.ui.base.PagingLoadStateAdapter
import anime.comic.ui.search.adapter.SearchListAdapter
import anime.comic.ui.search.viewmodel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FragmentSearch : BaseFragment<FragmentPagingBinding, SearchViewModel>() {
    override val viewModel: SearchViewModel by activityViewModels()
    override val layoutId = R.layout.fragment_paging

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagingAdapter = SearchListAdapter(viewModel)
        val nativeAdSmall = NativeAdAdapter(NativeAdType.NATIVE_SMALL, viewModel)
        val nativeAdMedium = NativeAdAdapter(NativeAdType.NATIVE_MEDIUM, viewModel)
        binding.apply {
            swipeRefresh.setOnRefreshListener {
                pagingAdapter.refresh()
            }
            recyclerView.adapter =
                ConcatAdapter(nativeAdSmall, pagingAdapter.withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter { pagingAdapter.retry() },
                    footer = PagingLoadStateAdapter { pagingAdapter.retry() }
                ), nativeAdMedium)
        }

        pagingAdapter.addLoadStateListener { loadState ->
            viewModel.handlePagingLoading(loadState)
            if (loadState.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached) {
                viewModel.checkPagingDataEmpty(pagingAdapter.itemCount)
            }
        }

        viewModel.resultSearch.observe(viewLifecycleOwner) {
            pagingAdapter.submitData(lifecycle, it)
        }
    }


    companion object {
        fun newInstance() = FragmentSearch()
    }
}