package anime.comic.ui.search.viewmodel

import android.text.Editable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.usecases.SearchUseCase
import anime.comic.ui.base.BaseViewModel
import anime.comic.util.EventThrottleUtils
import anime.comic.util.toLiveData
import anime.comic.util.toSingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val searchUseCase: SearchUseCase) :
    BaseViewModel() {
    private var currentQueryValue: String? = null
    private var currentSearchResult: Flow<PagingData<ComicDetailData>>? = null

    private val _onSearchScreenEvent = MutableLiveData<OnSearchScreenEvent>()
    val onSearchScreenEvent = _onSearchScreenEvent.toSingleEvent()

    private val _isViewDefault = MutableLiveData(false)
    val isViewDefault = _isViewDefault.toLiveData()

    val listSearchHistory = searchUseCase.getListSearchHistory()

    private val _queryKeyword = MutableLiveData<String>()

    val resultSearch = _queryKeyword.switchMap { keyword ->
        mLoadingStateViewModel.postValue(ViewModelLoadState.Loading)
        val lastResult = currentSearchResult
        if (keyword == currentQueryValue && lastResult != null) {
            mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
            mPagingLoadingState.postValue(PagingLoadState.NotLoading)
            lastResult.asLiveData()
        } else {
            currentQueryValue = keyword
            viewModelScope.launch {
                searchUseCase.addSearchHistory(keyword)
            }
            val newResult =
                searchUseCase.getPagingComicByKeyword(keyword)
                    .cachedIn(viewModelScope)
            currentSearchResult = newResult
            mLoadingStateViewModel.postValue(ViewModelLoadState.Loaded)
            newResult.asLiveData()
        }
    }

    val searchComic = fun(keyword: String) {
        if (keyword.isEmpty()) return
        _queryKeyword.postValue(keyword)
    }


    fun onTextChange(keyword: Editable) {
        _isViewDefault.postValue(!(keyword.isEmpty() || keyword.isBlank()))
    }

    fun onClickCancelSearch() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onSearchScreenEvent.postValue(OnSearchScreenEvent.OnClickCancelSearch)
        }
    }

    fun onClickItemSearch(comicDetailData: ComicDetailData) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onSearchScreenEvent.postValue(OnSearchScreenEvent.OnMoveToDetail(comicDetailData))
        }
    }

    fun onClearText() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onSearchScreenEvent.postValue(OnSearchScreenEvent.OnClearText)
            mLoadingStateViewModel.postValue(ViewModelLoadState.None)
            clearPagingState()
        }
    }

    fun onClickClearSearchHistory() {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            viewModelScope.launch {
                searchUseCase.deleteSearchHistory()
            }
        }
    }

    val onClickChipItem = fun(keyword: String) {
        EventThrottleUtils.request(coroutineScope = viewModelScope) {
            _onSearchScreenEvent.postValue(OnSearchScreenEvent.OnSearch(keyword))
            searchComic(keyword)
        }
    }

    companion object {
        const val CHIP_COUNT = 15
    }
}

sealed class OnSearchScreenEvent {
    object OnClickCancelSearch : OnSearchScreenEvent()
    object OnClearText : OnSearchScreenEvent()
    class OnMoveToDetail(val comicDetailData: ComicDetailData) : OnSearchScreenEvent()
    class OnSearch(val keyword: String) : OnSearchScreenEvent()

    fun isClearText() = this is OnClearText
}



