package anime.comic.domain.usecases

import anime.comic.data.Resource
import anime.comic.data.local.entity.AppConfigEntity
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.repositories.ComicRepository
import anime.comic.domain.repositories.HomeRepository
import anime.comic.domain.repositories.UserRepository
import javax.inject.Inject

interface FcmUseCase {
    suspend fun getComicDetail(idComic: String): Resource<ComicDetailData>
    suspend fun isLogin(): Boolean
    suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity)
}

class FcmUseCaseImpl @Inject constructor(
    private val comicRepository: ComicRepository,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository
) : FcmUseCase {
    override suspend fun getComicDetail(idComic: String) = comicRepository.getDetailComic(idComic)

    override suspend fun isLogin() = userRepository.isUserLogged()
    override suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity) =
        homeRepository.upsertAppConfig(upsert)

}