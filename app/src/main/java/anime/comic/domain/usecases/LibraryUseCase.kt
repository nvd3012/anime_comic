package anime.comic.domain.usecases

import androidx.lifecycle.LiveData
import anime.comic.data.local.entity.BookmarksEntity
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.domain.repositories.LibraryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface LibraryUseCase {
    fun getListBookmarks(): LiveData<List<BookmarksEntity>>
    suspend fun deleteBookmarks(data: BookmarksEntity)
    suspend fun deleteBookmarks(data: List<BookmarksEntity>)
    fun getListChaptersRecent(): LiveData<List<ChapterRecentEntity>>
    suspend fun deleteChaptersRecent(data: ChapterRecentEntity)
    suspend fun deleteChaptersRecent(data: List<ChapterRecentEntity>)
    suspend fun deleteAllChapterRecent()
}

class LibraryUseCaseImpl @Inject constructor(private val libraryRepository: LibraryRepository) :
    LibraryUseCase {
    override fun getListBookmarks(): LiveData<List<BookmarksEntity>> =
        libraryRepository.getListBookmarks()

    override suspend fun deleteBookmarks(data: BookmarksEntity) =
        withContext(Dispatchers.IO) {
            libraryRepository.deleteBookmarks(data)
        }

    override suspend fun deleteBookmarks(data: List<BookmarksEntity>) =
        withContext(Dispatchers.IO) {
            libraryRepository.deleteBookmarks(data)
        }

    override fun getListChaptersRecent(): LiveData<List<ChapterRecentEntity>> =
        libraryRepository.getListChaptersRecent()

    override suspend fun deleteChaptersRecent(data: ChapterRecentEntity) =
        withContext(Dispatchers.IO) {
            libraryRepository.deleteChaptersRecent(data)
        }

    override suspend fun deleteChaptersRecent(data: List<ChapterRecentEntity>) =
        withContext(Dispatchers.IO) {
            libraryRepository.deleteChaptersRecent(data)
        }

    override suspend fun deleteAllChapterRecent() = libraryRepository.deleteAllChapterRecent()

}