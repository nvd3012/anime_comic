package anime.comic.domain.usecases

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.entity.AppConfigEntity
import anime.comic.data.local.entity.BookmarksEntity
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.repositories.ComicRepository
import anime.comic.domain.repositories.HomeRepository
import anime.comic.domain.repositories.UserRepository
import anime.comic.ui.comicdetail.adapter.ChapterItemData
import anime.comic.ui.comicdetail.fragments.ChaptersOrder
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface ComicDetailUseCase {
    fun getPagingChapterComicOrderRev(comicId: String): Flow<PagingData<ChapterItemData>>
    fun getPagingChapterComicOrderPos(comicId: String): Flow<PagingData<ChapterItemData>>
    fun getChapterRecent(idComic: String): Flow<ChapterRecentEntity?>
    suspend fun isUserLogin(): Boolean
    suspend fun getDetailComic(id: String): Resource<ComicDetailData>
    suspend fun saveChapterRecent(data: ChapterRecentEntity)
    fun getSubscribeById(id: String): LiveData<BookmarksEntity?>
    suspend fun deleteSubscribeById(id: String)
    suspend fun addSubscribe(data: ComicDetailData)
    suspend fun postSubscribeToServer(idComic: String): ResourceWithoutData
    suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity?)
}

class ComicDetailUseCaseImpl @Inject constructor(
    private val comicRepository: ComicRepository,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository
) :
    ComicDetailUseCase {
    override suspend fun getDetailComic(id: String) = comicRepository.getDetailComic(id)

    override fun getPagingChapterComicOrderRev(comicId: String) =
        comicRepository.getPagingChapterComic(comicId, ChaptersOrder.REVERSE)

    override fun getPagingChapterComicOrderPos(comicId: String) =
        comicRepository.getPagingChapterComic(comicId, ChaptersOrder.POSITION)

    override suspend fun isUserLogin() = userRepository.isUserLogged()

    override fun getChapterRecent(idComic: String) =
        comicRepository.getChapterRecent(idComic)

    override suspend fun saveChapterRecent(data: ChapterRecentEntity) =
        comicRepository.saveChapterRecent(data)

    override fun getSubscribeById(id: String) = comicRepository.getSubscribeById(id)
    override suspend fun deleteSubscribeById(id: String) = comicRepository.deleteSubscribeById(id)
    override suspend fun addSubscribe(data: ComicDetailData) = comicRepository.addSubscribe(data)
    override suspend fun postSubscribeToServer(idComic: String) =
        comicRepository.postBookmarksComicById(idComic)

    override suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity?) =
        homeRepository.upsertAppConfig(upsert)
}