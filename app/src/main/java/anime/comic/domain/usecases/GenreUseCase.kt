package anime.comic.domain.usecases

import androidx.paging.PagingData
import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.entity.GenreEntity
import anime.comic.data.model.ComicDetailData
import anime.comic.data.model.GenreModel
import anime.comic.domain.repositories.GenreRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface GenreUseCase {
    fun getPagingComicByGenreName(genre: String): Flow<PagingData<ComicDetailData>>
    fun getAllGenres(): Flow<List<GenreEntity>>
    suspend fun getGenresFromServer() : Resource<List<GenreModel>>
}

class GenreUseCaseImpl @Inject constructor(private val genreRepository: GenreRepository) :
    GenreUseCase {
    override fun getPagingComicByGenreName(genre: String) =
        genreRepository.getPagingComicByGenreName(genre)

    override fun getAllGenres() = genreRepository.getAllGenres()

    override suspend fun getGenresFromServer() = genreRepository.getGenresFromServer()
}