package anime.comic.domain.usecases

import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.data.model.ImageModel
import anime.comic.domain.repositories.ComicRepository
import anime.comic.ui.comicreader.adapter.ImageData
import javax.inject.Inject

interface ComicReaderUseCase {
    suspend fun getImagesChapter(idChapter: String): Resource<ImageData?>
    suspend fun getCrawlImgServer1(url: String): Resource<List<String>>
    suspend fun getCrawlImgServer2(url: String): Resource<List<String>>
    suspend fun saveChapterRecent(data: ChapterRecentEntity)
    suspend fun postImgChapterToServer(idChapter: String, data: ImageModel): ResourceWithoutData

}

class ComicReaderUseCaseImpl @Inject constructor(private val comicRepository: ComicRepository) :
    ComicReaderUseCase {
    override suspend fun getImagesChapter(idChapter: String) =
        comicRepository.getImagesChapter(idChapter)

    override suspend fun getCrawlImgServer1(url: String) = comicRepository.crawlChapterServer1(url)
    override suspend fun getCrawlImgServer2(url: String) = comicRepository.crawlChapterServer2(url)

    override suspend fun saveChapterRecent(data: ChapterRecentEntity) =
        comicRepository.saveChapterRecent(data)

    override suspend fun postImgChapterToServer(idChapter: String, data: ImageModel) =
        comicRepository.postImgChapterToServer(idChapter, data)

}