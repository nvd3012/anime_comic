package anime.comic.domain.usecases

import androidx.lifecycle.LiveData
import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.entity.AppConfigEntity
import anime.comic.data.model.AppConfigModel
import anime.comic.domain.repositories.HomeRepository
import anime.comic.domain.repositories.UserRepository
import anime.comic.ui.home.adapter.HomeData

interface HomeUseCase {
    suspend fun getDataHomeScreen(): Resource<HomeData>
    suspend fun sendTokenFcm()
    suspend fun postFID(fid: String): ResourceWithoutData
    suspend fun getAppConfig(): AppConfigEntity?
    suspend fun getAppConfigFromServer(): Resource<AppConfigModel>
    suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity)
    fun getAppConfigLiveData(): LiveData<AppConfigEntity>
}

class HomeUseCaseImpl(
    private val homeRepository: HomeRepository,
    private val userRepository: UserRepository
) : HomeUseCase {
    override suspend fun getDataHomeScreen() = homeRepository.getDataHome()
    override suspend fun sendTokenFcm() = userRepository.sendTokenFcm()
    override suspend fun postFID(fid: String) = homeRepository.postFID(fid)
    override suspend fun getAppConfig() = homeRepository.getAppConfig()
    override suspend fun getAppConfigFromServer() = homeRepository.getAppConfigServer()

    override suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity) =
        homeRepository.upsertAppConfig(upsert)

    override fun getAppConfigLiveData() = homeRepository.getAppConfigLiveData()
}