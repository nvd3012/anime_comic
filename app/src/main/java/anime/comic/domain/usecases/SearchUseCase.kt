package anime.comic.domain.usecases

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.repositories.ComicRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface SearchUseCase {
    fun getPagingComicByKeyword(keyword: String): Flow<PagingData<ComicDetailData>>
    fun getListSearchHistory(): LiveData<List<String>>
    suspend fun deleteSearchHistory()
    suspend fun addSearchHistory(keyword: String)
}

class SearchUseCaseImpl @Inject constructor(private val comicRepository: ComicRepository) :
    SearchUseCase {
    override fun getPagingComicByKeyword(keyword: String) =
        comicRepository.getPagingSearchComicByKeyword(keyword)

    override fun getListSearchHistory() = comicRepository.getListSearchHistory()

    override suspend fun deleteSearchHistory() = withContext(Dispatchers.IO) {
        comicRepository.deleteSearchHistory()
    }

    override suspend fun addSearchHistory(keyword: String) = withContext(Dispatchers.IO) {
        comicRepository.addSearchHistory(keyword)
    }
}