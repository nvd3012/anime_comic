package anime.comic.domain.usecases

import androidx.paging.PagingData
import anime.comic.data.model.ComicDetailData
import anime.comic.domain.repositories.ComicRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface PagingUseCase {
    fun getPagingComicByType(type: String): Flow<PagingData<ComicDetailData>>
}

class PagingUseCaseImpl @Inject constructor(private val comicRepository: ComicRepository) :
    PagingUseCase {
    override fun getPagingComicByType(type: String) = comicRepository.getPagingComicByType(type)

}