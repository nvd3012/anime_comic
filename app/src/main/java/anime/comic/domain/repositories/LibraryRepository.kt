package anime.comic.domain.repositories

import androidx.lifecycle.LiveData
import anime.comic.data.local.dao.BookmarksDAO
import anime.comic.data.local.dao.ChapterRecentDAO
import anime.comic.data.local.entity.BookmarksEntity
import anime.comic.data.local.entity.ChapterRecentEntity
import javax.inject.Inject

interface LibraryRepository {
    fun getListBookmarks(): LiveData<List<BookmarksEntity>>
    suspend fun deleteBookmarks(data: BookmarksEntity)
    suspend fun deleteBookmarks(data: List<BookmarksEntity>)
    fun getListChaptersRecent(): LiveData<List<ChapterRecentEntity>>
    suspend fun deleteChaptersRecent(data: ChapterRecentEntity)
    suspend fun deleteChaptersRecent(data: List<ChapterRecentEntity>)
    suspend fun deleteAllChapterRecent()
}

class LibraryRepositoryImpl @Inject constructor(
    private val chapterRecentDAO: ChapterRecentDAO,
    private val bookmarksDAO: BookmarksDAO
) : LibraryRepository {
    override fun getListBookmarks() = bookmarksDAO.getAll()
    override suspend fun deleteBookmarks(data: BookmarksEntity) = bookmarksDAO.deleteData(data)

    override suspend fun deleteBookmarks(data: List<BookmarksEntity>) =
        bookmarksDAO.deleteData(data)

    override fun getListChaptersRecent() = chapterRecentDAO.getAll()

    override suspend fun deleteChaptersRecent(data: ChapterRecentEntity) =
        chapterRecentDAO.deleteData(
            data
        )

    override suspend fun deleteChaptersRecent(data: List<ChapterRecentEntity>) =
        chapterRecentDAO.deleteData(
            data
        )

    override suspend fun deleteAllChapterRecent() = chapterRecentDAO.deleteAll()

}