package anime.comic.domain.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import anime.comic.data.BasePagingSource
import anime.comic.data.BaseRepository
import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.dao.GenreDAO
import anime.comic.data.local.entity.GenreEntity
import anime.comic.data.model.ComicDetailData
import anime.comic.data.model.GenreModel
import anime.comic.data.remote.api.genre.GenreApiService
import anime.comic.util.DateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

interface GenreRepository {
    fun getPagingComicByGenreName(genre: String): Flow<PagingData<ComicDetailData>>
    fun getAllGenres(): Flow<List<GenreEntity>>
    suspend fun getGenresFromServer() : Resource<List<GenreModel>>
    suspend fun testApi()
}

class GenreRepositoryImpl @Inject constructor(
    private val genreDAO: GenreDAO,
    private val genreApiService: GenreApiService
) : BaseRepository(), GenreRepository {
    init {
        Timber.e("create Genre repository")
    }

    override fun getPagingComicByGenreName(genre: String) = Pager(
        config = PagingConfig(
            pageSize = NETWORK_PAGE_SIZE,
            enablePlaceholders = false
        ), pagingSourceFactory = {
            BasePagingSource(NETWORK_PAGE_SIZE) { page, params ->
                requestComicGenre(page = page, params.loadSize, genre = genre)
            }
        }
    ).flow

    override fun getAllGenres() = genreDAO.getAll()

    override suspend fun getGenresFromServer() = getResult { genreApiService.getListGenres() }

//    override suspend fun getGenresFromServer() : ResourceWithoutData {
//        val res = getResult { genreApiService.getListGenres() }
//        if (res.isSuccess()) {
//            val newData = res.data?.map { GenreEntity(it._id, it.genre) } ?: listOf()
//            genreDAO.addData(newData)
//        }
//        return res
//    }

    override suspend fun testApi() {
        withContext(Dispatchers.IO) {
//            genreApiService.apiTest("https://api.github.com/")
            genreApiService.getListGenres()
            genreApiService.getListGenres()
        }
    }

    private suspend fun requestComicGenre(
        page: Int,
        perPage: Int,
        genre: String
    ): List<ComicDetailData> = if (genre.isEmpty()) listOf() else
        getResult {
            genreApiService.getListComicsByGenre(
                page = page,
                perPage = perPage,
                genre = genre
            )
        }.data?.map {
            ComicDetailData(
                id = it._id,
                authors = it.authors ?: listOf(),
                description = it.description ?: "",
                name = it.name ?: "",
                genres = it.genres ?: listOf(),
                otherName = it.otherName ?: "",
                rating = it.rating ?: 0f,
                status = it.status ?: "",
                thumbnail = it.thumbnail ?: "",
                updated = DateUtils.formatDateSocial(it.updated ?: ""),
                viewCount = it.viewCount ?: 0,
                lastChapter = it.lastChapter ?: 0,
                newChapter = it.newChapter ?: "",
                code = it.code ?: ""
            )
        } ?: listOf()

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }
}