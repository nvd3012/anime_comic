package anime.comic.domain.repositories

import anime.comic.data.BaseRepository
import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.dao.AppConfigDAO
import anime.comic.data.local.dao.UserDAO
import anime.comic.data.local.entity.UserEntity
import anime.comic.data.model.UserModel
import anime.comic.data.remote.api.auth.LoginRequest
import anime.comic.data.remote.api.auth.SignUpRequest
import anime.comic.data.remote.api.auth.UserApiService
import anime.comic.util.ReadWriteFileUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

interface UserRepository {
    suspend fun login(body: LoginRequest): Resource<UserModel>
    suspend fun signUp(body: SignUpRequest): ResourceWithoutData
    suspend fun resetPassword(email: String): ResourceWithoutData
    suspend fun sendVerified(email: String): ResourceWithoutData
    suspend fun sendTokenFcm()
    suspend fun isUserLogged(): Boolean
    suspend fun addData(userEntity: UserEntity)
    suspend fun deleteById(id: String)
    suspend fun deleteData(userEntity: UserEntity)
}

class UserRepositoryImpl @Inject constructor(
    private val userApiService: UserApiService,
    private val userDAO: UserDAO,
    private val readWriteFileUtil: ReadWriteFileUtil,
    private val appConfigDAO: AppConfigDAO
) : UserRepository, BaseRepository() {

    override suspend fun login(body: LoginRequest): Resource<UserModel> {
        val resource = getResult { userApiService.login(body) }
        if (resource.isSuccess()) {
            resource.data?.apply {
                readWriteFileUtil.saveToken(accessToken)
                addData(
                    UserEntity(
                        _id,
                        coin,
                        referralCode,
                        shareCode,
                        isNotify,
                        isBlocked,
                        isAcceptPolicy,
                        isDailySpin
                    )
                )
            }
        }
        return resource
    }

    override suspend fun signUp(body: SignUpRequest) = withContext(Dispatchers.IO) {
        getResult { userApiService.signUp(body) }
    }

    override suspend fun resetPassword(email: String) = withContext(Dispatchers.IO) {
        getResult { userApiService.resetPassword(mapOf(KEY_EMAIL to email)) }
    }

    override suspend fun sendVerified(email: String) = withContext(Dispatchers.IO) {
        getResult { userApiService.sendVerify(mapOf(KEY_EMAIL to email)) }
    }

    override suspend fun sendTokenFcm() {
        withContext(Dispatchers.IO) {
            val appConfig = appConfigDAO.getAppConfig()
            appConfig?.let {
                if (appConfig.token.isNullOrEmpty() || appConfig.tokenFcm.isNullOrEmpty()) return@withContext
                Timber.e("send token ${appConfig.tokenFcm}")
                getResult { userApiService.sendTokenFcm(mapOf(KEY_TOKEN to appConfig.tokenFcm)) }.onSuccessSuspend {
                    val newAppConfig = appConfig.copy(tokenFcm = null)
                    appConfigDAO.addData(newAppConfig)
                }
            }?:return@withContext
        }
    }

    override suspend fun isUserLogged() = withContext(Dispatchers.IO) {
        !appConfigDAO.getAppConfig()?.token.isNullOrEmpty()
    }

    override suspend fun addData(userEntity: UserEntity) = userDAO.addData(userEntity)
    override suspend fun deleteById(id: String) = userDAO.deleteById(id)
    override suspend fun deleteData(userEntity: UserEntity) = userDAO.deleteData(userEntity)

    companion object {
        private const val KEY_EMAIL = "email"
        private const val KEY_TOKEN = "token"
    }
}