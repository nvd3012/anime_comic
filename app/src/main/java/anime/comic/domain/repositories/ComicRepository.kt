package anime.comic.domain.repositories

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import anime.comic.data.*
import anime.comic.data.local.dao.BookmarksDAO
import anime.comic.data.local.dao.ChapterRecentDAO
import anime.comic.data.local.dao.SearchHistoryDAO
import anime.comic.data.local.entity.BookmarksEntity
import anime.comic.data.local.entity.ChapterRecentEntity
import anime.comic.data.local.entity.SearchHistoryEntity
import anime.comic.data.model.ComicDetailData
import anime.comic.data.model.ImageModel
import anime.comic.data.remote.api.chapter.ChapterApiService
import anime.comic.data.remote.api.comics.ComicsApiService
import anime.comic.ui.comicdetail.adapter.ChapterItemData
import anime.comic.ui.comicdetail.fragments.ChaptersOrder
import anime.comic.ui.comicreader.adapter.ImageData
import anime.comic.util.DateUtils
import kotlinx.coroutines.flow.Flow
import org.jsoup.Jsoup
import timber.log.Timber
import javax.inject.Inject

interface ComicRepository {
    suspend fun saveChapterRecent(data: ChapterRecentEntity)
    suspend fun getDetailComic(id: String): Resource<ComicDetailData>
    suspend fun getImagesChapter(idChapter: String): Resource<ImageData?>
    suspend fun getBookmarksStateByComicId(idComic: String): ResourceWithoutData
    suspend fun postBookmarksComicById(idComic: String): ResourceWithoutData
    fun getChapterRecent(idComic: String): Flow<ChapterRecentEntity?>
    fun getPagingChapterComic(
        comicId: String,
        orderType: ChaptersOrder
    ): Flow<PagingData<ChapterItemData>>

    fun getPagingSearchComicByKeyword(keyword: String): Flow<PagingData<ComicDetailData>>
    fun getPagingComicByType(type: String): Flow<PagingData<ComicDetailData>>
    fun getSubscribeById(id: String): LiveData<BookmarksEntity?>
    suspend fun deleteSubscribeById(id: String)
    suspend fun addSubscribe(data: ComicDetailData)
    fun getListSearchHistory(): LiveData<List<String>>
    suspend fun deleteSearchHistory()
    suspend fun addSearchHistory(keyword: String)
    suspend fun crawlChapterServer1(href: String): Resource<List<String>>
    suspend fun crawlChapterServer2(href: String): Resource<List<String>>
    suspend fun postImgChapterToServer(idChapter: String, data: ImageModel): ResourceWithoutData
}

class ComicRepositoryImpl @Inject constructor(
    private val comicsApiService: ComicsApiService,
    private val chapterApiService: ChapterApiService,
    private val chapterRecentDao: ChapterRecentDAO,
    private val bookmarksDAO: BookmarksDAO,
    private val searchHistoryDAO: SearchHistoryDAO
) :
    ComicRepository, BaseRepository() {
    override fun getChapterRecent(idComic: String) =
        chapterRecentDao.getByIdComic(idComic)

    override suspend fun saveChapterRecent(data: ChapterRecentEntity) =
        chapterRecentDao.addData(data)

    override suspend fun getDetailComic(id: String): Resource<ComicDetailData> =
        getResult { comicsApiService.getComicById(id) }.map {
            ComicDetailData(
                id = it?._id ?: "",
                authors = it?.authors ?: listOf(),
                description = it?.description ?: "",
                genres = it?.genres ?: listOf(),
                name = it?.name ?: "",
                otherName = it?.otherName ?: "",
                rating = it?.rating ?: 0f,
                status = it?.status ?: "",
                thumbnail = it?.thumbnail ?: "",
                updated = DateUtils.formatDateSocial(it?.updated ?: ""),
                viewCount = it?.viewCount ?: 0,
                lastChapter = it?.lastChapter ?: 0,
                newChapter = it?.newChapter ?: "",
                code = it?.code ?: ""
            )
        }

    override suspend fun getImagesChapter(idChapter: String): Resource<ImageData?> =
        getResult { chapterApiService.getListImageChapter(idChapter) }.map {
            it?.let {
                return@let ImageData(
                    serverImage1 = it.server1,
                    serverImage2 = it.server2
                )
            }
        }

    override suspend fun getBookmarksStateByComicId(idComic: String) = getResult {
        comicsApiService.getBookmarksStateByComicId(idComic)
    }

    override suspend fun postBookmarksComicById(idComic: String) = getResult {
        comicsApiService.postBookmarksComicById(mapOf(Pair(COMIC_ID, idComic), Pair(MARKED, true)))
    }

    override fun getPagingChapterComic(comicId: String, orderType: ChaptersOrder) = Pager(
        config = PagingConfig(
            pageSize = NETWORK_PAGE_SIZE,
            enablePlaceholders = false
        ), pagingSourceFactory = {
            BasePagingSource(NETWORK_PAGE_SIZE) { page, params ->
                requestComicChapter(
                    page = page,
                    perPage = params.loadSize,
                    idComic = comicId,
                    orderType = orderType
                )
            }
        }
    ).flow

    override fun getPagingSearchComicByKeyword(keyword: String) =
        Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = false),
            pagingSourceFactory = {
                BasePagingSource(NETWORK_PAGE_SIZE) { page, params ->
                    requestComicByKeyword(page, params.loadSize, keyword)
                }
            }
        ).flow

    override fun getPagingComicByType(type: String) = Pager(
        config = PagingConfig(pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = false),
        pagingSourceFactory = {
            BasePagingSource(NETWORK_PAGE_SIZE) { page, params ->
                requestComicByType(page, params.loadSize, type)
            }
        }
    ).flow

    override fun getSubscribeById(id: String) = bookmarksDAO.getByIdComic(id)

    override suspend fun deleteSubscribeById(id: String) = bookmarksDAO.deleteById(id)
    override suspend fun addSubscribe(data: ComicDetailData) {
        bookmarksDAO.addData(
            BookmarksEntity(
                idComic = data.id,
                comicDetailData = data
            )
        )
    }

    override fun getListSearchHistory() = searchHistoryDAO.getAllSearchHistory()
    override suspend fun deleteSearchHistory() = searchHistoryDAO.deleteSearchHistory()
    override suspend fun addSearchHistory(keyword: String) = searchHistoryDAO.addData(
        SearchHistoryEntity(keyword)
    )

    override suspend fun crawlChapterServer1(href: String) = getResultPublic {
        chapterApiService.crawlChapterServer1(href)
    }.map {
        val document = Jsoup.parse(it?.string())
        val images = document.select("div.container-chapter-reader > img")
        images.map { data ->
            Timber.e(data.attr("src"))
            data.attr("src")
        }
    }

    override suspend fun crawlChapterServer2(href: String) = getResultPublic {
        chapterApiService.crawlChapterServer2(href)
    }.map {
        val document = Jsoup.parse(it?.string())
        val images = document.select("div.container-chapter-reader > img")
        images.map { data ->
            Timber.e(data.attr("src"))
            data.attr("src")
        }
    }

    override suspend fun postImgChapterToServer(
        idChapter: String,
        data: ImageModel
    ) = getResult { chapterApiService.postImgChapterToServer(idChapter, data) }

    private suspend fun requestComicByType(
        page: Int,
        perPage: Int,
        type: String
    ): List<ComicDetailData> =

        getResult { comicsApiService.getListComicsByType(page, perPage, type) }.data?.map {
            ComicDetailData(
                id = it._id,
                authors = it.authors ?: listOf(),
                description = it.description ?: "",
                genres = it.genres ?: listOf(),
                name = it.name ?: "",
                otherName = it.otherName ?: "",
                rating = it.rating ?: 0f,
                status = it.status ?: "",
                thumbnail = it.thumbnail ?: "",
                updated = DateUtils.formatDateSocial(it.updated ?: ""),
                viewCount = it.viewCount ?: 0,
                lastChapter = it.lastChapter ?: 0,
                newChapter = it.newChapter ?: "",
                code = it.code ?: ""
            )
        } ?: listOf()

    private suspend fun requestComicByKeyword(
        page: Int,
        perPage: Int,
        keyword: String
    ): List<ComicDetailData> =

        getResult { comicsApiService.getListComicsByKeyWord(page, perPage, keyword) }.data?.map {
            ComicDetailData(
                id = it._id,
                authors = it.authors ?: listOf(),
                description = it.description ?: "",
                genres = it.genres ?: listOf(),
                name = it.name ?: "",
                otherName = it.otherName ?: "",
                rating = it.rating ?: 0f,
                status = it.status ?: "",
                thumbnail = it.thumbnail ?: "",
                updated = DateUtils.formatDateSocial(it.updated ?: ""),
                viewCount = it.viewCount ?: 0,
                lastChapter = it.lastChapter ?: 0,
                newChapter = it.newChapter ?: "",
                code = it.code ?: ""
            )
        } ?: listOf()

    private suspend fun requestComicChapter(
        page: Int,
        perPage: Int,
        idComic: String,
        orderType: ChaptersOrder
    ): List<ChapterItemData> {
        val serviceCall = if (orderType == ChaptersOrder.POSITION) {
            chapterApiService.getListChapter(
                page = page,
                perPage = perPage,
                idComic = idComic
            )
        } else {
            chapterApiService.getListChapter(
                page = page,
                perPage = perPage,
                idComic = idComic,
                sort = -1
            )
        }
        return getResult {
            serviceCall
        }.data?.map {
            ChapterItemData(
                idChapter = it._id,
                chapterName = it.name,
                chapterUploaded = it.uploaded,
                chapterView = it.view,
                href = it.href ?: ""
            )
        } ?: listOf()
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
        private const val COMIC_ID = "comicId"
        private const val MARKED = "isMarked"
    }
}