package anime.comic.domain.repositories

import androidx.lifecycle.LiveData
import anime.comic.data.BaseRepository
import anime.comic.data.Resource
import anime.comic.data.ResourceWithoutData
import anime.comic.data.local.dao.AppConfigDAO
import anime.comic.data.local.entity.AppConfigEntity
import anime.comic.data.model.AppConfigModel
import anime.comic.data.model.ComicDetailData
import anime.comic.data.remote.api.home.HomeApiService
import anime.comic.ui.home.adapter.HomeData
import anime.comic.util.Const
import anime.comic.util.DateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface HomeRepository {
    suspend fun getDataHome(): Resource<HomeData>
    suspend fun postFID(fid: String): ResourceWithoutData
    suspend fun getAppConfig(): AppConfigEntity?
    suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity?)
    fun getAppConfigLiveData(): LiveData<AppConfigEntity>
    suspend fun getAppConfigServer(): Resource<AppConfigModel>
}

class HomeRepositoryImpl @Inject constructor(
    private val homeApiService: HomeApiService,
    private val appConfigDAO: AppConfigDAO
) : BaseRepository(), HomeRepository {

    override suspend fun getDataHome() =
        getResult { homeApiService.getDataHomeScreen() }.map { oldData ->
            HomeData(
                latest = oldData?.latest?.map {
                    ComicDetailData(
                        id = it._id,
                        authors = it.authors ?: listOf(),
                        description = it.description ?: "",
                        genres = it.genres ?: listOf(),
                        name = it.name ?: "",
                        otherName = it.otherName ?: "",
                        rating = it.rating ?: 0f,
                        status = it.status ?: "",
                        thumbnail = it.thumbnail ?: "",
                        updated = DateUtils.formatDateSocial(it.updated ?: ""),
                        viewCount = it.viewCount ?: 0,
                        lastChapter = it.lastChapter ?: 0,
                        newChapter = it.newChapter ?: "",
                        code = it.code ?: ""
                    )
                } ?: listOf(),
                popular = oldData?.popular?.map {
                    ComicDetailData(
                        id = it._id,
                        authors = it.authors ?: listOf(),
                        description = it.description ?: "",
                        genres = it.genres ?: listOf(),
                        name = it.name ?: "",
                        otherName = it.otherName ?: "",
                        rating = it.rating ?: 0f,
                        status = it.status ?: "",
                        thumbnail = it.thumbnail ?: "",
                        updated = DateUtils.formatDateSocial(it.updated ?: ""),
                        viewCount = it.viewCount ?: 0,
                        lastChapter = it.lastChapter ?: 0,
                        newChapter = it.newChapter ?: "",
                        code = it.code ?: ""
                    )
                } ?: listOf(),
                topWeek = oldData?.topWeek?.map {
                    ComicDetailData(
                        id = it._id,
                        authors = it.authors ?: listOf(),
                        description = it.description ?: "",
                        genres = it.genres ?: listOf(),
                        name = it.name ?: "",
                        otherName = it.otherName ?: "",
                        rating = it.rating ?: 0f,
                        status = it.status ?: "",
                        thumbnail = it.thumbnail ?: "",
                        updated = DateUtils.formatDateSocial(it.updated ?: ""),
                        viewCount = it.viewCount ?: 0,
                        lastChapter = it.lastChapter ?: 0,
                        newChapter = it.newChapter ?: "",
                        code = it.code ?: ""
                    )
                } ?: listOf(),
                newest = oldData?.newest?.map {
                    ComicDetailData(
                        id = it._id,
                        authors = it.authors ?: listOf(),
                        description = it.description ?: "",
                        genres = it.genres ?: listOf(),
                        name = it.name ?: "",
                        otherName = it.otherName ?: "",
                        rating = it.rating ?: 0f,
                        status = it.status ?: "",
                        thumbnail = it.thumbnail ?: "",
                        updated = DateUtils.formatDateSocial(it.updated ?: ""),
                        viewCount = it.viewCount ?: 0,
                        lastChapter = it.lastChapter ?: 0,
                        newChapter = it.newChapter ?: "",
                        code = it.code ?: ""
                    )
                } ?: listOf(),
                banners = oldData?.banners ?: listOf()
            )
        }

    override suspend fun postFID(fid: String) = getResult {
        val body = mapOf(Const.FID to fid)
        homeApiService.postFIDToServer(body)
    }

    override suspend fun getAppConfig() = appConfigDAO.getAppConfig()

    override suspend fun upsertAppConfig(upsert: (AppConfigEntity) -> AppConfigEntity?) {
        withContext(Dispatchers.IO) {
            val data = appConfigDAO.getAppConfig()
            upsert(data ?: AppConfigEntity())?.let {
                appConfigDAO.addData(it)
            }
        }
    }

    override fun getAppConfigLiveData() = appConfigDAO.getAppConfigLiveData()
    override suspend fun getAppConfigServer() =
        getResult {
            homeApiService.getAppConfigFromServer()
        }.apply {
            onSuccessSuspend { response ->
                upsertAppConfig {
                    response?.run {
                        it.copy(
                            function = function,
                            isActived = isActived ?: 0,
                            name = name
                        )
                    }
                }
            }
        }
}