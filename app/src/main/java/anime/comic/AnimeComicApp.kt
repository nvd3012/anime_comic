package anime.comic

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class AnimeComicApp : MultiDexApplication() {
    companion object {
        lateinit var instance: AnimeComicApp private set
        val context get() = instance.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}