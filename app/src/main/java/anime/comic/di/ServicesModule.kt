package anime.comic.di

import anime.comic.data.ApiManager
import anime.comic.data.remote.api.auth.UserApiService
import anime.comic.data.remote.api.chapter.ChapterApiService
import anime.comic.data.remote.api.comics.ComicsApiService
import anime.comic.data.remote.api.genre.GenreApiService
import anime.comic.data.remote.api.home.HomeApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ServicesModule {

    @Singleton
    @Provides
    fun provideUserApiService(apiManager: ApiManager): UserApiService =
        apiManager.createApiService()

    @Singleton
    @Provides
    fun provideComicsApiService(apiManager: ApiManager): ComicsApiService =
        apiManager.createApiService()

    @Singleton
    @Provides
    fun provideHomeApiService(apiManager: ApiManager): HomeApiService =
        apiManager.createApiService()

    @Singleton
    @Provides
    fun provideGenreApiService(apiManager: ApiManager): GenreApiService =
        apiManager.createApiService()

    @Singleton
    @Provides
    fun provideChapterApiService(apiManager: ApiManager): ChapterApiService =
        apiManager.createApiService()
}