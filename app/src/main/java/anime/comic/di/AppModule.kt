package anime.comic.di

import android.content.Context
import androidx.room.Room
import anime.comic.data.local.db.AppDatabase
import anime.comic.data.local.pref.AppPref
import anime.comic.data.local.pref.AppPrefImpl
import anime.comic.util.Const
import anime.comic.util.ReadWriteFileUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class AppModule {

    @Singleton
    @Provides
    fun provideAppContext(@ApplicationContext context: Context): Context = context

    @Singleton
    @Provides
    fun providePrefHelper(
        context: Context
    ): AppPref = AppPrefImpl(context)

    @Singleton
    @Provides
    fun provideAppDatabase(context: Context) = AppDatabase.getInstance(context)

    @Provides
    fun provideReadWriteFileUtil(context: Context) =
        ReadWriteFileUtil(context)
}