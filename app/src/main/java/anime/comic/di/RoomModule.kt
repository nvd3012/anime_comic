package anime.comic.di

import anime.comic.data.local.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {
    @Provides
    fun provideUserDao(database: AppDatabase) = database.userDao()

    @Provides
    fun provideGenreDao(database: AppDatabase) = database.genreDao()

    @Provides
    fun provideChapterRecentDao(database: AppDatabase) = database.chapterRecentDao()

    @Provides
    fun provideBookmarksDao(database: AppDatabase) = database.bookmarksDao()

    @Provides
    fun provideAppConfigDao(database: AppDatabase) = database.appConfigDao()

    @Provides
    fun provideSearchHistory(database: AppDatabase) = database.searchHistoryDao()
}