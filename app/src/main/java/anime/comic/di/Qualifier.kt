package anime.comic.di

import javax.inject.Qualifier

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class HttpLogging

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class HttpHeader