package anime.comic.di

import anime.comic.domain.repositories.*
import anime.comic.domain.usecases.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class UseCaseModule {
    @Provides
    fun provideGenreUseCase(genreRepository: GenreRepository): GenreUseCase =
        GenreUseCaseImpl(genreRepository)

    @Singleton
    @Provides
    fun provideHomeUseCase(
        homeRepository: HomeRepository,
        userRepository: UserRepository
    ): HomeUseCase =
        HomeUseCaseImpl(homeRepository, userRepository)

    @Singleton
    @Provides
    fun provideComicReaderUseCase(comicRepository: ComicRepository): ComicReaderUseCase =
        ComicReaderUseCaseImpl(comicRepository)

    @Singleton
    @Provides
    fun provideComicDetailUseCase(
        comicRepository: ComicRepository,
        userRepository: UserRepository,
        homeRepository: HomeRepository
    ): ComicDetailUseCase =
        ComicDetailUseCaseImpl(comicRepository, userRepository, homeRepository)

    @Singleton
    @Provides
    fun provideSearchComicUseCase(comicRepository: ComicRepository): SearchUseCase =
        SearchUseCaseImpl(comicRepository)

    @Singleton
    @Provides
    fun providePagingUseCase(comicRepository: ComicRepository): PagingUseCase =
        PagingUseCaseImpl(comicRepository)

    @Singleton
    @Provides
    fun provideLibraryUseCase(libraryRepository: LibraryRepository): LibraryUseCase =
        LibraryUseCaseImpl(libraryRepository)

    @Singleton
    @Provides
    fun provideFcmUseCase(
        comicRepository: ComicRepository,
        userRepository: UserRepository,
        homeRepository: HomeRepository
    ): FcmUseCase = FcmUseCaseImpl(comicRepository, userRepository, homeRepository)
}