package anime.comic.di

import anime.comic.data.local.dao.*
import anime.comic.data.local.pref.AppPref
import anime.comic.data.remote.api.auth.UserApiService
import anime.comic.data.remote.api.chapter.ChapterApiService
import anime.comic.data.remote.api.comics.ComicsApiService
import anime.comic.data.remote.api.genre.GenreApiService
import anime.comic.data.remote.api.home.HomeApiService
import anime.comic.domain.repositories.*
import anime.comic.util.ReadWriteFileUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideUserRepository(
        userApiService: UserApiService,
        userDAO: UserDAO,
        readWriteFileUtil: ReadWriteFileUtil,
        appConfigDAO: AppConfigDAO
    ): UserRepository =
        UserRepositoryImpl(userApiService, userDAO, readWriteFileUtil, appConfigDAO)

    @Singleton
    @Provides
    fun provideGenreRepository(
        genreDAO: GenreDAO,
        genreApiService: GenreApiService
    ): GenreRepository =
        GenreRepositoryImpl(genreDAO, genreApiService)

    @Singleton
    @Provides
    fun provideHomeRepository(
        homeApiService: HomeApiService,
        appConfigDAO: AppConfigDAO
    ): HomeRepository =
        HomeRepositoryImpl(homeApiService, appConfigDAO)

    @Singleton
    @Provides
    fun provideComicReaderRepository(
        comicsApiService: ComicsApiService,
        chapterApiService: ChapterApiService,
        chapterRecentDAO: ChapterRecentDAO,
        bookmarksDAO: BookmarksDAO,
        searchHistoryDAO: SearchHistoryDAO
    ): ComicRepository =
        ComicRepositoryImpl(
            comicsApiService,
            chapterApiService,
            chapterRecentDAO,
            bookmarksDAO,
            searchHistoryDAO
        )

    @Singleton
    @Provides
    fun provideLibraryRepository(
        chapterRecentDAO: ChapterRecentDAO,
        bookmarksDAO: BookmarksDAO
    ): LibraryRepository = LibraryRepositoryImpl(chapterRecentDAO, bookmarksDAO)
}