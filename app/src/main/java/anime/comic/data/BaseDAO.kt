package anime.comic.data

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDAO<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addData(data: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addData(data: List<T>)

    @Delete
    suspend fun deleteData(data: T)

    @Delete
    suspend fun deleteData(data: List<T>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateData(data: T)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateData(data: List<T>)
}