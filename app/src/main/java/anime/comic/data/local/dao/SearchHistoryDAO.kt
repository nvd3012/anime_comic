package anime.comic.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.SearchHistoryEntity

@Dao
interface SearchHistoryDAO: BaseDAO<SearchHistoryEntity> {
    @Query("SELECT keyWord FROM ${SearchHistoryEntity.TABLE_NAME}")
    fun getAllSearchHistory() : LiveData<List<String>>

    @Query("DELETE FROM ${SearchHistoryEntity.TABLE_NAME}")
    suspend fun deleteSearchHistory()
}