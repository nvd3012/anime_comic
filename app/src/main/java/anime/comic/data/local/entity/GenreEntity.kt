package anime.comic.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = GenreEntity.TABLE_NAME)
data class GenreEntity(
    @PrimaryKey val _id: String,
    @ColumnInfo val genre: String
) {

    companion object {
        const val TABLE_NAME = "tb_genre"
    }
}