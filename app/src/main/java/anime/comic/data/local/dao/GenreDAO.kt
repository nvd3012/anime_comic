package anime.comic.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.GenreEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GenreDAO : BaseDAO<GenreEntity> {
    @Query("SELECT * FROM ${GenreEntity.TABLE_NAME}")
    fun getAll(): Flow<List<GenreEntity>>
}