package anime.comic.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import anime.comic.data.model.FunctionModel

@Entity(tableName = AppConfigEntity.TABLE_NAME)
data class AppConfigEntity(
    @PrimaryKey val id : Int = 1,
    @ColumnInfo val token: String? = null,
    @ColumnInfo val tokenFcm: String? = null,
    @ColumnInfo val fid: String? = null,
    @ColumnInfo val isUser: Boolean = false,
    @Embedded val function: FunctionModel? = null,
    @ColumnInfo val isActived: Int = 0,
    @ColumnInfo val name: String? = null
) {

    companion object {
        const val TABLE_NAME = "tbl_app_config"
    }
}