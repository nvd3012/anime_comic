package anime.comic.data.local.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import anime.comic.data.model.ComicDetailData
import anime.comic.ui.comicdetail.adapter.ChapterItemData

@Entity(tableName = ChapterRecentEntity.TABLE_NAME)
data class ChapterRecentEntity(
    @PrimaryKey val idComic: String,
    @Embedded val comicDetailData: ComicDetailData,
    @Embedded val chapterItemData: ChapterItemData
): LibraryItem {
    companion object {
        const val TABLE_NAME = "tb_chapter_recent"
    }
}