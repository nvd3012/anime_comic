package anime.comic.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.UserEntity

@Dao
interface UserDAO : BaseDAO<UserEntity> {
    @Query("DELETE FROM ${UserEntity.TABLE_NAME} WHERE _id LIKE :id")
    suspend fun deleteById(id: String)

    @Query("DELETE FROM ${UserEntity.TABLE_NAME}")
    suspend fun deleteAll()

    @Query("SELECT * FROM ${UserEntity.TABLE_NAME}")
    suspend fun getAll(): List<UserEntity>

    @Query("SELECT * FROM ${UserEntity.TABLE_NAME} WHERE _id LIKE :id")
    suspend fun getById(id: String): UserEntity
}