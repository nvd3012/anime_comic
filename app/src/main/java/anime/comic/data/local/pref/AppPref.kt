package anime.comic.data.local.pref

interface AppPref {

    fun isFirstRun(): Boolean

    fun remove(key: String)

    fun clear()

    fun saveString(key: String, value: String)

    fun saveInt(key: String, value: Int)

    fun saveLong(key: String, value: Long)

    fun saveBoolean(key: String, value: Boolean)

    fun getString(key: String): String

    fun getInt(key: String): Int

    fun getLong(key: String): Long

    fun getBoolean(key: String): Boolean
}