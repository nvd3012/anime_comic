package anime.comic.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.HomeEntity

@Dao
interface HomeDAO : BaseDAO<HomeEntity> {
    @Query("SELECT * FROM ${HomeEntity.TABLE_NAME} WHERE id = 1")
    suspend fun getHomeData(): LiveData<HomeEntity>
}