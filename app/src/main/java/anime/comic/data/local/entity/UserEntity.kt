package anime.comic.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = UserEntity.TABLE_NAME)
data class UserEntity(
    @PrimaryKey val _id: String,
    @ColumnInfo val coin: Long = 0,
    @ColumnInfo val referralCode: String = "",
    @ColumnInfo val shareCode: String = "",
    @ColumnInfo val isNotify: Boolean = false,
    @ColumnInfo val isBlocked: Boolean = false,
    @ColumnInfo val isAcceptPolicy: Boolean = false,
    @ColumnInfo val isDailySpin: Boolean = false
) {
    companion object {
        const val TABLE_NAME = "user_table"
    }
}