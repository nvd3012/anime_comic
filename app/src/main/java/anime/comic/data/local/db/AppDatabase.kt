package anime.comic.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import anime.comic.data.local.dao.*
import anime.comic.data.local.entity.*
import anime.comic.util.Const
import anime.comic.util.helper.RoomConverter

@Database(
    entities = [
        UserEntity::class, GenreEntity::class,
        ChapterRecentEntity::class, BookmarksEntity::class,
        AppConfigEntity::class,SearchHistoryEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(RoomConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDAO
    abstract fun genreDao(): GenreDAO
    abstract fun chapterRecentDao(): ChapterRecentDAO
    abstract fun bookmarksDao(): BookmarksDAO
    abstract fun appConfigDao(): AppConfigDAO
    abstract fun searchHistoryDao(): SearchHistoryDAO

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildData(
                        context
                    )
                        .also { INSTANCE = it }
            }

        private fun buildData(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            Const.DATABASE_NAME
        ).build()
    }
}