package anime.comic.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = SearchHistoryEntity.TABLE_NAME)
data class SearchHistoryEntity(
    @PrimaryKey val keyWord: String
) {

    companion object {
        const val TABLE_NAME = "tbl_search_history"
    }
}