package anime.comic.data.local.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import anime.comic.data.model.ComicDetailData

@Entity(tableName = BookmarksEntity.TABLE_NAME)
data class BookmarksEntity(
    @PrimaryKey val idComic: String,
    @Embedded val comicDetailData: ComicDetailData
): LibraryItem {
    companion object{
        const val TABLE_NAME = "tb_bookmarks"
    }
}