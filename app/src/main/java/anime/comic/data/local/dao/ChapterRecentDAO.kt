package anime.comic.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.ChapterRecentEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ChapterRecentDAO : BaseDAO<ChapterRecentEntity> {

    @Query("SELECT * FROM ${ChapterRecentEntity.TABLE_NAME} WHERE idComic = :idComic")
    fun getByIdComic(idComic: String): Flow<ChapterRecentEntity?>

    @Query("SELECT * FROM ${ChapterRecentEntity.TABLE_NAME}")
    fun getAll(): LiveData<List<ChapterRecentEntity>>

    @Query("DELETE FROM ${ChapterRecentEntity.TABLE_NAME}")
    suspend fun deleteAll()

    @Query("DELETE FROM ${ChapterRecentEntity.TABLE_NAME} WHERE idComic =:id")
    suspend fun deleteById(id: String)
}