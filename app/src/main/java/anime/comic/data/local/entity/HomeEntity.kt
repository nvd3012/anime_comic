package anime.comic.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import anime.comic.data.model.ComicDetailData
import anime.comic.data.model.ComicModel

@Entity(tableName = HomeEntity.TABLE_NAME)
data class HomeEntity(
    @PrimaryKey val id: Int = 1,
    @ColumnInfo val latest: List<ComicDetailData>,
    @ColumnInfo val newest: List<ComicDetailData>,
    @ColumnInfo val popular: List<ComicDetailData>,
    @ColumnInfo val topWeek: List<ComicDetailData>,
    @ColumnInfo val banners: List<String>
) {

    companion object {
        const val TABLE_NAME = "tbl_home"
    }
}