package anime.comic.data.local.pref

import android.content.Context
import androidx.core.content.edit

class AppPrefImpl(context: Context) : AppPref {
    companion object {
        private const val FIRST_RUN = "FIRST_RUN"
    }

    private val sharedPreferences = context.getSharedPreferences(
        context.packageName,
        Context.MODE_PRIVATE
    )

    override fun isFirstRun(): Boolean {
        val isFirstRun = sharedPreferences.getBoolean(FIRST_RUN, true)
        if (isFirstRun) {
            sharedPreferences.edit { putBoolean(FIRST_RUN, false) }
        }
        return isFirstRun
    }

    override fun remove(key: String) {
        sharedPreferences.edit {
            remove(key)
        }
    }

    override fun clear() {
        sharedPreferences.edit { clear() }
    }

    override fun saveString(key: String, value: String) =
        sharedPreferences.edit { putString(key, value) }

    override fun saveInt(key: String, value: Int) =
        sharedPreferences.edit { putInt(key, value) }

    override fun saveLong(key: String, value: Long) =
        sharedPreferences.edit { putLong(key, value) }

    override fun saveBoolean(key: String, value: Boolean) =
        sharedPreferences.edit { putBoolean(key, value) }

    override fun getString(key: String) = sharedPreferences.getString(key, "") ?: ""

    override fun getInt(key: String) = sharedPreferences.getInt(key, 0)

    override fun getLong(key: String) = sharedPreferences.getLong(key, 0)

    override fun getBoolean(key: String) = sharedPreferences.getBoolean(key, false)
}