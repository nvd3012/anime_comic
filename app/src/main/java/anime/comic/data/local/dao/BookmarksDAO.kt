package anime.comic.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.BookmarksEntity

@Dao
interface BookmarksDAO : BaseDAO<BookmarksEntity> {

    @Query("SELECT * FROM ${BookmarksEntity.TABLE_NAME} WHERE id =:idComic")
    fun getByIdComic(idComic: String): LiveData<BookmarksEntity?>

    @Query("DELETE FROM ${BookmarksEntity.TABLE_NAME} WHERE id =:idComic")
    suspend fun deleteById(idComic: String)

    @Query("SELECT * FROM ${BookmarksEntity.TABLE_NAME}")
    fun getAll(): LiveData<List<BookmarksEntity>>

    @Query("DELETE FROM ${BookmarksEntity.TABLE_NAME}")
    suspend fun deleteAll()
}