package anime.comic.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import anime.comic.data.BaseDAO
import anime.comic.data.local.entity.AppConfigEntity

@Dao
interface AppConfigDAO : BaseDAO<AppConfigEntity> {
    @Query("SELECT * FROM ${AppConfigEntity.TABLE_NAME} WHERE id = 1")
    suspend fun getAppConfig(): AppConfigEntity?

    @Query("SELECT * FROM ${AppConfigEntity.TABLE_NAME} WHERE id = 1")
    fun getAppConfigLiveData(): LiveData<AppConfigEntity>
}