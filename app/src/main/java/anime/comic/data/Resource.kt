package anime.comic.data

typealias ResourceWithoutData = Resource<Any?>

data class Resource<out T>(
    val status: Status,
    val data: T? = null,
    val error: ErrorResponse = ErrorResponse()
) {
    enum class Status {
        SUCCESS,
        ERROR
    }

    fun isError(): Boolean = status == Status.ERROR
    fun isSuccess(): Boolean = status == Status.SUCCESS

    fun onFailure(block: (ErrorResponse) -> Unit): Resource<T> =
        when (this.status) {
            Status.SUCCESS -> this
            Status.ERROR -> {
                block(error)
                this
            }
        }

    fun onSuccess(block: (T?) -> Unit) = when (this.status) {
        Status.SUCCESS -> {
            block(data)
            this
        }
        Status.ERROR -> this
    }

    suspend fun onSuccessSuspend(block: suspend (T?) -> Unit) = when (this.status) {
        Status.SUCCESS -> {
            block(data)
            this
        }
        Status.ERROR -> this
    }

    fun <R> map(map: (T?) -> R): Resource<R> {
        val data = map(data)
        return if (isSuccess()) successOrNull(data) else error(error)
    }

    companion object {
        fun <T> successOrNull(data: T? = null): Resource<T> {
            return Resource(Status.SUCCESS, data)
        }

        fun <T> success(data: T): Resource<T> {
            return Resource(Status.SUCCESS, data)
        }

        fun <T> error(error: ErrorResponse): Resource<T> {
            return Resource(Status.ERROR, error = error)
        }
    }
}