package anime.comic.data.remote.api.chapter

import anime.comic.data.ResponseData
import anime.comic.data.ResponseWithoutData
import anime.comic.data.model.ChapterModel
import anime.comic.data.model.ImageModel
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ChapterApiService {
    @GET("/chapters/images/{chapterId}")
    suspend fun getListImageChapter(@Path("chapterId") idChapter: String): ResponseData<ImageModel>

    @GET("/chapters")
    suspend fun getListChapter(
        @Query("comicId") idComic: String,
        @Query("page") page: Int,
        @Query("length") perPage: Int,
        @Query("sort") sort: Int = 1
    ): ResponseData<List<ChapterModel>>

    @GET("/chapters/:chapterId")
    suspend fun getDetailChapter(idChapter: String): ResponseWithoutData

    @GET
    suspend fun crawlChapterServer1(
        @Url urlImg: String
    ): Response<ResponseBody>

    @Headers("cookie: content_server=server2")
    @GET
    suspend fun crawlChapterServer2(
        @Url urlImg: String
    ): Response<ResponseBody>

    @POST("/chapters/images/{chapterId}")
    suspend fun postImgChapterToServer(
        @Path("chapterId") idChapter: String,
        @Body data: ImageModel
    ): ResponseWithoutData

}