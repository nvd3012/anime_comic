package anime.comic.data.remote.api.auth

import anime.comic.data.model.UserModel
import anime.comic.data.ResponseData
import anime.comic.data.ResponseWithoutData
import retrofit2.http.Body
import retrofit2.http.POST

interface UserApiService {
    @POST("/signup")
    suspend fun signUp(@Body body: SignUpRequest): ResponseWithoutData

    @POST("/login")
    suspend fun login(@Body body: LoginRequest): ResponseData<UserModel>

    @POST("/reset-password")
    suspend fun resetPassword(@Body body: Map<String,String>): ResponseWithoutData

    @POST("/send-verify")
    suspend fun sendVerify(@Body body: Map<String,String>): ResponseWithoutData

    @POST("/fcm/token")
    suspend fun sendTokenFcm(@Body body: Map<String, String>): ResponseWithoutData
}