package anime.comic.data.remote.api.auth

import anime.comic.util.helper.ValidationHelper

data class LoginRequest(
    var email: String = "",
    var password: String = "",
) {
    fun isEnableLoginButton() =
        ValidationHelper.isValidEmail(email) && ValidationHelper.isPassword(password)

    fun isEnableResetPasswordButton() = ValidationHelper.isValidEmail(email)

    fun textErrorEmail() =
        if (ValidationHelper.isValidEmail(email) || email.isEmpty()) null else "Email error"

    fun textErrorPassword() =
        if (ValidationHelper.isPassword(password) || password.isEmpty()) null else "Password error"
}