package anime.comic.data.remote.api.genre

import anime.comic.data.ResponseData
import anime.comic.data.model.ComicModel
import anime.comic.data.model.GenreModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface GenreApiService {
    @GET("genres")
    suspend fun getListGenres(): ResponseData<List<GenreModel>>

    @GET("comics")
    suspend fun getListComicsByGenre(
        @Query("page") page: Int,
        @Query("length") perPage: Int,
        @Query("genre") genre: String
    ): ResponseData<List<ComicModel>>

    @GET
    suspend fun apiTest(@Url api: String)
}