package anime.comic.data.remote.api.comics

import anime.comic.data.ResponseData
import anime.comic.data.ResponseWithoutData
import anime.comic.data.model.ComicModel
import retrofit2.http.*

interface ComicsApiService {

    @GET("comics")
    suspend fun getListComicsByKeyWord(
        @Query("page") page: Int,
        @Query("length") perPage: Int,
        @Query("search") keyWord: String
    ): ResponseData<List<ComicModel>>

    @GET("comics")
    suspend fun getListComicsByType(
        @Query("page") page: Int,
        @Query("length") perPage: Int,
        @Query("type") type: String
    ): ResponseData<List<ComicModel>>

    @GET("comics/{idComic}")
    suspend fun getComicById(@Path("idComic") id: String): ResponseData<ComicModel>

    @GET("bookmarks")
    suspend fun getBookmarksStateByComicId(@Query("comicId")id : String) : ResponseWithoutData

    @POST("bookmarks")
    suspend fun postBookmarksComicById(@Body body: Map<String,*>): ResponseWithoutData
}