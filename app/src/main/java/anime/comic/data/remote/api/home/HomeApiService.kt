package anime.comic.data.remote.api.home

import anime.comic.data.ResponseData
import anime.comic.data.ResponseWithoutData
import anime.comic.data.model.AppConfigModel
import anime.comic.data.model.HomeModel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface HomeApiService {
    @GET("comics/home")
    suspend fun getDataHomeScreen(): ResponseData<HomeModel>

    @POST("/device")
    suspend fun postFIDToServer(@Body body: Map<String,String>): ResponseWithoutData

    @GET("general/get-app-function")
    suspend fun getAppConfigFromServer(): ResponseData<AppConfigModel>
}