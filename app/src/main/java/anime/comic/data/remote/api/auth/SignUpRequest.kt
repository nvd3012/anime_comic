package anime.comic.data.remote.api.auth

import anime.comic.util.helper.ValidationHelper

data class SignUpRequest(
    var email: String = "",
    var password: String = "",
    var confirmPassword: String = "",
    var isAcceptTerm: Boolean = false
) {
    fun isEnableRegisterButton() =
        ValidationHelper.isValidEmail(email) && ValidationHelper.isPassword(password) && password == confirmPassword && isAcceptTerm

    fun textErrorConfirmPassword() =
        if (password == confirmPassword || confirmPassword.isEmpty()) null else "Password confirm error"

    fun textErrorEmail() =
        if (ValidationHelper.isValidEmail(email) || email.isEmpty()) null else "Email error"

    fun textErrorPassword() =
        if (ValidationHelper.isPassword(password) || password.isEmpty()) null else "Password error"
}