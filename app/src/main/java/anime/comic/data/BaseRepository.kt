package anime.comic.data

import com.squareup.moshi.JsonClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

typealias ResponseWithoutData = Response<BaseResponse<Any?>>
typealias ResponseData<T> = Response<BaseResponse<T>>

/**
 * Abstract Base repository class with error handling
 */
abstract class BaseRepository {

    protected suspend fun <T> getResult(call: suspend () -> Response<BaseResponse<T>>): Resource<T> =
        withContext(Dispatchers.IO) {
            try {
                val response = call()
                when (val body = response.body()) {
                    is BaseResponse -> {
                        when (response.code()) {
                            API_STATUS_200 -> {
                                Resource.successOrNull(body.data)
                            }
                            else -> {
                                Resource.error(
                                    ErrorResponse.createApiError(
                                        response.code(),
                                        body.message
                                    )
                                )
                            }
                        }
                    }
                    else -> {
                        Resource.error(ErrorResponse.createNetworkError())
                    }
                }
            } catch (t: Throwable) {
                t.printStackTrace()
                when (t) {
                    is SocketTimeoutException -> Resource.error(ErrorResponse.createTimeoutError())
                    is HttpException -> Resource.error(ErrorResponse.createNotFoundError())
                    else -> Resource.error(ErrorResponse.createNetworkError())
                }
            }
        }

    protected suspend fun <T> getResultPublic(call: suspend () -> Response<T>): Resource<T> =
        withContext(Dispatchers.IO) {
            try {
                val response = call()
                    when (response.code()) {
                        API_STATUS_200 -> {
                            Resource.successOrNull(response.body())
                        }
                        else -> {
                            Resource.error(
                                ErrorResponse.createApiError(
                                    response.code(),
                                    response.message()
                                )
                            )
                        }
                    }
            } catch (t: Throwable) {
                t.printStackTrace()
                when (t) {
                    is SocketTimeoutException -> Resource.error(ErrorResponse.createTimeoutError())
                    is HttpException -> Resource.error(ErrorResponse.createNotFoundError())
                    else -> Resource.error(ErrorResponse.createNetworkError())
                }
            }
        }

    companion object {
        const val API_STATUS_200 = 200
        const val API_STATUS_400 = 400
        const val API_STATUS_401 = 401
        const val API_STATUS_500 = 500
    }
}

@JsonClass(generateAdapter = true)
data class BaseResponse<T>(
    val success: Boolean = false,
    val message: String = "",
    val data: T?
)