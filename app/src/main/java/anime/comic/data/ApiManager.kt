package anime.comic.data

import anime.comic.BuildConfig
import anime.comic.data.local.dao.AppConfigDAO
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.runBlocking
import okhttp3.ConnectionPool
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiManager @Inject constructor(private val appConfigDAO: AppConfigDAO) {

    inline fun <reified T> createApiService(): T {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        return retrofit.create(T::class.java)
    }

    inline fun <reified T> createApiServicePublic(): T {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        return retrofit.create(T::class.java)
    }


    private fun loggingInterceptor(): Interceptor =
        HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        }


    private fun headerInterceptor(): Interceptor = runBlocking {
        val appConfig = appConfigDAO.getAppConfig()
        Interceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader(
                    "Authorization", appConfig?.token ?: BuildConfig.TOKEN_APP
                ).addHeader("isUser", "${appConfig?.isUser ?: false}")
                .build()
            chain.proceed(request)
        }
    }


    fun okHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .readTimeout(20, TimeUnit.SECONDS)
        .connectionPool(ConnectionPool(40, 3, TimeUnit.MINUTES))
        .addInterceptor(headerInterceptor())
        .addInterceptor(loggingInterceptor())
        .build()
}