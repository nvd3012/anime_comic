package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class HomeModel(
    val latest: List<ComicModel>?,
    val newest: List<ComicModel>?,
    val popular: List<ComicModel>?,
    val topWeek: List<ComicModel>?,
    val banners: List<String>?
) {

}