package anime.comic.data.model

data class AlertDialogModel(
    val title: String? = null,
    val content: String? = null,
    val textPos: String? = null,
    val textNeg: String? = null,
    val isCancelable: Boolean = false,
    val isCanceledOnTouchOutside: Boolean = false,
    val posEvent: () -> Unit = {},
    val negEvent: () -> Unit = {}
)