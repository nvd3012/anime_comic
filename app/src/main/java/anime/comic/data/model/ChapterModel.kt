package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class ChapterModel(
    val _id: String,
    val name: String,
    val uploaded: String,
    val view: Long,
    val href: String?
) {
}