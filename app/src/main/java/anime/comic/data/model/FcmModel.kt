package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class FcmModel(
    val id: String?,
    val title: String?,
    val content: String?,
    val state: Boolean = false
) {

}