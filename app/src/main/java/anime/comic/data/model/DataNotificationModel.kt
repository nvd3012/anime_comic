package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class DataNotificationModel(
    val fcmId: Int,
    val topicType: String?,
    val title: String?,
    val content: String?,
    val comicId : String?
) {
}