package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class FunctionModel(
    val imgQuery: String?
) {
}