package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class UserModel(
    val _id: String,
    val coin: Long = 0,
    val referralCode: String = "",
    val shareCode: String = "",
    val isNotify: Boolean = false,
    val isBlocked: Boolean = false,
    val isAcceptPolicy: Boolean = false,
    val isDailySpin: Boolean = false,
    val accessToken: String
)