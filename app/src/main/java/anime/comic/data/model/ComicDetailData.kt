package anime.comic.data.model

import android.os.Parcelable
import anime.comic.util.helper.CommonHelper
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ComicDetailData(
    val id: String = "",
    val authors: List<String> = listOf(),
    val description: String = "",
    val genres: List<String> = listOf(),
    val name: String = "",
    val otherName: String = "",
    val rating: Float = 0f,
    val status: String = "",
    val thumbnail: String = "",
    val updated: String = "",
    val viewCount: Long = 0,
    val lastChapter: Int = 0,
    val newChapter: String = "",
    val code: String = ""
) : Parcelable{

    fun formatViewCount() = CommonHelper.formatNumberSocial(viewCount)

    fun formatState() = if (status != "Completed") updated else status

    fun chapterCount() = lastChapter.toString()

    override fun toString(): String {
        return super.toString()
    }
}