package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass

@Keep
@JsonClass(generateAdapter = true)
data class AppConfigModel(
    val _id: String?,
    val isActived: Int?,
    val name: String?,
    val function: FunctionModel?,
    val status: Int?
) {
}