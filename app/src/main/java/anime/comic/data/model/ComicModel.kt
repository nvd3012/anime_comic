package anime.comic.data.model

import androidx.annotation.Keep
import com.squareup.moshi.JsonClass
import kotlin.math.abs

@Keep
@JsonClass(generateAdapter = true)
data class ComicModel(
    val _id: String,
    val authors: List<String>? = listOf(),
    val description: String? = "",
    val genres: List<String>? = listOf(),
    val name: String? = "",
    val otherName: String? = "",
    val rating: Float? = 0f,
    val status: String? = "",
    val thumbnail: String? = "",
    val updated: String? = "",
    val viewCount: Long? = 0,
    val lastChapter: Int? = 0,
    val newChapter: String? = "",
    val code: String? = ""
) {
}