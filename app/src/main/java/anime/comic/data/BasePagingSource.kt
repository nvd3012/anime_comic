package anime.comic.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import timber.log.Timber

class BasePagingSource<T : Any>(
    private val pageSize: Int,
    private val requestData: suspend (Int, LoadParams<Int>) -> List<T>
) : PagingSource<Int, T>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, T> = try {
        val position = params.key ?: STARTING_PAGE_INDEX
        val dataList = requestData(position, params)
        LoadResult.Page(
            data = dataList,
            prevKey = if (position == STARTING_PAGE_INDEX) null else position - STARTING_PAGE_INDEX,
            nextKey = if (dataList.isEmpty()) {
                null
            } else {
                // initial load size = 3 * NETWORK_PAGE_SIZE
                // ensure we're not requesting duplicating items, at the 2nd request
                position + (params.loadSize / pageSize)
            }
        )
    } catch (e: Exception) {
        Timber.e("error paging $e")
        LoadResult.Error(e)
    }

    override fun getRefreshKey(state: PagingState<Int, T>) = STARTING_PAGE_INDEX

    companion object {
        const val STARTING_PAGE_INDEX = 1
    }
}